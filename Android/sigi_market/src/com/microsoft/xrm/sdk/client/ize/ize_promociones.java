package com.microsoft.xrm.sdk.client.ize;
// Mobile Model Generator for Dynamics CRM 1.0
//
// Copyright (c) Microsoft Corporation
//
// All rights reserved.
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the ""Software""), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//  ize_promociones.java

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;
import com.microsoft.xrm.sdk.*;


@EntityLogicalNameAttribute("ize_promociones")
public class ize_promociones extends Entity {
	
	public static final String EntityLogicalName = "ize_promociones";
	public static final int EntityTypeCode = 10019;
	
	public ize_promociones() {
		super("ize_promociones");
	}

	public static ize_promociones build() {
		return new ize_promociones();
	}

	@AttributeSchemaNameAttribute("ize_promocionesId")
	@AttributeLogicalNameAttribute("ize_promocionesid")
	public UUID getize_promocionesId() {
		return this.getAttributeValue("ize_promocionesid");
	}
	
	@AttributeSchemaNameAttribute("ize_promocionesId")
	@AttributeLogicalNameAttribute("ize_promocionesid")
	public ize_promociones setize_promocionesId(UUID value) {
		this.setAttributeValue("ize_promocionesid", value);
		if (value != null) {
			super.setId(value);
		}
		else {
			super.setId(new UUID(0L, 0L));
		}

		return this;
	}
	
	@Override
	@AttributeSchemaNameAttribute("ize_promocionesId")
	@AttributeLogicalNameAttribute("ize_promocionesid")
	public UUID getId() {
		return super.getId();
	}
	
	@Override
	@AttributeSchemaNameAttribute("ize_promocionesId")
	@AttributeLogicalNameAttribute("ize_promocionesid")
	public void setId(UUID value) {
		this.setize_promocionesId(value);
	}
	

	@AttributeSchemaNameAttribute("CreatedBy")
	@AttributeLogicalNameAttribute("createdby")
	public EntityReference getCreatedBy() {
		return this.getAttributeValue("createdby");
	}
	
	@AttributeSchemaNameAttribute("CreatedBy")
	@AttributeLogicalNameAttribute("createdby")
	public ize_promociones setCreatedBy(EntityReference value) {
		this.setAttributeValue("createdby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedByName")
	@AttributeLogicalNameAttribute("createdbyname")
	public String getCreatedByName() {
		return this.getAttributeValue("createdbyname");
	}
	
	@AttributeSchemaNameAttribute("CreatedByName")
	@AttributeLogicalNameAttribute("createdbyname")
	public ize_promociones setCreatedByName(String value) {
		this.setAttributeValue("createdbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedByYomiName")
	@AttributeLogicalNameAttribute("createdbyyominame")
	public String getCreatedByYomiName() {
		return this.getAttributeValue("createdbyyominame");
	}
	
	@AttributeSchemaNameAttribute("CreatedByYomiName")
	@AttributeLogicalNameAttribute("createdbyyominame")
	public ize_promociones setCreatedByYomiName(String value) {
		this.setAttributeValue("createdbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOn")
	@AttributeLogicalNameAttribute("createdon")
	public Date getCreatedOn() {
		return this.getAttributeValue("createdon");
	}
	
	@AttributeSchemaNameAttribute("CreatedOn")
	@AttributeLogicalNameAttribute("createdon")
	public ize_promociones setCreatedOn(Date value) {
		this.setAttributeValue("createdon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfBy")
	@AttributeLogicalNameAttribute("createdonbehalfby")
	public EntityReference getCreatedOnBehalfBy() {
		return this.getAttributeValue("createdonbehalfby");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfBy")
	@AttributeLogicalNameAttribute("createdonbehalfby")
	public ize_promociones setCreatedOnBehalfBy(EntityReference value) {
		this.setAttributeValue("createdonbehalfby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfByName")
	@AttributeLogicalNameAttribute("createdonbehalfbyname")
	public String getCreatedOnBehalfByName() {
		return this.getAttributeValue("createdonbehalfbyname");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfByName")
	@AttributeLogicalNameAttribute("createdonbehalfbyname")
	public ize_promociones setCreatedOnBehalfByName(String value) {
		this.setAttributeValue("createdonbehalfbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("createdonbehalfbyyominame")
	public String getCreatedOnBehalfByYomiName() {
		return this.getAttributeValue("createdonbehalfbyyominame");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("createdonbehalfbyyominame")
	public ize_promociones setCreatedOnBehalfByYomiName(String value) {
		this.setAttributeValue("createdonbehalfbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("EntityImage_Timestamp")
	@AttributeLogicalNameAttribute("entityimage_timestamp")
	public BigInteger getEntityImage_Timestamp() {
		return this.getAttributeValue("entityimage_timestamp");
	}
	
	@AttributeSchemaNameAttribute("EntityImage_Timestamp")
	@AttributeLogicalNameAttribute("entityimage_timestamp")
	public ize_promociones setEntityImage_Timestamp(BigInteger value) {
		this.setAttributeValue("entityimage_timestamp", value);
		return this;
	}

	@AttributeSchemaNameAttribute("EntityImage_URL")
	@AttributeLogicalNameAttribute("entityimage_url")
	public String getEntityImage_URL() {
		return this.getAttributeValue("entityimage_url");
	}
	
	@AttributeSchemaNameAttribute("EntityImage_URL")
	@AttributeLogicalNameAttribute("entityimage_url")
	public ize_promociones setEntityImage_URL(String value) {
		this.setAttributeValue("entityimage_url", value);
		return this;
	}

	@AttributeSchemaNameAttribute("EntityImageId")
	@AttributeLogicalNameAttribute("entityimageid")
	public UUID getEntityImageId() {
		return this.getAttributeValue("entityimageid");
	}
	
	@AttributeSchemaNameAttribute("EntityImageId")
	@AttributeLogicalNameAttribute("entityimageid")
	public ize_promociones setEntityImageId(UUID value) {
		this.setAttributeValue("entityimageid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ImportSequenceNumber")
	@AttributeLogicalNameAttribute("importsequencenumber")
	public int getImportSequenceNumber() {
		return this.getAttributeValue("importsequencenumber");
	}
	
	@AttributeSchemaNameAttribute("ImportSequenceNumber")
	@AttributeLogicalNameAttribute("importsequencenumber")
	public ize_promociones setImportSequenceNumber(int value) {
		this.setAttributeValue("importsequencenumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ActadeRecepcin")
	@AttributeLogicalNameAttribute("ize_actaderecepcin")
	public Date getIze_ActadeRecepcin() {
		return this.getAttributeValue("ize_actaderecepcin");
	}
	
	@AttributeSchemaNameAttribute("Ize_ActadeRecepcin")
	@AttributeLogicalNameAttribute("ize_actaderecepcin")
	public ize_promociones setIze_ActadeRecepcin(Date value) {
		this.setAttributeValue("ize_actaderecepcin", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_address1_composite")
	@AttributeLogicalNameAttribute("ize_address1_composite")
	public String getIze_address1_composite() {
		return this.getAttributeValue("ize_address1_composite");
	}
	
	@AttributeSchemaNameAttribute("Ize_address1_composite")
	@AttributeLogicalNameAttribute("ize_address1_composite")
	public ize_promociones setIze_address1_composite(String value) {
		this.setAttributeValue("ize_address1_composite", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Ascensores")
	@AttributeLogicalNameAttribute("ize_ascensores")
	public String getIze_Ascensores() {
		return this.getAttributeValue("ize_ascensores");
	}
	
	@AttributeSchemaNameAttribute("Ize_Ascensores")
	@AttributeLogicalNameAttribute("ize_ascensores")
	public ize_promociones setIze_Ascensores(String value) {
		this.setAttributeValue("ize_ascensores", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_calle")
	@AttributeLogicalNameAttribute("ize_calle")
	public String getIze_calle() {
		return this.getAttributeValue("ize_calle");
	}
	
	@AttributeSchemaNameAttribute("Ize_calle")
	@AttributeLogicalNameAttribute("ize_calle")
	public ize_promociones setIze_calle(String value) {
		this.setAttributeValue("ize_calle", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Calle2")
	@AttributeLogicalNameAttribute("ize_calle2")
	public String getIze_Calle2() {
		return this.getAttributeValue("ize_calle2");
	}
	
	@AttributeSchemaNameAttribute("Ize_Calle2")
	@AttributeLogicalNameAttribute("ize_calle2")
	public ize_promociones setIze_Calle2(String value) {
		this.setAttributeValue("ize_calle2", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Calle3")
	@AttributeLogicalNameAttribute("ize_calle3")
	public String getIze_Calle3() {
		return this.getAttributeValue("ize_calle3");
	}
	
	@AttributeSchemaNameAttribute("Ize_Calle3")
	@AttributeLogicalNameAttribute("ize_calle3")
	public ize_promociones setIze_Calle3(String value) {
		this.setAttributeValue("ize_calle3", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Capexdescripcin")
	@AttributeLogicalNameAttribute("ize_capexdescripcin")
	public String getIze_Capexdescripcin() {
		return this.getAttributeValue("ize_capexdescripcin");
	}
	
	@AttributeSchemaNameAttribute("Ize_Capexdescripcin")
	@AttributeLogicalNameAttribute("ize_capexdescripcin")
	public ize_promociones setIze_Capexdescripcin(String value) {
		this.setAttributeValue("ize_capexdescripcin", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Capexestimado")
	@AttributeLogicalNameAttribute("ize_capexestimado")
	public String getIze_Capexestimado() {
		return this.getAttributeValue("ize_capexestimado");
	}
	
	@AttributeSchemaNameAttribute("Ize_Capexestimado")
	@AttributeLogicalNameAttribute("ize_capexestimado")
	public ize_promociones setIze_Capexestimado(String value) {
		this.setAttributeValue("ize_capexestimado", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Cdigo")
	@AttributeLogicalNameAttribute("ize_cdigo")
	public String getIze_Cdigo() {
		return this.getAttributeValue("ize_cdigo");
	}
	
	@AttributeSchemaNameAttribute("Ize_Cdigo")
	@AttributeLogicalNameAttribute("ize_cdigo")
	public ize_promociones setIze_Cdigo(String value) {
		this.setAttributeValue("ize_cdigo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_CertFinObra")
	@AttributeLogicalNameAttribute("ize_certfinobra")
	public Date getIze_CertFinObra() {
		return this.getAttributeValue("ize_certfinobra");
	}
	
	@AttributeSchemaNameAttribute("Ize_CertFinObra")
	@AttributeLogicalNameAttribute("ize_certfinobra")
	public ize_promociones setIze_CertFinObra(Date value) {
		this.setAttributeValue("ize_certfinobra", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Ciudad")
	@AttributeLogicalNameAttribute("ize_ciudad")
	public String getIze_Ciudad() {
		return this.getAttributeValue("ize_ciudad");
	}
	
	@AttributeSchemaNameAttribute("Ize_Ciudad")
	@AttributeLogicalNameAttribute("ize_ciudad")
	public ize_promociones setIze_Ciudad(String value) {
		this.setAttributeValue("ize_ciudad", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_CodigoPostal")
	@AttributeLogicalNameAttribute("ize_codigopostal")
	public String getIze_CodigoPostal() {
		return this.getAttributeValue("ize_codigopostal");
	}
	
	@AttributeSchemaNameAttribute("Ize_CodigoPostal")
	@AttributeLogicalNameAttribute("ize_codigopostal")
	public ize_promociones setIze_CodigoPostal(String value) {
		this.setAttributeValue("ize_codigopostal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Comercializa")
	@AttributeLogicalNameAttribute("ize_comercializa")
	public EntityReference getIze_Comercializa() {
		return this.getAttributeValue("ize_comercializa");
	}
	
	@AttributeSchemaNameAttribute("Ize_Comercializa")
	@AttributeLogicalNameAttribute("ize_comercializa")
	public ize_promociones setIze_Comercializa(EntityReference value) {
		this.setAttributeValue("ize_comercializa", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Comercializador")
	@AttributeLogicalNameAttribute("ize_comercializador")
	public EntityReference getIze_Comercializador() {
		return this.getAttributeValue("ize_comercializador");
	}
	
	@AttributeSchemaNameAttribute("Ize_Comercializador")
	@AttributeLogicalNameAttribute("ize_comercializador")
	public ize_promociones setIze_Comercializador(EntityReference value) {
		this.setAttributeValue("ize_comercializador", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ComercializadorName")
	@AttributeLogicalNameAttribute("ize_comercializadorname")
	public String getIze_ComercializadorName() {
		return this.getAttributeValue("ize_comercializadorname");
	}
	
	@AttributeSchemaNameAttribute("Ize_ComercializadorName")
	@AttributeLogicalNameAttribute("ize_comercializadorname")
	public ize_promociones setIze_ComercializadorName(String value) {
		this.setAttributeValue("ize_comercializadorname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ComercializadorYomiName")
	@AttributeLogicalNameAttribute("ize_comercializadoryominame")
	public String getIze_ComercializadorYomiName() {
		return this.getAttributeValue("ize_comercializadoryominame");
	}
	
	@AttributeSchemaNameAttribute("Ize_ComercializadorYomiName")
	@AttributeLogicalNameAttribute("ize_comercializadoryominame")
	public ize_promociones setIze_ComercializadorYomiName(String value) {
		this.setAttributeValue("ize_comercializadoryominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ComercializaName")
	@AttributeLogicalNameAttribute("ize_comercializaname")
	public String getIze_ComercializaName() {
		return this.getAttributeValue("ize_comercializaname");
	}
	
	@AttributeSchemaNameAttribute("Ize_ComercializaName")
	@AttributeLogicalNameAttribute("ize_comercializaname")
	public ize_promociones setIze_ComercializaName(String value) {
		this.setAttributeValue("ize_comercializaname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ComercializaYomiName")
	@AttributeLogicalNameAttribute("ize_comercializayominame")
	public String getIze_ComercializaYomiName() {
		return this.getAttributeValue("ize_comercializayominame");
	}
	
	@AttributeSchemaNameAttribute("Ize_ComercializaYomiName")
	@AttributeLogicalNameAttribute("ize_comercializayominame")
	public ize_promociones setIze_ComercializaYomiName(String value) {
		this.setAttributeValue("ize_comercializayominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Descripcion")
	@AttributeLogicalNameAttribute("ize_descripcion")
	public String getIze_Descripcion() {
		return this.getAttributeValue("ize_descripcion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Descripcion")
	@AttributeLogicalNameAttribute("ize_descripcion")
	public ize_promociones setIze_Descripcion(String value) {
		this.setAttributeValue("ize_descripcion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Destacada")
	@AttributeLogicalNameAttribute("ize_destacada")
	public OptionSetValue getIze_Destacada() {
		return this.getAttributeValue("ize_destacada");
	}
	
	@AttributeSchemaNameAttribute("Ize_Destacada")
	@AttributeLogicalNameAttribute("ize_destacada")
	public ize_promociones setIze_Destacada(OptionSetValue value) {
		this.setAttributeValue("ize_destacada", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_DiponiblePortalWeb")
	@AttributeLogicalNameAttribute("ize_diponibleportalweb")
	public OptionSetValue getIze_DiponiblePortalWeb() {
		return this.getAttributeValue("ize_diponibleportalweb");
	}
	
	@AttributeSchemaNameAttribute("Ize_DiponiblePortalWeb")
	@AttributeLogicalNameAttribute("ize_diponibleportalweb")
	public ize_promociones setIze_DiponiblePortalWeb(OptionSetValue value) {
		this.setAttributeValue("ize_diponibleportalweb", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_direccioncompleta")
	@AttributeLogicalNameAttribute("ize_direccioncompleta")
	public String getIze_direccioncompleta() {
		return this.getAttributeValue("ize_direccioncompleta");
	}
	
	@AttributeSchemaNameAttribute("Ize_direccioncompleta")
	@AttributeLogicalNameAttribute("ize_direccioncompleta")
	public ize_promociones setIze_direccioncompleta(String value) {
		this.setAttributeValue("ize_direccioncompleta", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_EntidadFinanciera")
	@AttributeLogicalNameAttribute("ize_entidadfinanciera")
	public EntityReference getIze_EntidadFinanciera() {
		return this.getAttributeValue("ize_entidadfinanciera");
	}
	
	@AttributeSchemaNameAttribute("Ize_EntidadFinanciera")
	@AttributeLogicalNameAttribute("ize_entidadfinanciera")
	public ize_promociones setIze_EntidadFinanciera(EntityReference value) {
		this.setAttributeValue("ize_entidadfinanciera", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_EntidadFinancieraName")
	@AttributeLogicalNameAttribute("ize_entidadfinancieraname")
	public String getIze_EntidadFinancieraName() {
		return this.getAttributeValue("ize_entidadfinancieraname");
	}
	
	@AttributeSchemaNameAttribute("Ize_EntidadFinancieraName")
	@AttributeLogicalNameAttribute("ize_entidadfinancieraname")
	public ize_promociones setIze_EntidadFinancieraName(String value) {
		this.setAttributeValue("ize_entidadfinancieraname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_EntidadFinancieraYomiName")
	@AttributeLogicalNameAttribute("ize_entidadfinancierayominame")
	public String getIze_EntidadFinancieraYomiName() {
		return this.getAttributeValue("ize_entidadfinancierayominame");
	}
	
	@AttributeSchemaNameAttribute("Ize_EntidadFinancieraYomiName")
	@AttributeLogicalNameAttribute("ize_entidadfinancierayominame")
	public ize_promociones setIze_EntidadFinancieraYomiName(String value) {
		this.setAttributeValue("ize_entidadfinancierayominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Estado")
	@AttributeLogicalNameAttribute("ize_estado")
	public OptionSetValue getIze_Estado() {
		return this.getAttributeValue("ize_estado");
	}
	
	@AttributeSchemaNameAttribute("Ize_Estado")
	@AttributeLogicalNameAttribute("ize_estado")
	public ize_promociones setIze_Estado(OptionSetValue value) {
		this.setAttributeValue("ize_estado", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Estadooprovincia")
	@AttributeLogicalNameAttribute("ize_estadooprovincia")
	public String getIze_Estadooprovincia() {
		return this.getAttributeValue("ize_estadooprovincia");
	}
	
	@AttributeSchemaNameAttribute("Ize_Estadooprovincia")
	@AttributeLogicalNameAttribute("ize_estadooprovincia")
	public ize_promociones setIze_Estadooprovincia(String value) {
		this.setAttributeValue("ize_estadooprovincia", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_FechaEscrituraPrevista")
	@AttributeLogicalNameAttribute("ize_fechaescrituraprevista")
	public Date getIze_FechaEscrituraPrevista() {
		return this.getAttributeValue("ize_fechaescrituraprevista");
	}
	
	@AttributeSchemaNameAttribute("Ize_FechaEscrituraPrevista")
	@AttributeLogicalNameAttribute("ize_fechaescrituraprevista")
	public ize_promociones setIze_FechaEscrituraPrevista(Date value) {
		this.setAttributeValue("ize_fechaescrituraprevista", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_FechaEscrituraReal")
	@AttributeLogicalNameAttribute("ize_fechaescriturareal")
	public Date getIze_FechaEscrituraReal() {
		return this.getAttributeValue("ize_fechaescriturareal");
	}
	
	@AttributeSchemaNameAttribute("Ize_FechaEscrituraReal")
	@AttributeLogicalNameAttribute("ize_fechaescriturareal")
	public ize_promociones setIze_FechaEscrituraReal(Date value) {
		this.setAttributeValue("ize_fechaescriturareal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_FechaFinObraReal")
	@AttributeLogicalNameAttribute("ize_fechafinobrareal")
	public Date getIze_FechaFinObraReal() {
		return this.getAttributeValue("ize_fechafinobrareal");
	}
	
	@AttributeSchemaNameAttribute("Ize_FechaFinObraReal")
	@AttributeLogicalNameAttribute("ize_fechafinobrareal")
	public ize_promociones setIze_FechaFinObraReal(Date value) {
		this.setAttributeValue("ize_fechafinobrareal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_FechaInicioListPostVenta")
	@AttributeLogicalNameAttribute("ize_fechainiciolistpostventa")
	public Date getIze_FechaInicioListPostVenta() {
		return this.getAttributeValue("ize_fechainiciolistpostventa");
	}
	
	@AttributeSchemaNameAttribute("Ize_FechaInicioListPostVenta")
	@AttributeLogicalNameAttribute("ize_fechainiciolistpostventa")
	public ize_promociones setIze_FechaInicioListPostVenta(Date value) {
		this.setAttributeValue("ize_fechainiciolistpostventa", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_FechaInicioListPreentrega")
	@AttributeLogicalNameAttribute("ize_fechainiciolistpreentrega")
	public Date getIze_FechaInicioListPreentrega() {
		return this.getAttributeValue("ize_fechainiciolistpreentrega");
	}
	
	@AttributeSchemaNameAttribute("Ize_FechaInicioListPreentrega")
	@AttributeLogicalNameAttribute("ize_fechainiciolistpreentrega")
	public ize_promociones setIze_FechaInicioListPreentrega(Date value) {
		this.setAttributeValue("ize_fechainiciolistpreentrega", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_FechaLicenciadeObras")
	@AttributeLogicalNameAttribute("ize_fechalicenciadeobras")
	public Date getIze_FechaLicenciadeObras() {
		return this.getAttributeValue("ize_fechalicenciadeobras");
	}
	
	@AttributeSchemaNameAttribute("Ize_FechaLicenciadeObras")
	@AttributeLogicalNameAttribute("ize_fechalicenciadeobras")
	public ize_promociones setIze_FechaLicenciadeObras(Date value) {
		this.setAttributeValue("ize_fechalicenciadeobras", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Gimnasio")
	@AttributeLogicalNameAttribute("ize_gimnasio")
	public OptionSetValue getIze_Gimnasio() {
		return this.getAttributeValue("ize_gimnasio");
	}
	
	@AttributeSchemaNameAttribute("Ize_Gimnasio")
	@AttributeLogicalNameAttribute("ize_gimnasio")
	public ize_promociones setIze_Gimnasio(OptionSetValue value) {
		this.setAttributeValue("ize_gimnasio", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Golf")
	@AttributeLogicalNameAttribute("ize_golf")
	public OptionSetValue getIze_Golf() {
		return this.getAttributeValue("ize_golf");
	}
	
	@AttributeSchemaNameAttribute("Ize_Golf")
	@AttributeLogicalNameAttribute("ize_golf")
	public ize_promociones setIze_Golf(OptionSetValue value) {
		this.setAttributeValue("ize_golf", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ID")
	@AttributeLogicalNameAttribute("ize_id")
	public String getIze_ID() {
		return this.getAttributeValue("ize_id");
	}
	
	@AttributeSchemaNameAttribute("Ize_ID")
	@AttributeLogicalNameAttribute("ize_id")
	public ize_promociones setIze_ID(String value) {
		this.setAttributeValue("ize_id", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Iniciocomercializacion")
	@AttributeLogicalNameAttribute("ize_iniciocomercializacion")
	public Date getIze_Iniciocomercializacion() {
		return this.getAttributeValue("ize_iniciocomercializacion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Iniciocomercializacion")
	@AttributeLogicalNameAttribute("ize_iniciocomercializacion")
	public ize_promociones setIze_Iniciocomercializacion(Date value) {
		this.setAttributeValue("ize_iniciocomercializacion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_latitud")
	@AttributeLogicalNameAttribute("ize_latitud")
	public double getIze_latitud() {
		return this.getAttributeValue("ize_latitud");
	}
	
	@AttributeSchemaNameAttribute("Ize_latitud")
	@AttributeLogicalNameAttribute("ize_latitud")
	public ize_promociones setIze_latitud(double value) {
		this.setAttributeValue("ize_latitud", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_LicActGaraje")
	@AttributeLogicalNameAttribute("ize_licactgaraje")
	public Date getIze_LicActGaraje() {
		return this.getAttributeValue("ize_licactgaraje");
	}
	
	@AttributeSchemaNameAttribute("Ize_LicActGaraje")
	@AttributeLogicalNameAttribute("ize_licactgaraje")
	public ize_promociones setIze_LicActGaraje(Date value) {
		this.setAttributeValue("ize_licactgaraje", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_LicActPiscina")
	@AttributeLogicalNameAttribute("ize_licactpiscina")
	public Date getIze_LicActPiscina() {
		return this.getAttributeValue("ize_licactpiscina");
	}
	
	@AttributeSchemaNameAttribute("Ize_LicActPiscina")
	@AttributeLogicalNameAttribute("ize_licactpiscina")
	public ize_promociones setIze_LicActPiscina(Date value) {
		this.setAttributeValue("ize_licactpiscina", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Licprimeraocupedif")
	@AttributeLogicalNameAttribute("ize_licprimeraocupedif")
	public Date getIze_Licprimeraocupedif() {
		return this.getAttributeValue("ize_licprimeraocupedif");
	}
	
	@AttributeSchemaNameAttribute("Ize_Licprimeraocupedif")
	@AttributeLogicalNameAttribute("ize_licprimeraocupedif")
	public ize_promociones setIze_Licprimeraocupedif(Date value) {
		this.setAttributeValue("ize_licprimeraocupedif", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Longitud")
	@AttributeLogicalNameAttribute("ize_longitud")
	public double getIze_Longitud() {
		return this.getAttributeValue("ize_longitud");
	}
	
	@AttributeSchemaNameAttribute("Ize_Longitud")
	@AttributeLogicalNameAttribute("ize_longitud")
	public ize_promociones setIze_Longitud(double value) {
		this.setAttributeValue("ize_longitud", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_name")
	@AttributeLogicalNameAttribute("ize_name")
	public String getIze_name() {
		return this.getAttributeValue("ize_name");
	}
	
	@AttributeSchemaNameAttribute("Ize_name")
	@AttributeLogicalNameAttribute("ize_name")
	public ize_promociones setIze_name(String value) {
		this.setAttributeValue("ize_name", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Numactivosocipadosileg")
	@AttributeLogicalNameAttribute("ize_numactivosocipadosileg")
	public String getIze_Numactivosocipadosileg() {
		return this.getAttributeValue("ize_numactivosocipadosileg");
	}
	
	@AttributeSchemaNameAttribute("Ize_Numactivosocipadosileg")
	@AttributeLogicalNameAttribute("ize_numactivosocipadosileg")
	public ize_promociones setIze_Numactivosocipadosileg(String value) {
		this.setAttributeValue("ize_numactivosocipadosileg", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_numbloques")
	@AttributeLogicalNameAttribute("ize_numbloques")
	public String getIze_numbloques() {
		return this.getAttributeValue("ize_numbloques");
	}
	
	@AttributeSchemaNameAttribute("Ize_numbloques")
	@AttributeLogicalNameAttribute("ize_numbloques")
	public ize_promociones setIze_numbloques(String value) {
		this.setAttributeValue("ize_numbloques", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_NumdeAlturas")
	@AttributeLogicalNameAttribute("ize_numdealturas")
	public String getIze_NumdeAlturas() {
		return this.getAttributeValue("ize_numdealturas");
	}
	
	@AttributeSchemaNameAttribute("Ize_NumdeAlturas")
	@AttributeLogicalNameAttribute("ize_numdealturas")
	public ize_promociones setIze_NumdeAlturas(String value) {
		this.setAttributeValue("ize_numdealturas", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_numhabitantesmunicipio")
	@AttributeLogicalNameAttribute("ize_numhabitantesmunicipio")
	public double getIze_numhabitantesmunicipio() {
		return this.getAttributeValue("ize_numhabitantesmunicipio");
	}
	
	@AttributeSchemaNameAttribute("Ize_numhabitantesmunicipio")
	@AttributeLogicalNameAttribute("ize_numhabitantesmunicipio")
	public ize_promociones setIze_numhabitantesmunicipio(double value) {
		this.setAttributeValue("ize_numhabitantesmunicipio", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Opexdescripcion")
	@AttributeLogicalNameAttribute("ize_opexdescripcion")
	public String getIze_Opexdescripcion() {
		return this.getAttributeValue("ize_opexdescripcion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Opexdescripcion")
	@AttributeLogicalNameAttribute("ize_opexdescripcion")
	public ize_promociones setIze_Opexdescripcion(String value) {
		this.setAttributeValue("ize_opexdescripcion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Opexestimado")
	@AttributeLogicalNameAttribute("ize_opexestimado")
	public String getIze_Opexestimado() {
		return this.getAttributeValue("ize_opexestimado");
	}
	
	@AttributeSchemaNameAttribute("Ize_Opexestimado")
	@AttributeLogicalNameAttribute("ize_opexestimado")
	public ize_promociones setIze_Opexestimado(String value) {
		this.setAttributeValue("ize_opexestimado", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PadelTenisSquash")
	@AttributeLogicalNameAttribute("ize_padeltenissquash")
	public OptionSetValue getIze_PadelTenisSquash() {
		return this.getAttributeValue("ize_padeltenissquash");
	}
	
	@AttributeSchemaNameAttribute("Ize_PadelTenisSquash")
	@AttributeLogicalNameAttribute("ize_padeltenissquash")
	public ize_promociones setIze_PadelTenisSquash(OptionSetValue value) {
		this.setAttributeValue("ize_padeltenissquash", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Pais")
	@AttributeLogicalNameAttribute("ize_pais")
	public String getIze_Pais() {
		return this.getAttributeValue("ize_pais");
	}
	
	@AttributeSchemaNameAttribute("Ize_Pais")
	@AttributeLogicalNameAttribute("ize_pais")
	public ize_promociones setIze_Pais(String value) {
		this.setAttributeValue("ize_pais", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Piscina")
	@AttributeLogicalNameAttribute("ize_piscina")
	public OptionSetValue getIze_Piscina() {
		return this.getAttributeValue("ize_piscina");
	}
	
	@AttributeSchemaNameAttribute("Ize_Piscina")
	@AttributeLogicalNameAttribute("ize_piscina")
	public ize_promociones setIze_Piscina(OptionSetValue value) {
		this.setAttributeValue("ize_piscina", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Portero")
	@AttributeLogicalNameAttribute("ize_portero")
	public OptionSetValue getIze_Portero() {
		return this.getAttributeValue("ize_portero");
	}
	
	@AttributeSchemaNameAttribute("Ize_Portero")
	@AttributeLogicalNameAttribute("ize_portero")
	public ize_promociones setIze_Portero(OptionSetValue value) {
		this.setAttributeValue("ize_portero", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Posicionexacta")
	@AttributeLogicalNameAttribute("ize_posicionexacta")
	public boolean getIze_Posicionexacta() {
		return this.getAttributeValue("ize_posicionexacta");
	}
	
	@AttributeSchemaNameAttribute("Ize_Posicionexacta")
	@AttributeLogicalNameAttribute("ize_posicionexacta")
	public ize_promociones setIze_Posicionexacta(boolean value) {
		this.setAttributeValue("ize_posicionexacta", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PromocinId")
	@AttributeLogicalNameAttribute("ize_promocinid")
	public EntityReference getIze_PromocinId() {
		return this.getAttributeValue("ize_promocinid");
	}
	
	@AttributeSchemaNameAttribute("Ize_PromocinId")
	@AttributeLogicalNameAttribute("ize_promocinid")
	public ize_promociones setIze_PromocinId(EntityReference value) {
		this.setAttributeValue("ize_promocinid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PromocinIdName")
	@AttributeLogicalNameAttribute("ize_promocinidname")
	public String getIze_PromocinIdName() {
		return this.getAttributeValue("ize_promocinidname");
	}
	
	@AttributeSchemaNameAttribute("Ize_PromocinIdName")
	@AttributeLogicalNameAttribute("ize_promocinidname")
	public ize_promociones setIze_PromocinIdName(String value) {
		this.setAttributeValue("ize_promocinidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Promocionbanco")
	@AttributeLogicalNameAttribute("ize_promocionbanco")
	public boolean getIze_Promocionbanco() {
		return this.getAttributeValue("ize_promocionbanco");
	}
	
	@AttributeSchemaNameAttribute("Ize_Promocionbanco")
	@AttributeLogicalNameAttribute("ize_promocionbanco")
	public ize_promociones setIze_Promocionbanco(boolean value) {
		this.setAttributeValue("ize_promocionbanco", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_promocionurl")
	@AttributeLogicalNameAttribute("ize_promocionurl")
	public String getIze_promocionurl() {
		return this.getAttributeValue("ize_promocionurl");
	}
	
	@AttributeSchemaNameAttribute("Ize_promocionurl")
	@AttributeLogicalNameAttribute("ize_promocionurl")
	public ize_promociones setIze_promocionurl(String value) {
		this.setAttributeValue("ize_promocionurl", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PropiedadClienteId")
	@AttributeLogicalNameAttribute("ize_propiedadclienteid")
	public EntityReference getIze_PropiedadClienteId() {
		return this.getAttributeValue("ize_propiedadclienteid");
	}
	
	@AttributeSchemaNameAttribute("Ize_PropiedadClienteId")
	@AttributeLogicalNameAttribute("ize_propiedadclienteid")
	public ize_promociones setIze_PropiedadClienteId(EntityReference value) {
		this.setAttributeValue("ize_propiedadclienteid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PropiedadClienteIdName")
	@AttributeLogicalNameAttribute("ize_propiedadclienteidname")
	public String getIze_PropiedadClienteIdName() {
		return this.getAttributeValue("ize_propiedadclienteidname");
	}
	
	@AttributeSchemaNameAttribute("Ize_PropiedadClienteIdName")
	@AttributeLogicalNameAttribute("ize_propiedadclienteidname")
	public ize_promociones setIze_PropiedadClienteIdName(String value) {
		this.setAttributeValue("ize_propiedadclienteidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PropiedadClienteIdYomiName")
	@AttributeLogicalNameAttribute("ize_propiedadclienteidyominame")
	public String getIze_PropiedadClienteIdYomiName() {
		return this.getAttributeValue("ize_propiedadclienteidyominame");
	}
	
	@AttributeSchemaNameAttribute("Ize_PropiedadClienteIdYomiName")
	@AttributeLogicalNameAttribute("ize_propiedadclienteidyominame")
	public ize_promociones setIze_PropiedadClienteIdYomiName(String value) {
		this.setAttributeValue("ize_propiedadclienteidyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_reservada")
	@AttributeLogicalNameAttribute("ize_reservada")
	public boolean getIze_reservada() {
		return this.getAttributeValue("ize_reservada");
	}
	
	@AttributeSchemaNameAttribute("Ize_reservada")
	@AttributeLogicalNameAttribute("ize_reservada")
	public ize_promociones setIze_reservada(boolean value) {
		this.setAttributeValue("ize_reservada", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Seguridad")
	@AttributeLogicalNameAttribute("ize_seguridad")
	public OptionSetValue getIze_Seguridad() {
		return this.getAttributeValue("ize_seguridad");
	}
	
	@AttributeSchemaNameAttribute("Ize_Seguridad")
	@AttributeLogicalNameAttribute("ize_seguridad")
	public ize_promociones setIze_Seguridad(OptionSetValue value) {
		this.setAttributeValue("ize_seguridad", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Tipo")
	@AttributeLogicalNameAttribute("ize_tipo")
	public OptionSetValue getIze_Tipo() {
		return this.getAttributeValue("ize_tipo");
	}
	
	@AttributeSchemaNameAttribute("Ize_Tipo")
	@AttributeLogicalNameAttribute("ize_tipo")
	public ize_promociones setIze_Tipo(OptionSetValue value) {
		this.setAttributeValue("ize_tipo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_TipoGestion")
	@AttributeLogicalNameAttribute("ize_tipogestion")
	public OptionSetValue getIze_TipoGestion() {
		return this.getAttributeValue("ize_tipogestion");
	}
	
	@AttributeSchemaNameAttribute("Ize_TipoGestion")
	@AttributeLogicalNameAttribute("ize_tipogestion")
	public ize_promociones setIze_TipoGestion(OptionSetValue value) {
		this.setAttributeValue("ize_tipogestion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_TipoPromocion")
	@AttributeLogicalNameAttribute("ize_tipopromocion")
	public OptionSetValue getIze_TipoPromocion() {
		return this.getAttributeValue("ize_tipopromocion");
	}
	
	@AttributeSchemaNameAttribute("Ize_TipoPromocion")
	@AttributeLogicalNameAttribute("ize_tipopromocion")
	public ize_promociones setIze_TipoPromocion(OptionSetValue value) {
		this.setAttributeValue("ize_tipopromocion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_urlgooglemaps")
	@AttributeLogicalNameAttribute("ize_urlgooglemaps")
	public String getIze_urlgooglemaps() {
		return this.getAttributeValue("ize_urlgooglemaps");
	}
	
	@AttributeSchemaNameAttribute("Ize_urlgooglemaps")
	@AttributeLogicalNameAttribute("ize_urlgooglemaps")
	public ize_promociones setIze_urlgooglemaps(String value) {
		this.setAttributeValue("ize_urlgooglemaps", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Uso")
	@AttributeLogicalNameAttribute("ize_uso")
	public OptionSetValue getIze_Uso() {
		return this.getAttributeValue("ize_uso");
	}
	
	@AttributeSchemaNameAttribute("Ize_Uso")
	@AttributeLogicalNameAttribute("ize_uso")
	public ize_promociones setIze_Uso(OptionSetValue value) {
		this.setAttributeValue("ize_uso", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Vpo")
	@AttributeLogicalNameAttribute("ize_vpo")
	public boolean getIze_Vpo() {
		return this.getAttributeValue("ize_vpo");
	}
	
	@AttributeSchemaNameAttribute("Ize_Vpo")
	@AttributeLogicalNameAttribute("ize_vpo")
	public ize_promociones setIze_Vpo(boolean value) {
		this.setAttributeValue("ize_vpo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ZonasComunesDescripcion")
	@AttributeLogicalNameAttribute("ize_zonascomunesdescripcion")
	public String getIze_ZonasComunesDescripcion() {
		return this.getAttributeValue("ize_zonascomunesdescripcion");
	}
	
	@AttributeSchemaNameAttribute("Ize_ZonasComunesDescripcion")
	@AttributeLogicalNameAttribute("ize_zonascomunesdescripcion")
	public ize_promociones setIze_ZonasComunesDescripcion(String value) {
		this.setAttributeValue("ize_zonascomunesdescripcion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ZonasInfantiles")
	@AttributeLogicalNameAttribute("ize_zonasinfantiles")
	public OptionSetValue getIze_ZonasInfantiles() {
		return this.getAttributeValue("ize_zonasinfantiles");
	}
	
	@AttributeSchemaNameAttribute("Ize_ZonasInfantiles")
	@AttributeLogicalNameAttribute("ize_zonasinfantiles")
	public ize_promociones setIze_ZonasInfantiles(OptionSetValue value) {
		this.setAttributeValue("ize_zonasinfantiles", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ZonasVerdes")
	@AttributeLogicalNameAttribute("ize_zonasverdes")
	public OptionSetValue getIze_ZonasVerdes() {
		return this.getAttributeValue("ize_zonasverdes");
	}
	
	@AttributeSchemaNameAttribute("Ize_ZonasVerdes")
	@AttributeLogicalNameAttribute("ize_zonasverdes")
	public ize_promociones setIze_ZonasVerdes(OptionSetValue value) {
		this.setAttributeValue("ize_zonasverdes", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedBy")
	@AttributeLogicalNameAttribute("modifiedby")
	public EntityReference getModifiedBy() {
		return this.getAttributeValue("modifiedby");
	}
	
	@AttributeSchemaNameAttribute("ModifiedBy")
	@AttributeLogicalNameAttribute("modifiedby")
	public ize_promociones setModifiedBy(EntityReference value) {
		this.setAttributeValue("modifiedby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedByName")
	@AttributeLogicalNameAttribute("modifiedbyname")
	public String getModifiedByName() {
		return this.getAttributeValue("modifiedbyname");
	}
	
	@AttributeSchemaNameAttribute("ModifiedByName")
	@AttributeLogicalNameAttribute("modifiedbyname")
	public ize_promociones setModifiedByName(String value) {
		this.setAttributeValue("modifiedbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedByYomiName")
	@AttributeLogicalNameAttribute("modifiedbyyominame")
	public String getModifiedByYomiName() {
		return this.getAttributeValue("modifiedbyyominame");
	}
	
	@AttributeSchemaNameAttribute("ModifiedByYomiName")
	@AttributeLogicalNameAttribute("modifiedbyyominame")
	public ize_promociones setModifiedByYomiName(String value) {
		this.setAttributeValue("modifiedbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOn")
	@AttributeLogicalNameAttribute("modifiedon")
	public Date getModifiedOn() {
		return this.getAttributeValue("modifiedon");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOn")
	@AttributeLogicalNameAttribute("modifiedon")
	public ize_promociones setModifiedOn(Date value) {
		this.setAttributeValue("modifiedon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfBy")
	@AttributeLogicalNameAttribute("modifiedonbehalfby")
	public EntityReference getModifiedOnBehalfBy() {
		return this.getAttributeValue("modifiedonbehalfby");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfBy")
	@AttributeLogicalNameAttribute("modifiedonbehalfby")
	public ize_promociones setModifiedOnBehalfBy(EntityReference value) {
		this.setAttributeValue("modifiedonbehalfby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfByName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyname")
	public String getModifiedOnBehalfByName() {
		return this.getAttributeValue("modifiedonbehalfbyname");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfByName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyname")
	public ize_promociones setModifiedOnBehalfByName(String value) {
		this.setAttributeValue("modifiedonbehalfbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyyominame")
	public String getModifiedOnBehalfByYomiName() {
		return this.getAttributeValue("modifiedonbehalfbyyominame");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyyominame")
	public ize_promociones setModifiedOnBehalfByYomiName(String value) {
		this.setAttributeValue("modifiedonbehalfbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OverriddenCreatedOn")
	@AttributeLogicalNameAttribute("overriddencreatedon")
	public Date getOverriddenCreatedOn() {
		return this.getAttributeValue("overriddencreatedon");
	}
	
	@AttributeSchemaNameAttribute("OverriddenCreatedOn")
	@AttributeLogicalNameAttribute("overriddencreatedon")
	public ize_promociones setOverriddenCreatedOn(Date value) {
		this.setAttributeValue("overriddencreatedon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwnerId")
	@AttributeLogicalNameAttribute("ownerid")
	public EntityReference getOwnerId() {
		return this.getAttributeValue("ownerid");
	}
	
	@AttributeSchemaNameAttribute("OwnerId")
	@AttributeLogicalNameAttribute("ownerid")
	public ize_promociones setOwnerId(EntityReference value) {
		this.setAttributeValue("ownerid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwnerIdName")
	@AttributeLogicalNameAttribute("owneridname")
	public String getOwnerIdName() {
		return this.getAttributeValue("owneridname");
	}
	
	@AttributeSchemaNameAttribute("OwnerIdName")
	@AttributeLogicalNameAttribute("owneridname")
	public ize_promociones setOwnerIdName(String value) {
		this.setAttributeValue("owneridname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwnerIdYomiName")
	@AttributeLogicalNameAttribute("owneridyominame")
	public String getOwnerIdYomiName() {
		return this.getAttributeValue("owneridyominame");
	}
	
	@AttributeSchemaNameAttribute("OwnerIdYomiName")
	@AttributeLogicalNameAttribute("owneridyominame")
	public ize_promociones setOwnerIdYomiName(String value) {
		this.setAttributeValue("owneridyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwningBusinessUnit")
	@AttributeLogicalNameAttribute("owningbusinessunit")
	public EntityReference getOwningBusinessUnit() {
		return this.getAttributeValue("owningbusinessunit");
	}
	
	@AttributeSchemaNameAttribute("OwningBusinessUnit")
	@AttributeLogicalNameAttribute("owningbusinessunit")
	public ize_promociones setOwningBusinessUnit(EntityReference value) {
		this.setAttributeValue("owningbusinessunit", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwningTeam")
	@AttributeLogicalNameAttribute("owningteam")
	public EntityReference getOwningTeam() {
		return this.getAttributeValue("owningteam");
	}
	
	@AttributeSchemaNameAttribute("OwningTeam")
	@AttributeLogicalNameAttribute("owningteam")
	public ize_promociones setOwningTeam(EntityReference value) {
		this.setAttributeValue("owningteam", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwningUser")
	@AttributeLogicalNameAttribute("owninguser")
	public EntityReference getOwningUser() {
		return this.getAttributeValue("owninguser");
	}
	
	@AttributeSchemaNameAttribute("OwningUser")
	@AttributeLogicalNameAttribute("owninguser")
	public ize_promociones setOwningUser(EntityReference value) {
		this.setAttributeValue("owninguser", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Processid")
	@AttributeLogicalNameAttribute("processid")
	public UUID getProcessid() {
		return this.getAttributeValue("processid");
	}
	
	@AttributeSchemaNameAttribute("Processid")
	@AttributeLogicalNameAttribute("processid")
	public ize_promociones setProcessid(UUID value) {
		this.setAttributeValue("processid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Stageid")
	@AttributeLogicalNameAttribute("stageid")
	public UUID getStageid() {
		return this.getAttributeValue("stageid");
	}
	
	@AttributeSchemaNameAttribute("Stageid")
	@AttributeLogicalNameAttribute("stageid")
	public ize_promociones setStageid(UUID value) {
		this.setAttributeValue("stageid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Statecode")
	@AttributeLogicalNameAttribute("statecode")
	public OptionSetValue getStatecode() {
		return this.getAttributeValue("statecode");
	}
	
	@AttributeSchemaNameAttribute("Statecode")
	@AttributeLogicalNameAttribute("statecode")
	public ize_promociones setStatecode(OptionSetValue value) {
		this.setAttributeValue("statecode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Statuscode")
	@AttributeLogicalNameAttribute("statuscode")
	public OptionSetValue getStatuscode() {
		return this.getAttributeValue("statuscode");
	}
	
	@AttributeSchemaNameAttribute("Statuscode")
	@AttributeLogicalNameAttribute("statuscode")
	public ize_promociones setStatuscode(OptionSetValue value) {
		this.setAttributeValue("statuscode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("TimeZoneRuleVersionNumber")
	@AttributeLogicalNameAttribute("timezoneruleversionnumber")
	public int getTimeZoneRuleVersionNumber() {
		return this.getAttributeValue("timezoneruleversionnumber");
	}
	
	@AttributeSchemaNameAttribute("TimeZoneRuleVersionNumber")
	@AttributeLogicalNameAttribute("timezoneruleversionnumber")
	public ize_promociones setTimeZoneRuleVersionNumber(int value) {
		this.setAttributeValue("timezoneruleversionnumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Traversedpath")
	@AttributeLogicalNameAttribute("traversedpath")
	public String getTraversedpath() {
		return this.getAttributeValue("traversedpath");
	}
	
	@AttributeSchemaNameAttribute("Traversedpath")
	@AttributeLogicalNameAttribute("traversedpath")
	public ize_promociones setTraversedpath(String value) {
		this.setAttributeValue("traversedpath", value);
		return this;
	}

	@AttributeSchemaNameAttribute("UTCConversionTimeZoneCode")
	@AttributeLogicalNameAttribute("utcconversiontimezonecode")
	public int getUTCConversionTimeZoneCode() {
		return this.getAttributeValue("utcconversiontimezonecode");
	}
	
	@AttributeSchemaNameAttribute("UTCConversionTimeZoneCode")
	@AttributeLogicalNameAttribute("utcconversiontimezonecode")
	public ize_promociones setUTCConversionTimeZoneCode(int value) {
		this.setAttributeValue("utcconversiontimezonecode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("VersionNumber")
	@AttributeLogicalNameAttribute("versionnumber")
	public BigInteger getVersionNumber() {
		return this.getAttributeValue("versionnumber");
	}
	
	@AttributeSchemaNameAttribute("VersionNumber")
	@AttributeLogicalNameAttribute("versionnumber")
	public ize_promociones setVersionNumber(BigInteger value) {
		this.setAttributeValue("versionnumber", value);
		return this;
	}

}

