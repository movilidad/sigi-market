// Mobile Model Generator for Dynamics CRM 1.0
//
// Copyright (c) Microsoft Corporation
//
// All rights reserved.
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the ""Software""), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//  ize_subtipoloologia.java
package com.microsoft.xrm.sdk.client.ize;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;
import com.microsoft.xrm.sdk.*;


public enum ize_subtipoloologia {
	Piso(274040000),
	Apartamento(274040001),
	Atico(274040002),
	Casaadosada(274040003),
	Chalet(274040004),
	Duplex(274040006),
	Estudio(274040007),
	Fincarustica(274040008),
	Plantabaja(274040009),
	Diafana(274040020),
	Tabicada(274040021),
	Comerciobarrio(274040005),
	Enclaveestrategico(274040010),
	Localenrentabilidad(274040011),
	Centrocomercial(274040012),
	lineacomercial1(274040013),
	lineacomercial2(274040014),
	lineacomercial3(274040015),
	Urbano(274040016),
	Urbanizable(274040017),
	Nourbanizable(274040018),
	Coche(274040019),
	Doble(274040022),
	Moto(274040023);
	private int mValue;

	ize_subtipoloologia(int value) {
		mValue = value;
	}

	public int getValue() {
		return mValue;
	}

	public static ize_subtipoloologia toObject(int value) {
		switch(value) {
			case 274040000:
				return Piso;
			case 274040001:
				return Apartamento;
			case 274040002:
				return Atico;
			case 274040003:
				return Casaadosada;
			case 274040004:
				return Chalet;
			case 274040006:
				return Duplex;
			case 274040007:
				return Estudio;
			case 274040008:
				return Fincarustica;
			case 274040009:
				return Plantabaja;
			case 274040020:
				return Diafana;
			case 274040021:
				return Tabicada;
			case 274040005:
				return Comerciobarrio;
			case 274040010:
				return Enclaveestrategico;
			case 274040011:
				return Localenrentabilidad;
			case 274040012:
				return Centrocomercial;
			case 274040013:
				return lineacomercial1;
			case 274040014:
				return lineacomercial2;
			case 274040015:
				return lineacomercial3;
			case 274040016:
				return Urbano;
			case 274040017:
				return Urbanizable;
			case 274040018:
				return Nourbanizable;
			case 274040019:
				return Coche;
			case 274040022:
				return Doble;
			case 274040023:
				return Moto;
			default: 
				return null;
		}
	}
}

