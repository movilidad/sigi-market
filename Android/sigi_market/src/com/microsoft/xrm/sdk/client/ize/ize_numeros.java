package com.microsoft.xrm.sdk.client.ize;
// Mobile Model Generator for Dynamics CRM 1.0
//
// Copyright (c) Microsoft Corporation
//
// All rights reserved.
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the ""Software""), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//  ize_numeros.java

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;
import com.microsoft.xrm.sdk.*;


public enum ize_numeros {
	Cero(274040000,0),
	Uno(274040001,1),
	Dos(274040002,2),
	Tres(274040003,3),
	Cuatro(274040004,4),
	Cinco(274040005,5),
	Seis(274040006,6),
	Siete(274040007,7),
	Ocho(274040008,8),
	Nueve(274040009,9),
	Diez(274040010,10),
	Once(274040011,11),
	Doce(274040012,12),
	Trece(274040013,13),
	Catorce(274040014,14),
	Quince(274040015,15),
	Dieciseis(274040016,16),
	Diecisiete(274040017,17),
	Dieciocho(274040018,18),
	Diecinueve(274040019,19);
	/*0(274040000),
	1(274040001),
	2(274040002),
	3(274040003),
	4(274040004),
	5(274040005),
	6(274040006),
	7(274040007),
	9(274040008),
	10(274040009),
	11(274040010),
	12(274040011),
	13(274040012),
	14(274040013),
	15(274040014),
	16(274040015),
	17(274040016),
	18(274040017),
	19(274040018),
	20(274040019);*/
	private int mValue;
	private int mValue_num;

	ize_numeros(int value) {
		mValue = value;
		
	}
	
	ize_numeros(int value, int value_num) {
		mValue = value;
		mValue_num = value_num;
		
	}

	public int getValue() {
		return mValue;
		
	}
	
	

	public int getmValue_num() {
		return mValue_num;
	}

	public void setmValue_num(int mValue_num) {
		this.mValue_num = mValue_num;
	}

	//public static ize_numeros toObject(int value) {
	public static int toObject(int value) {
		switch(value) {
			case 274040000:
				return Cero.mValue_num;
			case 274040001:
				return Uno.mValue_num;
			case 274040002:
				return Dos.mValue_num;
			case 274040003:
				return Tres.mValue_num;
			case 274040004:
				return Cuatro.mValue_num;
			case 274040005:
				return Cinco.mValue_num;
			case 274040006:
				return Seis.mValue_num;
			case 274040007:
				return Siete.mValue_num;
			case 274040008:
				return Ocho.mValue_num;
			case 274040009:
				return Nueve.mValue_num;
			case 274040010:
				return Diez.mValue_num;
			case 274040011:
				return Once.mValue_num;
			case 274040012:
				return Doce.mValue_num;
			case 274040013:
				return Trece.mValue_num;
			case 274040014:
				return Catorce.mValue_num;
			case 274040015:
				return Quince.mValue_num;
			case 274040016:
				return Dieciseis.mValue_num;
			case 274040017:
				return Diecisiete.mValue_num;
			case 274040018:
				return Dieciocho.mValue_num;
			case 274040019:
				return Diecinueve.mValue_num;
			default: 
				return -1;
		}
	}
}

