package com.microsoft.xrm.sdk.client.ize;
// Mobile Model Generator for Dynamics CRM 1.0
//
// Copyright (c) Microsoft Corporation
//
// All rights reserved.
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the ""Software""), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//  ize_videos.java

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;
import com.microsoft.xrm.sdk.*;


@EntityLogicalNameAttribute("ize_videos")
public class ize_videos extends Entity {
	
	public static final String EntityLogicalName = "ize_videos";
	public static final int EntityTypeCode = 10022;
	
	public ize_videos() {
		super("ize_videos");
	}

	public static ize_videos build() {
		return new ize_videos();
	}

	@AttributeSchemaNameAttribute("ize_videosId")
	@AttributeLogicalNameAttribute("ize_videosid")
	public UUID getize_videosId() {
		return this.getAttributeValue("ize_videosid");
	}
	
	@AttributeSchemaNameAttribute("ize_videosId")
	@AttributeLogicalNameAttribute("ize_videosid")
	public ize_videos setize_videosId(UUID value) {
		this.setAttributeValue("ize_videosid", value);
		if (value != null) {
			super.setId(value);
		}
		else {
			super.setId(new UUID(0L, 0L));
		}

		return this;
	}
	
	@Override
	@AttributeSchemaNameAttribute("ize_videosId")
	@AttributeLogicalNameAttribute("ize_videosid")
	public UUID getId() {
		return super.getId();
	}
	
	@Override
	@AttributeSchemaNameAttribute("ize_videosId")
	@AttributeLogicalNameAttribute("ize_videosid")
	public void setId(UUID value) {
		this.setize_videosId(value);
	}
	

	@AttributeSchemaNameAttribute("CreatedBy")
	@AttributeLogicalNameAttribute("createdby")
	public EntityReference getCreatedBy() {
		return this.getAttributeValue("createdby");
	}
	
	@AttributeSchemaNameAttribute("CreatedBy")
	@AttributeLogicalNameAttribute("createdby")
	public ize_videos setCreatedBy(EntityReference value) {
		this.setAttributeValue("createdby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedByName")
	@AttributeLogicalNameAttribute("createdbyname")
	public String getCreatedByName() {
		return this.getAttributeValue("createdbyname");
	}
	
	@AttributeSchemaNameAttribute("CreatedByName")
	@AttributeLogicalNameAttribute("createdbyname")
	public ize_videos setCreatedByName(String value) {
		this.setAttributeValue("createdbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedByYomiName")
	@AttributeLogicalNameAttribute("createdbyyominame")
	public String getCreatedByYomiName() {
		return this.getAttributeValue("createdbyyominame");
	}
	
	@AttributeSchemaNameAttribute("CreatedByYomiName")
	@AttributeLogicalNameAttribute("createdbyyominame")
	public ize_videos setCreatedByYomiName(String value) {
		this.setAttributeValue("createdbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOn")
	@AttributeLogicalNameAttribute("createdon")
	public Date getCreatedOn() {
		return this.getAttributeValue("createdon");
	}
	
	@AttributeSchemaNameAttribute("CreatedOn")
	@AttributeLogicalNameAttribute("createdon")
	public ize_videos setCreatedOn(Date value) {
		this.setAttributeValue("createdon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfBy")
	@AttributeLogicalNameAttribute("createdonbehalfby")
	public EntityReference getCreatedOnBehalfBy() {
		return this.getAttributeValue("createdonbehalfby");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfBy")
	@AttributeLogicalNameAttribute("createdonbehalfby")
	public ize_videos setCreatedOnBehalfBy(EntityReference value) {
		this.setAttributeValue("createdonbehalfby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfByName")
	@AttributeLogicalNameAttribute("createdonbehalfbyname")
	public String getCreatedOnBehalfByName() {
		return this.getAttributeValue("createdonbehalfbyname");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfByName")
	@AttributeLogicalNameAttribute("createdonbehalfbyname")
	public ize_videos setCreatedOnBehalfByName(String value) {
		this.setAttributeValue("createdonbehalfbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("createdonbehalfbyyominame")
	public String getCreatedOnBehalfByYomiName() {
		return this.getAttributeValue("createdonbehalfbyyominame");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("createdonbehalfbyyominame")
	public ize_videos setCreatedOnBehalfByYomiName(String value) {
		this.setAttributeValue("createdonbehalfbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ImportSequenceNumber")
	@AttributeLogicalNameAttribute("importsequencenumber")
	public int getImportSequenceNumber() {
		return this.getAttributeValue("importsequencenumber");
	}
	
	@AttributeSchemaNameAttribute("ImportSequenceNumber")
	@AttributeLogicalNameAttribute("importsequencenumber")
	public ize_videos setImportSequenceNumber(int value) {
		this.setAttributeValue("importsequencenumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_name")
	@AttributeLogicalNameAttribute("ize_name")
	public String getIze_name() {
		return this.getAttributeValue("ize_name");
	}
	
	@AttributeSchemaNameAttribute("Ize_name")
	@AttributeLogicalNameAttribute("ize_name")
	public ize_videos setIze_name(String value) {
		this.setAttributeValue("ize_name", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PromocinFaseId")
	@AttributeLogicalNameAttribute("ize_promocinfaseid")
	public EntityReference getIze_PromocinFaseId() {
		return this.getAttributeValue("ize_promocinfaseid");
	}
	
	@AttributeSchemaNameAttribute("Ize_PromocinFaseId")
	@AttributeLogicalNameAttribute("ize_promocinfaseid")
	public ize_videos setIze_PromocinFaseId(EntityReference value) {
		this.setAttributeValue("ize_promocinfaseid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PromocinFaseIdName")
	@AttributeLogicalNameAttribute("ize_promocinfaseidname")
	public String getIze_PromocinFaseIdName() {
		return this.getAttributeValue("ize_promocinfaseidname");
	}
	
	@AttributeSchemaNameAttribute("Ize_PromocinFaseIdName")
	@AttributeLogicalNameAttribute("ize_promocinfaseidname")
	public ize_videos setIze_PromocinFaseIdName(String value) {
		this.setAttributeValue("ize_promocinfaseidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_VideoPrincipal")
	@AttributeLogicalNameAttribute("ize_videoprincipal")
	public OptionSetValue getIze_VideoPrincipal() {
		return this.getAttributeValue("ize_videoprincipal");
	}
	
	@AttributeSchemaNameAttribute("Ize_VideoPrincipal")
	@AttributeLogicalNameAttribute("ize_videoprincipal")
	public ize_videos setIze_VideoPrincipal(OptionSetValue value) {
		this.setAttributeValue("ize_videoprincipal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_VideosIdinm")
	@AttributeLogicalNameAttribute("ize_videosidinm")
	public EntityReference getIze_VideosIdinm() {
		return this.getAttributeValue("ize_videosidinm");
	}
	
	@AttributeSchemaNameAttribute("Ize_VideosIdinm")
	@AttributeLogicalNameAttribute("ize_videosidinm")
	public ize_videos setIze_VideosIdinm(EntityReference value) {
		this.setAttributeValue("ize_videosidinm", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_VideosIdinmName")
	@AttributeLogicalNameAttribute("ize_videosidinmname")
	public String getIze_VideosIdinmName() {
		return this.getAttributeValue("ize_videosidinmname");
	}
	
	@AttributeSchemaNameAttribute("Ize_VideosIdinmName")
	@AttributeLogicalNameAttribute("ize_videosidinmname")
	public ize_videos setIze_VideosIdinmName(String value) {
		this.setAttributeValue("ize_videosidinmname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_VideoUrl")
	@AttributeLogicalNameAttribute("ize_videourl")
	public String getIze_VideoUrl() {
		return this.getAttributeValue("ize_videourl");
	}
	
	@AttributeSchemaNameAttribute("Ize_VideoUrl")
	@AttributeLogicalNameAttribute("ize_videourl")
	public ize_videos setIze_VideoUrl(String value) {
		this.setAttributeValue("ize_videourl", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedBy")
	@AttributeLogicalNameAttribute("modifiedby")
	public EntityReference getModifiedBy() {
		return this.getAttributeValue("modifiedby");
	}
	
	@AttributeSchemaNameAttribute("ModifiedBy")
	@AttributeLogicalNameAttribute("modifiedby")
	public ize_videos setModifiedBy(EntityReference value) {
		this.setAttributeValue("modifiedby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedByName")
	@AttributeLogicalNameAttribute("modifiedbyname")
	public String getModifiedByName() {
		return this.getAttributeValue("modifiedbyname");
	}
	
	@AttributeSchemaNameAttribute("ModifiedByName")
	@AttributeLogicalNameAttribute("modifiedbyname")
	public ize_videos setModifiedByName(String value) {
		this.setAttributeValue("modifiedbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedByYomiName")
	@AttributeLogicalNameAttribute("modifiedbyyominame")
	public String getModifiedByYomiName() {
		return this.getAttributeValue("modifiedbyyominame");
	}
	
	@AttributeSchemaNameAttribute("ModifiedByYomiName")
	@AttributeLogicalNameAttribute("modifiedbyyominame")
	public ize_videos setModifiedByYomiName(String value) {
		this.setAttributeValue("modifiedbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOn")
	@AttributeLogicalNameAttribute("modifiedon")
	public Date getModifiedOn() {
		return this.getAttributeValue("modifiedon");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOn")
	@AttributeLogicalNameAttribute("modifiedon")
	public ize_videos setModifiedOn(Date value) {
		this.setAttributeValue("modifiedon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfBy")
	@AttributeLogicalNameAttribute("modifiedonbehalfby")
	public EntityReference getModifiedOnBehalfBy() {
		return this.getAttributeValue("modifiedonbehalfby");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfBy")
	@AttributeLogicalNameAttribute("modifiedonbehalfby")
	public ize_videos setModifiedOnBehalfBy(EntityReference value) {
		this.setAttributeValue("modifiedonbehalfby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfByName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyname")
	public String getModifiedOnBehalfByName() {
		return this.getAttributeValue("modifiedonbehalfbyname");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfByName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyname")
	public ize_videos setModifiedOnBehalfByName(String value) {
		this.setAttributeValue("modifiedonbehalfbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyyominame")
	public String getModifiedOnBehalfByYomiName() {
		return this.getAttributeValue("modifiedonbehalfbyyominame");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyyominame")
	public ize_videos setModifiedOnBehalfByYomiName(String value) {
		this.setAttributeValue("modifiedonbehalfbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OverriddenCreatedOn")
	@AttributeLogicalNameAttribute("overriddencreatedon")
	public Date getOverriddenCreatedOn() {
		return this.getAttributeValue("overriddencreatedon");
	}
	
	@AttributeSchemaNameAttribute("OverriddenCreatedOn")
	@AttributeLogicalNameAttribute("overriddencreatedon")
	public ize_videos setOverriddenCreatedOn(Date value) {
		this.setAttributeValue("overriddencreatedon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwnerId")
	@AttributeLogicalNameAttribute("ownerid")
	public EntityReference getOwnerId() {
		return this.getAttributeValue("ownerid");
	}
	
	@AttributeSchemaNameAttribute("OwnerId")
	@AttributeLogicalNameAttribute("ownerid")
	public ize_videos setOwnerId(EntityReference value) {
		this.setAttributeValue("ownerid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwnerIdName")
	@AttributeLogicalNameAttribute("owneridname")
	public String getOwnerIdName() {
		return this.getAttributeValue("owneridname");
	}
	
	@AttributeSchemaNameAttribute("OwnerIdName")
	@AttributeLogicalNameAttribute("owneridname")
	public ize_videos setOwnerIdName(String value) {
		this.setAttributeValue("owneridname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwnerIdYomiName")
	@AttributeLogicalNameAttribute("owneridyominame")
	public String getOwnerIdYomiName() {
		return this.getAttributeValue("owneridyominame");
	}
	
	@AttributeSchemaNameAttribute("OwnerIdYomiName")
	@AttributeLogicalNameAttribute("owneridyominame")
	public ize_videos setOwnerIdYomiName(String value) {
		this.setAttributeValue("owneridyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwningBusinessUnit")
	@AttributeLogicalNameAttribute("owningbusinessunit")
	public EntityReference getOwningBusinessUnit() {
		return this.getAttributeValue("owningbusinessunit");
	}
	
	@AttributeSchemaNameAttribute("OwningBusinessUnit")
	@AttributeLogicalNameAttribute("owningbusinessunit")
	public ize_videos setOwningBusinessUnit(EntityReference value) {
		this.setAttributeValue("owningbusinessunit", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwningTeam")
	@AttributeLogicalNameAttribute("owningteam")
	public EntityReference getOwningTeam() {
		return this.getAttributeValue("owningteam");
	}
	
	@AttributeSchemaNameAttribute("OwningTeam")
	@AttributeLogicalNameAttribute("owningteam")
	public ize_videos setOwningTeam(EntityReference value) {
		this.setAttributeValue("owningteam", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OwningUser")
	@AttributeLogicalNameAttribute("owninguser")
	public EntityReference getOwningUser() {
		return this.getAttributeValue("owninguser");
	}
	
	@AttributeSchemaNameAttribute("OwningUser")
	@AttributeLogicalNameAttribute("owninguser")
	public ize_videos setOwningUser(EntityReference value) {
		this.setAttributeValue("owninguser", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Statecode")
	@AttributeLogicalNameAttribute("statecode")
	public OptionSetValue getStatecode() {
		return this.getAttributeValue("statecode");
	}
	
	@AttributeSchemaNameAttribute("Statecode")
	@AttributeLogicalNameAttribute("statecode")
	public ize_videos setStatecode(OptionSetValue value) {
		this.setAttributeValue("statecode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Statuscode")
	@AttributeLogicalNameAttribute("statuscode")
	public OptionSetValue getStatuscode() {
		return this.getAttributeValue("statuscode");
	}
	
	@AttributeSchemaNameAttribute("Statuscode")
	@AttributeLogicalNameAttribute("statuscode")
	public ize_videos setStatuscode(OptionSetValue value) {
		this.setAttributeValue("statuscode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("TimeZoneRuleVersionNumber")
	@AttributeLogicalNameAttribute("timezoneruleversionnumber")
	public int getTimeZoneRuleVersionNumber() {
		return this.getAttributeValue("timezoneruleversionnumber");
	}
	
	@AttributeSchemaNameAttribute("TimeZoneRuleVersionNumber")
	@AttributeLogicalNameAttribute("timezoneruleversionnumber")
	public ize_videos setTimeZoneRuleVersionNumber(int value) {
		this.setAttributeValue("timezoneruleversionnumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("UTCConversionTimeZoneCode")
	@AttributeLogicalNameAttribute("utcconversiontimezonecode")
	public int getUTCConversionTimeZoneCode() {
		return this.getAttributeValue("utcconversiontimezonecode");
	}
	
	@AttributeSchemaNameAttribute("UTCConversionTimeZoneCode")
	@AttributeLogicalNameAttribute("utcconversiontimezonecode")
	public ize_videos setUTCConversionTimeZoneCode(int value) {
		this.setAttributeValue("utcconversiontimezonecode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("VersionNumber")
	@AttributeLogicalNameAttribute("versionnumber")
	public BigInteger getVersionNumber() {
		return this.getAttributeValue("versionnumber");
	}
	
	@AttributeSchemaNameAttribute("VersionNumber")
	@AttributeLogicalNameAttribute("versionnumber")
	public ize_videos setVersionNumber(BigInteger value) {
		this.setAttributeValue("versionnumber", value);
		return this;
	}

}

