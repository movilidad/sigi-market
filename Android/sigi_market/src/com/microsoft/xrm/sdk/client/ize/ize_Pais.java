package com.microsoft.xrm.sdk.client.ize;
// Mobile Model Generator for Dynamics CRM 1.0
//
// Copyright (c) Microsoft Corporation
//
// All rights reserved.
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the ""Software""), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//  ize_Pais.java

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;
import com.microsoft.xrm.sdk.*;


@EntityLogicalNameAttribute("ize_pais")
public class ize_Pais extends Entity {
	
	public static final String EntityLogicalName = "ize_pais";
	public static final int EntityTypeCode = 10017;
	
	public ize_Pais() {
		super("ize_pais");
	}

	public static ize_Pais build() {
		return new ize_Pais();
	}

	@AttributeSchemaNameAttribute("ize_PaisId")
	@AttributeLogicalNameAttribute("ize_paisid")
	public UUID getize_PaisId() {
		return this.getAttributeValue("ize_paisid");
	}
	
	@AttributeSchemaNameAttribute("ize_PaisId")
	@AttributeLogicalNameAttribute("ize_paisid")
	public ize_Pais setize_PaisId(UUID value) {
		this.setAttributeValue("ize_paisid", value);
		if (value != null) {
			super.setId(value);
		}
		else {
			super.setId(new UUID(0L, 0L));
		}

		return this;
	}
	
	@Override
	@AttributeSchemaNameAttribute("ize_PaisId")
	@AttributeLogicalNameAttribute("ize_paisid")
	public UUID getId() {
		return super.getId();
	}
	
	@Override
	@AttributeSchemaNameAttribute("ize_PaisId")
	@AttributeLogicalNameAttribute("ize_paisid")
	public void setId(UUID value) {
		this.setize_PaisId(value);
	}
	

	@AttributeSchemaNameAttribute("CreatedBy")
	@AttributeLogicalNameAttribute("createdby")
	public EntityReference getCreatedBy() {
		return this.getAttributeValue("createdby");
	}
	
	@AttributeSchemaNameAttribute("CreatedBy")
	@AttributeLogicalNameAttribute("createdby")
	public ize_Pais setCreatedBy(EntityReference value) {
		this.setAttributeValue("createdby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedByName")
	@AttributeLogicalNameAttribute("createdbyname")
	public String getCreatedByName() {
		return this.getAttributeValue("createdbyname");
	}
	
	@AttributeSchemaNameAttribute("CreatedByName")
	@AttributeLogicalNameAttribute("createdbyname")
	public ize_Pais setCreatedByName(String value) {
		this.setAttributeValue("createdbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedByYomiName")
	@AttributeLogicalNameAttribute("createdbyyominame")
	public String getCreatedByYomiName() {
		return this.getAttributeValue("createdbyyominame");
	}
	
	@AttributeSchemaNameAttribute("CreatedByYomiName")
	@AttributeLogicalNameAttribute("createdbyyominame")
	public ize_Pais setCreatedByYomiName(String value) {
		this.setAttributeValue("createdbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOn")
	@AttributeLogicalNameAttribute("createdon")
	public Date getCreatedOn() {
		return this.getAttributeValue("createdon");
	}
	
	@AttributeSchemaNameAttribute("CreatedOn")
	@AttributeLogicalNameAttribute("createdon")
	public ize_Pais setCreatedOn(Date value) {
		this.setAttributeValue("createdon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfBy")
	@AttributeLogicalNameAttribute("createdonbehalfby")
	public EntityReference getCreatedOnBehalfBy() {
		return this.getAttributeValue("createdonbehalfby");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfBy")
	@AttributeLogicalNameAttribute("createdonbehalfby")
	public ize_Pais setCreatedOnBehalfBy(EntityReference value) {
		this.setAttributeValue("createdonbehalfby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfByName")
	@AttributeLogicalNameAttribute("createdonbehalfbyname")
	public String getCreatedOnBehalfByName() {
		return this.getAttributeValue("createdonbehalfbyname");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfByName")
	@AttributeLogicalNameAttribute("createdonbehalfbyname")
	public ize_Pais setCreatedOnBehalfByName(String value) {
		this.setAttributeValue("createdonbehalfbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("createdonbehalfbyyominame")
	public String getCreatedOnBehalfByYomiName() {
		return this.getAttributeValue("createdonbehalfbyyominame");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("createdonbehalfbyyominame")
	public ize_Pais setCreatedOnBehalfByYomiName(String value) {
		this.setAttributeValue("createdonbehalfbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ImportSequenceNumber")
	@AttributeLogicalNameAttribute("importsequencenumber")
	public int getImportSequenceNumber() {
		return this.getAttributeValue("importsequencenumber");
	}
	
	@AttributeSchemaNameAttribute("ImportSequenceNumber")
	@AttributeLogicalNameAttribute("importsequencenumber")
	public ize_Pais setImportSequenceNumber(int value) {
		this.setAttributeValue("importsequencenumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_name")
	@AttributeLogicalNameAttribute("ize_name")
	public String getIze_name() {
		return this.getAttributeValue("ize_name");
	}
	
	@AttributeSchemaNameAttribute("Ize_name")
	@AttributeLogicalNameAttribute("ize_name")
	public ize_Pais setIze_name(String value) {
		this.setAttributeValue("ize_name", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedBy")
	@AttributeLogicalNameAttribute("modifiedby")
	public EntityReference getModifiedBy() {
		return this.getAttributeValue("modifiedby");
	}
	
	@AttributeSchemaNameAttribute("ModifiedBy")
	@AttributeLogicalNameAttribute("modifiedby")
	public ize_Pais setModifiedBy(EntityReference value) {
		this.setAttributeValue("modifiedby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedByName")
	@AttributeLogicalNameAttribute("modifiedbyname")
	public String getModifiedByName() {
		return this.getAttributeValue("modifiedbyname");
	}
	
	@AttributeSchemaNameAttribute("ModifiedByName")
	@AttributeLogicalNameAttribute("modifiedbyname")
	public ize_Pais setModifiedByName(String value) {
		this.setAttributeValue("modifiedbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedByYomiName")
	@AttributeLogicalNameAttribute("modifiedbyyominame")
	public String getModifiedByYomiName() {
		return this.getAttributeValue("modifiedbyyominame");
	}
	
	@AttributeSchemaNameAttribute("ModifiedByYomiName")
	@AttributeLogicalNameAttribute("modifiedbyyominame")
	public ize_Pais setModifiedByYomiName(String value) {
		this.setAttributeValue("modifiedbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOn")
	@AttributeLogicalNameAttribute("modifiedon")
	public Date getModifiedOn() {
		return this.getAttributeValue("modifiedon");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOn")
	@AttributeLogicalNameAttribute("modifiedon")
	public ize_Pais setModifiedOn(Date value) {
		this.setAttributeValue("modifiedon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfBy")
	@AttributeLogicalNameAttribute("modifiedonbehalfby")
	public EntityReference getModifiedOnBehalfBy() {
		return this.getAttributeValue("modifiedonbehalfby");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfBy")
	@AttributeLogicalNameAttribute("modifiedonbehalfby")
	public ize_Pais setModifiedOnBehalfBy(EntityReference value) {
		this.setAttributeValue("modifiedonbehalfby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfByName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyname")
	public String getModifiedOnBehalfByName() {
		return this.getAttributeValue("modifiedonbehalfbyname");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfByName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyname")
	public ize_Pais setModifiedOnBehalfByName(String value) {
		this.setAttributeValue("modifiedonbehalfbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyyominame")
	public String getModifiedOnBehalfByYomiName() {
		return this.getAttributeValue("modifiedonbehalfbyyominame");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyyominame")
	public ize_Pais setModifiedOnBehalfByYomiName(String value) {
		this.setAttributeValue("modifiedonbehalfbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OrganizationId")
	@AttributeLogicalNameAttribute("organizationid")
	public EntityReference getOrganizationId() {
		return this.getAttributeValue("organizationid");
	}
	
	@AttributeSchemaNameAttribute("OrganizationId")
	@AttributeLogicalNameAttribute("organizationid")
	public ize_Pais setOrganizationId(EntityReference value) {
		this.setAttributeValue("organizationid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OrganizationIdName")
	@AttributeLogicalNameAttribute("organizationidname")
	public String getOrganizationIdName() {
		return this.getAttributeValue("organizationidname");
	}
	
	@AttributeSchemaNameAttribute("OrganizationIdName")
	@AttributeLogicalNameAttribute("organizationidname")
	public ize_Pais setOrganizationIdName(String value) {
		this.setAttributeValue("organizationidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OverriddenCreatedOn")
	@AttributeLogicalNameAttribute("overriddencreatedon")
	public Date getOverriddenCreatedOn() {
		return this.getAttributeValue("overriddencreatedon");
	}
	
	@AttributeSchemaNameAttribute("OverriddenCreatedOn")
	@AttributeLogicalNameAttribute("overriddencreatedon")
	public ize_Pais setOverriddenCreatedOn(Date value) {
		this.setAttributeValue("overriddencreatedon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Statecode")
	@AttributeLogicalNameAttribute("statecode")
	public OptionSetValue getStatecode() {
		return this.getAttributeValue("statecode");
	}
	
	@AttributeSchemaNameAttribute("Statecode")
	@AttributeLogicalNameAttribute("statecode")
	public ize_Pais setStatecode(OptionSetValue value) {
		this.setAttributeValue("statecode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Statuscode")
	@AttributeLogicalNameAttribute("statuscode")
	public OptionSetValue getStatuscode() {
		return this.getAttributeValue("statuscode");
	}
	
	@AttributeSchemaNameAttribute("Statuscode")
	@AttributeLogicalNameAttribute("statuscode")
	public ize_Pais setStatuscode(OptionSetValue value) {
		this.setAttributeValue("statuscode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("TimeZoneRuleVersionNumber")
	@AttributeLogicalNameAttribute("timezoneruleversionnumber")
	public int getTimeZoneRuleVersionNumber() {
		return this.getAttributeValue("timezoneruleversionnumber");
	}
	
	@AttributeSchemaNameAttribute("TimeZoneRuleVersionNumber")
	@AttributeLogicalNameAttribute("timezoneruleversionnumber")
	public ize_Pais setTimeZoneRuleVersionNumber(int value) {
		this.setAttributeValue("timezoneruleversionnumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("UTCConversionTimeZoneCode")
	@AttributeLogicalNameAttribute("utcconversiontimezonecode")
	public int getUTCConversionTimeZoneCode() {
		return this.getAttributeValue("utcconversiontimezonecode");
	}
	
	@AttributeSchemaNameAttribute("UTCConversionTimeZoneCode")
	@AttributeLogicalNameAttribute("utcconversiontimezonecode")
	public ize_Pais setUTCConversionTimeZoneCode(int value) {
		this.setAttributeValue("utcconversiontimezonecode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("VersionNumber")
	@AttributeLogicalNameAttribute("versionnumber")
	public BigInteger getVersionNumber() {
		return this.getAttributeValue("versionnumber");
	}
	
	@AttributeSchemaNameAttribute("VersionNumber")
	@AttributeLogicalNameAttribute("versionnumber")
	public ize_Pais setVersionNumber(BigInteger value) {
		this.setAttributeValue("versionnumber", value);
		return this;
	}

}

