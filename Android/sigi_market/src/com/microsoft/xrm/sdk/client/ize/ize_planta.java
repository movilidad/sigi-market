package com.microsoft.xrm.sdk.client.ize;
// Mobile Model Generator for Dynamics CRM 1.0
//
// Copyright (c) Microsoft Corporation
//
// All rights reserved.
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the ""Software""), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//  ize_planta.java

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;
import com.microsoft.xrm.sdk.*;


public enum ize_planta {
	Bajo(274040000),
	Principal(274040001),
	Entresuelo(274040002),
	�tico(274040003),
	Primero(274040004),
	Segundo(274040005),
	Tercero(274040006),
	Cuarto(274040007),
	Quinto(274040008),
	Sexto(274040009),
	S�ptimo(274040010),
	Octavo(274040011),
	Noveno(274040012),
	D�cimo(274040013),
	Und�cimo(274040014),	
	Duod�cimo(274040015),
	Decimotercero(274040016),
	Decimocuarto(274040017),
	Decimoquinto(274040018),
	Decimosexto(274040019),
	Decimos�ptimo(274040020),
	Decimoctavo(274040021),
	Decimonoveno(274040022),
	Vig�simo(274040023);
	/*1º(274040004),
	2º(274040005),
	3º(274040006),
	4º(274040007),
	5º(274040008),
	6º(274040009),
	7º(274040010),
	8º(274040011),
	9º(274040012),
	10º(274040013),
	11º(274040014),
	12º(274040015),
	13º(274040016),
	14º(274040017),
	15º(274040018),
	16º(274040019),
	17º(274040020),
	18º(274040021),
	19º(274040022),
	20º(274040023);*/
	private int mValue;

	ize_planta(int value) {
		mValue = value;
	}

	public int getValue() {
		return mValue;
	}

	public static ize_planta toObject(int value) {
		switch(value) {
			case 274040000:
				return Bajo;
			case 274040001:
				return Principal;
			case 274040002:
				return Entresuelo;
			case 274040003:
				return �tico;
			case 274040004:
				return Primero;
			case 274040005:
				return Segundo;
			case 274040006:
				return Tercero;
			case 274040007:
				return Cuarto;
			case 274040008:
				return Quinto;
			case 274040009:
				return Sexto;
			case 274040010:
				return S�ptimo;
			case 274040011:
				return Octavo;
			case 274040012:
				return Noveno;
			case 274040013:
				return D�cimo;
			case 274040014:
				return Und�cimo;
			case 274040015:
				return Duod�cimo;
			case 274040016:
				return Decimotercero;
			case 274040017:
				return Decimocuarto;
			case 274040018:
				return Decimoquinto;
			case 274040019:
				return Decimosexto;
			case 274040020:
				return Decimos�ptimo;
			case 274040021:
				return Decimoctavo;
			case 274040022:
				return Decimonoveno;
			case 274040023:
				return Vig�simo;
			default: 
				return null;
		}
	}
}

