// Mobile Model Generator for Dynamics CRM 1.0
//
// Copyright (c) Microsoft Corporation
//
// All rights reserved.
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the ""Software""), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//  ize_tipologia.java
package com.microsoft.xrm.sdk.client.ize;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;
import com.microsoft.xrm.sdk.*;


public enum ize_tipologia {
	Obranueva(274040000),
	Vivienda(274040001),
	Oficina(274040002),
	Local(274040003),
	Nave(274040004),
	Garaje(274040005),
	Terreno(274040006);
	private int mValue;

	ize_tipologia(int value) {
		mValue = value;
	}

	public int getValue() {
		return mValue;
	}

	public static ize_tipologia toObject(int value) {
		switch(value) {
			case 274040000:
				return Obranueva;
			case 274040001:
				return Vivienda;
			case 274040002:
				return Oficina;
			case 274040003:
				return Local;
			case 274040004:
				return Nave;
			case 274040005:
				return Garaje;
			case 274040006:
				return Terreno;
			default: 
				return null;
		}
	}
}

