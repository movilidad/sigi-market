package com.microsoft.xrm.sdk.client.ize;
// Mobile Model Generator for Dynamics CRM 1.0
//
// Copyright (c) Microsoft Corporation
//
// All rights reserved.
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the ""Software""), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//  ize_orientacion.java

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;
import com.microsoft.xrm.sdk.*;


public enum ize_orientacion {
	Norte(274040000),
	Sur(274040001),
	Este(274040002),
	Oeste(274040003),
	Noroeste(274040004),
	Suroeste(274040005),
	Noreste(274040006),
	Sureste(274040007);
	private int mValue;

	ize_orientacion(int value) {
		mValue = value;
	}

	public int getValue() {
		return mValue;
	}

	public static ize_orientacion toObject(int value) {
		switch(value) {
			case 274040000:
				return Norte;
			case 274040001:
				return Sur;
			case 274040002:
				return Este;
			case 274040003:
				return Oeste;
			case 274040004:
				return Noroeste;
			case 274040005:
				return Suroeste;
			case 274040006:
				return Noreste;
			case 274040007:
				return Sureste;
			default: 
				return null;
		}
	}
}

