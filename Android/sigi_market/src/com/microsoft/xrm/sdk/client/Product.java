// Mobile Model Generator for Dynamics CRM 1.0
//
// Copyright (c) Microsoft Corporation
//
// All rights reserved.
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the ""Software""), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//  Product.java

package com.microsoft.xrm.sdk.client;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;
import com.microsoft.xrm.sdk.*;


@EntityLogicalNameAttribute("product")
public class Product extends Entity {
	
	public static final String EntityLogicalName = "product";
	public static final int EntityTypeCode = 1024;
	
	public Product() {
		super("product");
	}

	public static Product build() {
		return new Product();
	}

	@AttributeSchemaNameAttribute("ProductId")
	@AttributeLogicalNameAttribute("productid")
	public UUID getProductId() {
		return this.getAttributeValue("productid");
	}
	
	@AttributeSchemaNameAttribute("ProductId")
	@AttributeLogicalNameAttribute("productid")
	public Product setProductId(UUID value) {
		this.setAttributeValue("productid", value);
		if (value != null) {
			super.setId(value);
		}
		else {
			super.setId(new UUID(0L, 0L));
		}

		return this;
	}
	
	@Override
	@AttributeSchemaNameAttribute("ProductId")
	@AttributeLogicalNameAttribute("productid")
	public UUID getId() {
		return super.getId();
	}
	
	@Override
	@AttributeSchemaNameAttribute("ProductId")
	@AttributeLogicalNameAttribute("productid")
	public void setId(UUID value) {
		this.setProductId(value);
	}
	

	@AttributeSchemaNameAttribute("CreatedBy")
	@AttributeLogicalNameAttribute("createdby")
	public EntityReference getCreatedBy() {
		return this.getAttributeValue("createdby");
	}
	
	@AttributeSchemaNameAttribute("CreatedBy")
	@AttributeLogicalNameAttribute("createdby")
	public Product setCreatedBy(EntityReference value) {
		this.setAttributeValue("createdby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedByName")
	@AttributeLogicalNameAttribute("createdbyname")
	public String getCreatedByName() {
		return this.getAttributeValue("createdbyname");
	}
	
	@AttributeSchemaNameAttribute("CreatedByName")
	@AttributeLogicalNameAttribute("createdbyname")
	public Product setCreatedByName(String value) {
		this.setAttributeValue("createdbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedByYomiName")
	@AttributeLogicalNameAttribute("createdbyyominame")
	public String getCreatedByYomiName() {
		return this.getAttributeValue("createdbyyominame");
	}
	
	@AttributeSchemaNameAttribute("CreatedByYomiName")
	@AttributeLogicalNameAttribute("createdbyyominame")
	public Product setCreatedByYomiName(String value) {
		this.setAttributeValue("createdbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOn")
	@AttributeLogicalNameAttribute("createdon")
	public Date getCreatedOn() {
		return this.getAttributeValue("createdon");
	}
	
	@AttributeSchemaNameAttribute("CreatedOn")
	@AttributeLogicalNameAttribute("createdon")
	public Product setCreatedOn(Date value) {
		this.setAttributeValue("createdon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfBy")
	@AttributeLogicalNameAttribute("createdonbehalfby")
	public EntityReference getCreatedOnBehalfBy() {
		return this.getAttributeValue("createdonbehalfby");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfBy")
	@AttributeLogicalNameAttribute("createdonbehalfby")
	public Product setCreatedOnBehalfBy(EntityReference value) {
		this.setAttributeValue("createdonbehalfby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfByName")
	@AttributeLogicalNameAttribute("createdonbehalfbyname")
	public String getCreatedOnBehalfByName() {
		return this.getAttributeValue("createdonbehalfbyname");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfByName")
	@AttributeLogicalNameAttribute("createdonbehalfbyname")
	public Product setCreatedOnBehalfByName(String value) {
		this.setAttributeValue("createdonbehalfbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CreatedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("createdonbehalfbyyominame")
	public String getCreatedOnBehalfByYomiName() {
		return this.getAttributeValue("createdonbehalfbyyominame");
	}
	
	@AttributeSchemaNameAttribute("CreatedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("createdonbehalfbyyominame")
	public Product setCreatedOnBehalfByYomiName(String value) {
		this.setAttributeValue("createdonbehalfbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CurrentCost")
	@AttributeLogicalNameAttribute("currentcost")
	public Money getCurrentCost() {
		return this.getAttributeValue("currentcost");
	}
	
	@AttributeSchemaNameAttribute("CurrentCost")
	@AttributeLogicalNameAttribute("currentcost")
	public Product setCurrentCost(Money value) {
		this.setAttributeValue("currentcost", value);
		return this;
	}

	@AttributeSchemaNameAttribute("CurrentCost_Base")
	@AttributeLogicalNameAttribute("currentcost_base")
	public Money getCurrentCost_Base() {
		return this.getAttributeValue("currentcost_base");
	}
	
	@AttributeSchemaNameAttribute("CurrentCost_Base")
	@AttributeLogicalNameAttribute("currentcost_base")
	public Product setCurrentCost_Base(Money value) {
		this.setAttributeValue("currentcost_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("DefaultUoMId")
	@AttributeLogicalNameAttribute("defaultuomid")
	public EntityReference getDefaultUoMId() {
		return this.getAttributeValue("defaultuomid");
	}
	
	@AttributeSchemaNameAttribute("DefaultUoMId")
	@AttributeLogicalNameAttribute("defaultuomid")
	public Product setDefaultUoMId(EntityReference value) {
		this.setAttributeValue("defaultuomid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("DefaultUoMIdName")
	@AttributeLogicalNameAttribute("defaultuomidname")
	public String getDefaultUoMIdName() {
		return this.getAttributeValue("defaultuomidname");
	}
	
	@AttributeSchemaNameAttribute("DefaultUoMIdName")
	@AttributeLogicalNameAttribute("defaultuomidname")
	public Product setDefaultUoMIdName(String value) {
		this.setAttributeValue("defaultuomidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("DefaultUoMScheduleId")
	@AttributeLogicalNameAttribute("defaultuomscheduleid")
	public EntityReference getDefaultUoMScheduleId() {
		return this.getAttributeValue("defaultuomscheduleid");
	}
	
	@AttributeSchemaNameAttribute("DefaultUoMScheduleId")
	@AttributeLogicalNameAttribute("defaultuomscheduleid")
	public Product setDefaultUoMScheduleId(EntityReference value) {
		this.setAttributeValue("defaultuomscheduleid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("DefaultUoMScheduleIdName")
	@AttributeLogicalNameAttribute("defaultuomscheduleidname")
	public String getDefaultUoMScheduleIdName() {
		return this.getAttributeValue("defaultuomscheduleidname");
	}
	
	@AttributeSchemaNameAttribute("DefaultUoMScheduleIdName")
	@AttributeLogicalNameAttribute("defaultuomscheduleidname")
	public Product setDefaultUoMScheduleIdName(String value) {
		this.setAttributeValue("defaultuomscheduleidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Description")
	@AttributeLogicalNameAttribute("description")
	public String getDescription() {
		return this.getAttributeValue("description");
	}
	
	@AttributeSchemaNameAttribute("Description")
	@AttributeLogicalNameAttribute("description")
	public Product setDescription(String value) {
		this.setAttributeValue("description", value);
		return this;
	}

	@AttributeSchemaNameAttribute("DMTImportState")
	@AttributeLogicalNameAttribute("dmtimportstate")
	public int getDMTImportState() {
		return this.getAttributeValue("dmtimportstate");
	}
	
	@AttributeSchemaNameAttribute("DMTImportState")
	@AttributeLogicalNameAttribute("dmtimportstate")
	public Product setDMTImportState(int value) {
		this.setAttributeValue("dmtimportstate", value);
		return this;
	}

	@AttributeSchemaNameAttribute("EntityImage_Timestamp")
	@AttributeLogicalNameAttribute("entityimage_timestamp")
	public BigInteger getEntityImage_Timestamp() {
		return this.getAttributeValue("entityimage_timestamp");
	}
	
	@AttributeSchemaNameAttribute("EntityImage_Timestamp")
	@AttributeLogicalNameAttribute("entityimage_timestamp")
	public Product setEntityImage_Timestamp(BigInteger value) {
		this.setAttributeValue("entityimage_timestamp", value);
		return this;
	}

	@AttributeSchemaNameAttribute("EntityImage_URL")
	@AttributeLogicalNameAttribute("entityimage_url")
	public String getEntityImage_URL() {
		return this.getAttributeValue("entityimage_url");
	}
	
	@AttributeSchemaNameAttribute("EntityImage_URL")
	@AttributeLogicalNameAttribute("entityimage_url")
	public Product setEntityImage_URL(String value) {
		this.setAttributeValue("entityimage_url", value);
		return this;
	}

	@AttributeSchemaNameAttribute("EntityImageId")
	@AttributeLogicalNameAttribute("entityimageid")
	public UUID getEntityImageId() {
		return this.getAttributeValue("entityimageid");
	}
	
	@AttributeSchemaNameAttribute("EntityImageId")
	@AttributeLogicalNameAttribute("entityimageid")
	public Product setEntityImageId(UUID value) {
		this.setAttributeValue("entityimageid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ExchangeRate")
	@AttributeLogicalNameAttribute("exchangerate")
	public BigDecimal getExchangeRate() {
		return this.getAttributeValue("exchangerate");
	}
	
	@AttributeSchemaNameAttribute("ExchangeRate")
	@AttributeLogicalNameAttribute("exchangerate")
	public Product setExchangeRate(BigDecimal value) {
		this.setAttributeValue("exchangerate", value);
		return this;
	}

	@AttributeSchemaNameAttribute("HierarchyPath")
	@AttributeLogicalNameAttribute("hierarchypath")
	public String getHierarchyPath() {
		return this.getAttributeValue("hierarchypath");
	}
	
	@AttributeSchemaNameAttribute("HierarchyPath")
	@AttributeLogicalNameAttribute("hierarchypath")
	public Product setHierarchyPath(String value) {
		this.setAttributeValue("hierarchypath", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ImportSequenceNumber")
	@AttributeLogicalNameAttribute("importsequencenumber")
	public int getImportSequenceNumber() {
		return this.getAttributeValue("importsequencenumber");
	}
	
	@AttributeSchemaNameAttribute("ImportSequenceNumber")
	@AttributeLogicalNameAttribute("importsequencenumber")
	public Product setImportSequenceNumber(int value) {
		this.setAttributeValue("importsequencenumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("IsKit")
	@AttributeLogicalNameAttribute("iskit")
	public boolean getIsKit() {
		return this.getAttributeValue("iskit");
	}
	
	@AttributeSchemaNameAttribute("IsKit")
	@AttributeLogicalNameAttribute("iskit")
	public Product setIsKit(boolean value) {
		this.setAttributeValue("iskit", value);
		return this;
	}

	@AttributeSchemaNameAttribute("IsStockItem")
	@AttributeLogicalNameAttribute("isstockitem")
	public boolean getIsStockItem() {
		return this.getAttributeValue("isstockitem");
	}
	
	@AttributeSchemaNameAttribute("IsStockItem")
	@AttributeLogicalNameAttribute("isstockitem")
	public Product setIsStockItem(boolean value) {
		this.setAttributeValue("isstockitem", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_aacc")
	@AttributeLogicalNameAttribute("ize_aacc")
	public boolean getIze_aacc() {
		return this.getAttributeValue("ize_aacc");
	}
	
	@AttributeSchemaNameAttribute("Ize_aacc")
	@AttributeLogicalNameAttribute("ize_aacc")
	public Product setIze_aacc(boolean value) {
		this.setAttributeValue("ize_aacc", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Accesibleminusvalidos")
	@AttributeLogicalNameAttribute("ize_accesibleminusvalidos")
	public OptionSetValue getIze_Accesibleminusvalidos() {
		return this.getAttributeValue("ize_accesibleminusvalidos");
	}
	
	@AttributeSchemaNameAttribute("Ize_Accesibleminusvalidos")
	@AttributeLogicalNameAttribute("ize_accesibleminusvalidos")
	public Product setIze_Accesibleminusvalidos(OptionSetValue value) {
		this.setAttributeValue("ize_accesibleminusvalidos", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_accesominusvalidos")
	@AttributeLogicalNameAttribute("ize_accesominusvalidos")
	public boolean getIze_accesominusvalidos() {
		return this.getAttributeValue("ize_accesominusvalidos");
	}
	
	@AttributeSchemaNameAttribute("Ize_accesominusvalidos")
	@AttributeLogicalNameAttribute("ize_accesominusvalidos")
	public Product setIze_accesominusvalidos(boolean value) {
		this.setAttributeValue("ize_accesominusvalidos", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_address1_composite")
	@AttributeLogicalNameAttribute("ize_address1_composite")
	public String getIze_address1_composite() {
		return this.getAttributeValue("ize_address1_composite");
	}
	
	@AttributeSchemaNameAttribute("Ize_address1_composite")
	@AttributeLogicalNameAttribute("ize_address1_composite")
	public Product setIze_address1_composite(String value) {
		this.setAttributeValue("ize_address1_composite", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Aireacondicionado")
	@AttributeLogicalNameAttribute("ize_aireacondicionado")
	public OptionSetValue getIze_Aireacondicionado() {
		return this.getAttributeValue("ize_aireacondicionado");
	}
	
	@AttributeSchemaNameAttribute("Ize_Aireacondicionado")
	@AttributeLogicalNameAttribute("ize_aireacondicionado")
	public Product setIze_Aireacondicionado(OptionSetValue value) {
		this.setAttributeValue("ize_aireacondicionado", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Alarma")
	@AttributeLogicalNameAttribute("ize_alarma")
	public String getIze_Alarma() {
		return this.getAttributeValue("ize_alarma");
	}
	
	@AttributeSchemaNameAttribute("Ize_Alarma")
	@AttributeLogicalNameAttribute("ize_alarma")
	public Product setIze_Alarma(String value) {
		this.setAttributeValue("ize_alarma", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Alquiler")
	@AttributeLogicalNameAttribute("ize_alquiler")
	public OptionSetValue getIze_Alquiler() {
		return this.getAttributeValue("ize_alquiler");
	}
	
	@AttributeSchemaNameAttribute("Ize_Alquiler")
	@AttributeLogicalNameAttribute("ize_alquiler")
	public Product setIze_Alquiler(OptionSetValue value) {
		this.setAttributeValue("ize_alquiler", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_AlturaReal")
	@AttributeLogicalNameAttribute("ize_alturareal")
	public OptionSetValue getIze_AlturaReal() {
		return this.getAttributeValue("ize_alturareal");
	}
	
	@AttributeSchemaNameAttribute("Ize_AlturaReal")
	@AttributeLogicalNameAttribute("ize_alturareal")
	public Product setIze_AlturaReal(OptionSetValue value) {
		this.setAttributeValue("ize_alturareal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Amueblado")
	@AttributeLogicalNameAttribute("ize_amueblado")
	public OptionSetValue getIze_Amueblado() {
		return this.getAttributeValue("ize_amueblado");
	}
	
	@AttributeSchemaNameAttribute("Ize_Amueblado")
	@AttributeLogicalNameAttribute("ize_amueblado")
	public Product setIze_Amueblado(OptionSetValue value) {
		this.setAttributeValue("ize_amueblado", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Amueblados")
	@AttributeLogicalNameAttribute("ize_amueblados")
	public boolean getIze_Amueblados() {
		return this.getAttributeValue("ize_amueblados");
	}
	
	@AttributeSchemaNameAttribute("Ize_Amueblados")
	@AttributeLogicalNameAttribute("ize_amueblados")
	public Product setIze_Amueblados(boolean value) {
		this.setAttributeValue("ize_amueblados", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Arrendado")
	@AttributeLogicalNameAttribute("ize_arrendado")
	public boolean getIze_Arrendado() {
		return this.getAttributeValue("ize_arrendado");
	}
	
	@AttributeSchemaNameAttribute("Ize_Arrendado")
	@AttributeLogicalNameAttribute("ize_arrendado")
	public Product setIze_Arrendado(boolean value) {
		this.setAttributeValue("ize_arrendado", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Ascensor")
	@AttributeLogicalNameAttribute("ize_ascensor")
	public OptionSetValue getIze_Ascensor() {
		return this.getAttributeValue("ize_ascensor");
	}
	
	@AttributeSchemaNameAttribute("Ize_Ascensor")
	@AttributeLogicalNameAttribute("ize_ascensor")
	public Product setIze_Ascensor(OptionSetValue value) {
		this.setAttributeValue("ize_ascensor", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Ascensores")
	@AttributeLogicalNameAttribute("ize_ascensores")
	public boolean getIze_Ascensores() {
		return this.getAttributeValue("ize_ascensores");
	}
	
	@AttributeSchemaNameAttribute("Ize_Ascensores")
	@AttributeLogicalNameAttribute("ize_ascensores")
	public Product setIze_Ascensores(boolean value) {
		this.setAttributeValue("ize_ascensores", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Bar")
	@AttributeLogicalNameAttribute("ize_bar")
	public boolean getIze_Bar() {
		return this.getAttributeValue("ize_bar");
	}
	
	@AttributeSchemaNameAttribute("Ize_Bar")
	@AttributeLogicalNameAttribute("ize_bar")
	public Product setIze_Bar(boolean value) {
		this.setAttributeValue("ize_bar", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Barmusical")
	@AttributeLogicalNameAttribute("ize_barmusical")
	public boolean getIze_Barmusical() {
		return this.getAttributeValue("ize_barmusical");
	}
	
	@AttributeSchemaNameAttribute("Ize_Barmusical")
	@AttributeLogicalNameAttribute("ize_barmusical")
	public Product setIze_Barmusical(boolean value) {
		this.setAttributeValue("ize_barmusical", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Barrio")
	@AttributeLogicalNameAttribute("ize_barrio")
	public EntityReference getIze_Barrio() {
		return this.getAttributeValue("ize_barrio");
	}
	
	@AttributeSchemaNameAttribute("Ize_Barrio")
	@AttributeLogicalNameAttribute("ize_barrio")
	public Product setIze_Barrio(EntityReference value) {
		this.setAttributeValue("ize_barrio", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_BarrioName")
	@AttributeLogicalNameAttribute("ize_barrioname")
	public String getIze_BarrioName() {
		return this.getAttributeValue("ize_barrioname");
	}
	
	@AttributeSchemaNameAttribute("Ize_BarrioName")
	@AttributeLogicalNameAttribute("ize_barrioname")
	public Product setIze_BarrioName(String value) {
		this.setAttributeValue("ize_barrioname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_BloqueId")
	@AttributeLogicalNameAttribute("ize_bloqueid")
	public EntityReference getIze_BloqueId() {
		return this.getAttributeValue("ize_bloqueid");
	}
	
	@AttributeSchemaNameAttribute("Ize_BloqueId")
	@AttributeLogicalNameAttribute("ize_bloqueid")
	public Product setIze_BloqueId(EntityReference value) {
		this.setAttributeValue("ize_bloqueid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_BloqueIdName")
	@AttributeLogicalNameAttribute("ize_bloqueidname")
	public String getIze_BloqueIdName() {
		return this.getAttributeValue("ize_bloqueidname");
	}
	
	@AttributeSchemaNameAttribute("Ize_BloqueIdName")
	@AttributeLogicalNameAttribute("ize_bloqueidname")
	public Product setIze_BloqueIdName(String value) {
		this.setAttributeValue("ize_bloqueidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Calefaccion")
	@AttributeLogicalNameAttribute("ize_calefaccion")
	public OptionSetValue getIze_Calefaccion() {
		return this.getAttributeValue("ize_calefaccion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Calefaccion")
	@AttributeLogicalNameAttribute("ize_calefaccion")
	public Product setIze_Calefaccion(OptionSetValue value) {
		this.setAttributeValue("ize_calefaccion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_calle2")
	@AttributeLogicalNameAttribute("ize_calle2")
	public String getIze_calle2() {
		return this.getAttributeValue("ize_calle2");
	}
	
	@AttributeSchemaNameAttribute("Ize_calle2")
	@AttributeLogicalNameAttribute("ize_calle2")
	public Product setIze_calle2(String value) {
		this.setAttributeValue("ize_calle2", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Calle3")
	@AttributeLogicalNameAttribute("ize_calle3")
	public String getIze_Calle3() {
		return this.getAttributeValue("ize_calle3");
	}
	
	@AttributeSchemaNameAttribute("Ize_Calle3")
	@AttributeLogicalNameAttribute("ize_calle3")
	public Product setIze_Calle3(String value) {
		this.setAttributeValue("ize_calle3", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Cargasregistrales")
	@AttributeLogicalNameAttribute("ize_cargasregistrales")
	public String getIze_Cargasregistrales() {
		return this.getAttributeValue("ize_cargasregistrales");
	}
	
	@AttributeSchemaNameAttribute("Ize_Cargasregistrales")
	@AttributeLogicalNameAttribute("ize_cargasregistrales")
	public Product setIze_Cargasregistrales(String value) {
		this.setAttributeValue("ize_cargasregistrales", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Cartel")
	@AttributeLogicalNameAttribute("ize_cartel")
	public OptionSetValue getIze_Cartel() {
		return this.getAttributeValue("ize_cartel");
	}
	
	@AttributeSchemaNameAttribute("Ize_Cartel")
	@AttributeLogicalNameAttribute("ize_cartel")
	public Product setIze_Cartel(OptionSetValue value) {
		this.setAttributeValue("ize_cartel", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_CertificadoEnergetico")
	@AttributeLogicalNameAttribute("ize_certificadoenergetico")
	public OptionSetValue getIze_CertificadoEnergetico() {
		return this.getAttributeValue("ize_certificadoenergetico");
	}
	
	@AttributeSchemaNameAttribute("Ize_CertificadoEnergetico")
	@AttributeLogicalNameAttribute("ize_certificadoenergetico")
	public Product setIze_CertificadoEnergetico(OptionSetValue value) {
		this.setAttributeValue("ize_certificadoenergetico", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Cocinaoffice")
	@AttributeLogicalNameAttribute("ize_cocinaoffice")
	public boolean getIze_Cocinaoffice() {
		return this.getAttributeValue("ize_cocinaoffice");
	}
	
	@AttributeSchemaNameAttribute("Ize_Cocinaoffice")
	@AttributeLogicalNameAttribute("ize_cocinaoffice")
	public Product setIze_Cocinaoffice(boolean value) {
		this.setAttributeValue("ize_cocinaoffice", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_CodCategora")
	@AttributeLogicalNameAttribute("ize_codcategora")
	public EntityReference getIze_CodCategora() {
		return this.getAttributeValue("ize_codcategora");
	}
	
	@AttributeSchemaNameAttribute("Ize_CodCategora")
	@AttributeLogicalNameAttribute("ize_codcategora")
	public Product setIze_CodCategora(EntityReference value) {
		this.setAttributeValue("ize_codcategora", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_CodCategoraName")
	@AttributeLogicalNameAttribute("ize_codcategoraname")
	public String getIze_CodCategoraName() {
		return this.getAttributeValue("ize_codcategoraname");
	}
	
	@AttributeSchemaNameAttribute("Ize_CodCategoraName")
	@AttributeLogicalNameAttribute("ize_codcategoraname")
	public Product setIze_CodCategoraName(String value) {
		this.setAttributeValue("ize_codcategoraname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_CodGrupo")
	@AttributeLogicalNameAttribute("ize_codgrupo")
	public EntityReference getIze_CodGrupo() {
		return this.getAttributeValue("ize_codgrupo");
	}
	
	@AttributeSchemaNameAttribute("Ize_CodGrupo")
	@AttributeLogicalNameAttribute("ize_codgrupo")
	public Product setIze_CodGrupo(EntityReference value) {
		this.setAttributeValue("ize_codgrupo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_CodGrupoName")
	@AttributeLogicalNameAttribute("ize_codgruponame")
	public String getIze_CodGrupoName() {
		return this.getAttributeValue("ize_codgruponame");
	}
	
	@AttributeSchemaNameAttribute("Ize_CodGrupoName")
	@AttributeLogicalNameAttribute("ize_codgruponame")
	public Product setIze_CodGrupoName(String value) {
		this.setAttributeValue("ize_codgruponame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_CodigoPostal")
	@AttributeLogicalNameAttribute("ize_codigopostal")
	public EntityReference getIze_CodigoPostal() {
		return this.getAttributeValue("ize_codigopostal");
	}
	
	@AttributeSchemaNameAttribute("Ize_CodigoPostal")
	@AttributeLogicalNameAttribute("ize_codigopostal")
	public Product setIze_CodigoPostal(EntityReference value) {
		this.setAttributeValue("ize_codigopostal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_CodigoPostalName")
	@AttributeLogicalNameAttribute("ize_codigopostalname")
	public String getIze_CodigoPostalName() {
		return this.getAttributeValue("ize_codigopostalname");
	}
	
	@AttributeSchemaNameAttribute("Ize_CodigoPostalName")
	@AttributeLogicalNameAttribute("ize_codigopostalname")
	public Product setIze_CodigoPostalName(String value) {
		this.setAttributeValue("ize_codigopostalname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_codigopostaltexto")
	@AttributeLogicalNameAttribute("ize_codigopostaltexto")
	public String getIze_codigopostaltexto() {
		return this.getAttributeValue("ize_codigopostaltexto");
	}
	
	@AttributeSchemaNameAttribute("Ize_codigopostaltexto")
	@AttributeLogicalNameAttribute("ize_codigopostaltexto")
	public Product setIze_codigopostaltexto(String value) {
		this.setAttributeValue("ize_codigopostaltexto", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_codigounidadventa")
	@AttributeLogicalNameAttribute("ize_codigounidadventa")
	public String getIze_codigounidadventa() {
		return this.getAttributeValue("ize_codigounidadventa");
	}
	
	@AttributeSchemaNameAttribute("Ize_codigounidadventa")
	@AttributeLogicalNameAttribute("ize_codigounidadventa")
	public Product setIze_codigounidadventa(String value) {
		this.setAttributeValue("ize_codigounidadventa", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_coefdivhorizontal")
	@AttributeLogicalNameAttribute("ize_coefdivhorizontal")
	public BigDecimal getIze_coefdivhorizontal() {
		return this.getAttributeValue("ize_coefdivhorizontal");
	}
	
	@AttributeSchemaNameAttribute("Ize_coefdivhorizontal")
	@AttributeLogicalNameAttribute("ize_coefdivhorizontal")
	public Product setIze_coefdivhorizontal(BigDecimal value) {
		this.setAttributeValue("ize_coefdivhorizontal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_coefedificabilidad")
	@AttributeLogicalNameAttribute("ize_coefedificabilidad")
	public BigDecimal getIze_coefedificabilidad() {
		return this.getAttributeValue("ize_coefedificabilidad");
	}
	
	@AttributeSchemaNameAttribute("Ize_coefedificabilidad")
	@AttributeLogicalNameAttribute("ize_coefedificabilidad")
	public Product setIze_coefedificabilidad(BigDecimal value) {
		this.setAttributeValue("ize_coefedificabilidad", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Comunicaciones")
	@AttributeLogicalNameAttribute("ize_comunicaciones")
	public String getIze_Comunicaciones() {
		return this.getAttributeValue("ize_comunicaciones");
	}
	
	@AttributeSchemaNameAttribute("Ize_Comunicaciones")
	@AttributeLogicalNameAttribute("ize_comunicaciones")
	public Product setIze_Comunicaciones(String value) {
		this.setAttributeValue("ize_comunicaciones", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Comunidad")
	@AttributeLogicalNameAttribute("ize_comunidad")
	public String getIze_Comunidad() {
		return this.getAttributeValue("ize_comunidad");
	}
	
	@AttributeSchemaNameAttribute("Ize_Comunidad")
	@AttributeLogicalNameAttribute("ize_comunidad")
	public Product setIze_Comunidad(String value) {
		this.setAttributeValue("ize_comunidad", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_concalefaccion")
	@AttributeLogicalNameAttribute("ize_concalefaccion")
	public boolean getIze_concalefaccion() {
		return this.getAttributeValue("ize_concalefaccion");
	}
	
	@AttributeSchemaNameAttribute("Ize_concalefaccion")
	@AttributeLogicalNameAttribute("ize_concalefaccion")
	public Product setIze_concalefaccion(boolean value) {
		this.setAttributeValue("ize_concalefaccion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_conjardin")
	@AttributeLogicalNameAttribute("ize_conjardin")
	public boolean getIze_conjardin() {
		return this.getAttributeValue("ize_conjardin");
	}
	
	@AttributeSchemaNameAttribute("Ize_conjardin")
	@AttributeLogicalNameAttribute("ize_conjardin")
	public Product setIze_conjardin(boolean value) {
		this.setAttributeValue("ize_conjardin", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Consultamedica")
	@AttributeLogicalNameAttribute("ize_consultamedica")
	public boolean getIze_Consultamedica() {
		return this.getAttributeValue("ize_consultamedica");
	}
	
	@AttributeSchemaNameAttribute("Ize_Consultamedica")
	@AttributeLogicalNameAttribute("ize_consultamedica")
	public Product setIze_Consultamedica(boolean value) {
		this.setAttributeValue("ize_consultamedica", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Descripcinacceso")
	@AttributeLogicalNameAttribute("ize_descripcinacceso")
	public String getIze_Descripcinacceso() {
		return this.getAttributeValue("ize_descripcinacceso");
	}
	
	@AttributeSchemaNameAttribute("Ize_Descripcinacceso")
	@AttributeLogicalNameAttribute("ize_descripcinacceso")
	public Product setIze_Descripcinacceso(String value) {
		this.setAttributeValue("ize_descripcinacceso", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Destacado")
	@AttributeLogicalNameAttribute("ize_destacado")
	public boolean getIze_Destacado() {
		return this.getAttributeValue("ize_destacado");
	}
	
	@AttributeSchemaNameAttribute("Ize_Destacado")
	@AttributeLogicalNameAttribute("ize_destacado")
	public Product setIze_Destacado(boolean value) {
		this.setAttributeValue("ize_destacado", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_direccion_calle1")
	@AttributeLogicalNameAttribute("ize_direccion_calle1")
	public String getIze_direccion_calle1() {
		return this.getAttributeValue("ize_direccion_calle1");
	}
	
	@AttributeSchemaNameAttribute("Ize_direccion_calle1")
	@AttributeLogicalNameAttribute("ize_direccion_calle1")
	public Product setIze_direccion_calle1(String value) {
		this.setAttributeValue("ize_direccion_calle1", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Direccion_escalera")
	@AttributeLogicalNameAttribute("ize_direccion_escalera")
	public String getIze_Direccion_escalera() {
		return this.getAttributeValue("ize_direccion_escalera");
	}
	
	@AttributeSchemaNameAttribute("Ize_Direccion_escalera")
	@AttributeLogicalNameAttribute("ize_direccion_escalera")
	public Product setIze_Direccion_escalera(String value) {
		this.setAttributeValue("ize_direccion_escalera", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Direccion_letra")
	@AttributeLogicalNameAttribute("ize_direccion_letra")
	public String getIze_Direccion_letra() {
		return this.getAttributeValue("ize_direccion_letra");
	}
	
	@AttributeSchemaNameAttribute("Ize_Direccion_letra")
	@AttributeLogicalNameAttribute("ize_direccion_letra")
	public Product setIze_Direccion_letra(String value) {
		this.setAttributeValue("ize_direccion_letra", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_direccion_numero")
	@AttributeLogicalNameAttribute("ize_direccion_numero")
	public String getIze_direccion_numero() {
		return this.getAttributeValue("ize_direccion_numero");
	}
	
	@AttributeSchemaNameAttribute("Ize_direccion_numero")
	@AttributeLogicalNameAttribute("ize_direccion_numero")
	public Product setIze_direccion_numero(String value) {
		this.setAttributeValue("ize_direccion_numero", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_direccion_tipo")
	@AttributeLogicalNameAttribute("ize_direccion_tipo")
	public OptionSetValue getIze_direccion_tipo() {
		return this.getAttributeValue("ize_direccion_tipo");
	}
	
	@AttributeSchemaNameAttribute("Ize_direccion_tipo")
	@AttributeLogicalNameAttribute("ize_direccion_tipo")
	public Product setIze_direccion_tipo(OptionSetValue value) {
		this.setAttributeValue("ize_direccion_tipo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Direccioncompletaweb")
	@AttributeLogicalNameAttribute("ize_direccioncompletaweb")
	public boolean getIze_Direccioncompletaweb() {
		return this.getAttributeValue("ize_direccioncompletaweb");
	}
	
	@AttributeSchemaNameAttribute("Ize_Direccioncompletaweb")
	@AttributeLogicalNameAttribute("ize_direccioncompletaweb")
	public Product setIze_Direccioncompletaweb(boolean value) {
		this.setAttributeValue("ize_direccioncompletaweb", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Direccionvisible")
	@AttributeLogicalNameAttribute("ize_direccionvisible")
	public boolean getIze_Direccionvisible() {
		return this.getAttributeValue("ize_direccionvisible");
	}
	
	@AttributeSchemaNameAttribute("Ize_Direccionvisible")
	@AttributeLogicalNameAttribute("ize_direccionvisible")
	public Product setIze_Direccionvisible(boolean value) {
		this.setAttributeValue("ize_direccionvisible", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Distrito")
	@AttributeLogicalNameAttribute("ize_distrito")
	public EntityReference getIze_Distrito() {
		return this.getAttributeValue("ize_distrito");
	}
	
	@AttributeSchemaNameAttribute("Ize_Distrito")
	@AttributeLogicalNameAttribute("ize_distrito")
	public Product setIze_Distrito(EntityReference value) {
		this.setAttributeValue("ize_distrito", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_DistritoName")
	@AttributeLogicalNameAttribute("ize_distritoname")
	public String getIze_DistritoName() {
		return this.getAttributeValue("ize_distritoname");
	}
	
	@AttributeSchemaNameAttribute("Ize_DistritoName")
	@AttributeLogicalNameAttribute("ize_distritoname")
	public Product setIze_DistritoName(String value) {
		this.setAttributeValue("ize_distritoname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Duracionopcion")
	@AttributeLogicalNameAttribute("ize_duracionopcion")
	public String getIze_Duracionopcion() {
		return this.getAttributeValue("ize_duracionopcion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Duracionopcion")
	@AttributeLogicalNameAttribute("ize_duracionopcion")
	public Product setIze_Duracionopcion(String value) {
		this.setAttributeValue("ize_duracionopcion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_documento_url")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_documento_url")
	public String getIze_eficiencia_energetica_documento_url() {
		return this.getAttributeValue("ize_eficiencia_energetica_documento_url");
	}
	
	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_documento_url")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_documento_url")
	public Product setIze_eficiencia_energetica_documento_url(String value) {
		this.setAttributeValue("ize_eficiencia_energetica_documento_url", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_documento_visible")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_documento_visible")
	public boolean getIze_eficiencia_energetica_documento_visible() {
		return this.getAttributeValue("ize_eficiencia_energetica_documento_visible");
	}
	
	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_documento_visible")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_documento_visible")
	public Product setIze_eficiencia_energetica_documento_visible(boolean value) {
		this.setAttributeValue("ize_eficiencia_energetica_documento_visible", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_emisiones")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_emisiones")
	public double getIze_eficiencia_energetica_emisiones() {
		return this.getAttributeValue("ize_eficiencia_energetica_emisiones");
	}
	
	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_emisiones")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_emisiones")
	public Product setIze_eficiencia_energetica_emisiones(double value) {
		this.setAttributeValue("ize_eficiencia_energetica_emisiones", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_energia")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_energia")
	public double getIze_eficiencia_energetica_energia() {
		return this.getAttributeValue("ize_eficiencia_energetica_energia");
	}
	
	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_energia")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_energia")
	public Product setIze_eficiencia_energetica_energia(double value) {
		this.setAttributeValue("ize_eficiencia_energetica_energia", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_energia_tipo")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_energia_tipo")
	public OptionSetValue getIze_eficiencia_energetica_energia_tipo() {
		return this.getAttributeValue("ize_eficiencia_energetica_energia_tipo");
	}
	
	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_energia_tipo")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_energia_tipo")
	public Product setIze_eficiencia_energetica_energia_tipo(OptionSetValue value) {
		this.setAttributeValue("ize_eficiencia_energetica_energia_tipo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_entramite")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_entramite")
	public boolean getIze_eficiencia_energetica_entramite() {
		return this.getAttributeValue("ize_eficiencia_energetica_entramite");
	}
	
	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_entramite")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_entramite")
	public Product setIze_eficiencia_energetica_entramite(boolean value) {
		this.setAttributeValue("ize_eficiencia_energetica_entramite", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_fecvalid")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_fecvalid")
	public Date getIze_eficiencia_energetica_fecvalid() {
		return this.getAttributeValue("ize_eficiencia_energetica_fecvalid");
	}
	
	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_fecvalid")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_fecvalid")
	public Product setIze_eficiencia_energetica_fecvalid(Date value) {
		this.setAttributeValue("ize_eficiencia_energetica_fecvalid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_n_expediente")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_n_expediente")
	public String getIze_eficiencia_energetica_n_expediente() {
		return this.getAttributeValue("ize_eficiencia_energetica_n_expediente");
	}
	
	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_n_expediente")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_n_expediente")
	public Product setIze_eficiencia_energetica_n_expediente(String value) {
		this.setAttributeValue("ize_eficiencia_energetica_n_expediente", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_tipo")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_tipo")
	public OptionSetValue getIze_eficiencia_energetica_tipo() {
		return this.getAttributeValue("ize_eficiencia_energetica_tipo");
	}
	
	@AttributeSchemaNameAttribute("Ize_eficiencia_energetica_tipo")
	@AttributeLogicalNameAttribute("ize_eficiencia_energetica_tipo")
	public Product setIze_eficiencia_energetica_tipo(OptionSetValue value) {
		this.setAttributeValue("ize_eficiencia_energetica_tipo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_esdecolaborador")
	@AttributeLogicalNameAttribute("ize_esdecolaborador")
	public boolean getIze_esdecolaborador() {
		return this.getAttributeValue("ize_esdecolaborador");
	}
	
	@AttributeSchemaNameAttribute("Ize_esdecolaborador")
	@AttributeLogicalNameAttribute("ize_esdecolaborador")
	public Product setIze_esdecolaborador(boolean value) {
		this.setAttributeValue("ize_esdecolaborador", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_esExterior")
	@AttributeLogicalNameAttribute("ize_esexterior")
	public boolean getIze_esExterior() {
		return this.getAttributeValue("ize_esexterior");
	}
	
	@AttributeSchemaNameAttribute("Ize_esExterior")
	@AttributeLogicalNameAttribute("ize_esexterior")
	public Product setIze_esExterior(boolean value) {
		this.setAttributeValue("ize_esexterior", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_EstadoEdificacion")
	@AttributeLogicalNameAttribute("ize_estadoedificacion")
	public OptionSetValue getIze_EstadoEdificacion() {
		return this.getAttributeValue("ize_estadoedificacion");
	}
	
	@AttributeSchemaNameAttribute("Ize_EstadoEdificacion")
	@AttributeLogicalNameAttribute("ize_estadoedificacion")
	public Product setIze_EstadoEdificacion(OptionSetValue value) {
		this.setAttributeValue("ize_estadoedificacion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Exterior")
	@AttributeLogicalNameAttribute("ize_exterior")
	public OptionSetValue getIze_Exterior() {
		return this.getAttributeValue("ize_exterior");
	}
	
	@AttributeSchemaNameAttribute("Ize_Exterior")
	@AttributeLogicalNameAttribute("ize_exterior")
	public Product setIze_Exterior(OptionSetValue value) {
		this.setAttributeValue("ize_exterior", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Fachadaprincipal")
	@AttributeLogicalNameAttribute("ize_fachadaprincipal")
	public String getIze_Fachadaprincipal() {
		return this.getAttributeValue("ize_fachadaprincipal");
	}
	
	@AttributeSchemaNameAttribute("Ize_Fachadaprincipal")
	@AttributeLogicalNameAttribute("ize_fachadaprincipal")
	public Product setIze_Fachadaprincipal(String value) {
		this.setAttributeValue("ize_fachadaprincipal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Fachadasecundaria")
	@AttributeLogicalNameAttribute("ize_fachadasecundaria")
	public String getIze_Fachadasecundaria() {
		return this.getAttributeValue("ize_fachadasecundaria");
	}
	
	@AttributeSchemaNameAttribute("Ize_Fachadasecundaria")
	@AttributeLogicalNameAttribute("ize_fachadasecundaria")
	public Product setIze_Fachadasecundaria(String value) {
		this.setAttributeValue("ize_fachadasecundaria", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Fecha_captacion")
	@AttributeLogicalNameAttribute("ize_fecha_captacion")
	public Date getIze_Fecha_captacion() {
		return this.getAttributeValue("ize_fecha_captacion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Fecha_captacion")
	@AttributeLogicalNameAttribute("ize_fecha_captacion")
	public Product setIze_Fecha_captacion(Date value) {
		this.setAttributeValue("ize_fecha_captacion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Fibraoptica")
	@AttributeLogicalNameAttribute("ize_fibraoptica")
	public boolean getIze_Fibraoptica() {
		return this.getAttributeValue("ize_fibraoptica");
	}
	
	@AttributeSchemaNameAttribute("Ize_Fibraoptica")
	@AttributeLogicalNameAttribute("ize_fibraoptica")
	public Product setIze_Fibraoptica(boolean value) {
		this.setAttributeValue("ize_fibraoptica", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Finca")
	@AttributeLogicalNameAttribute("ize_finca")
	public String getIze_Finca() {
		return this.getAttributeValue("ize_finca");
	}
	
	@AttributeSchemaNameAttribute("Ize_Finca")
	@AttributeLogicalNameAttribute("ize_finca")
	public Product setIze_Finca(String value) {
		this.setAttributeValue("ize_finca", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Folio")
	@AttributeLogicalNameAttribute("ize_folio")
	public String getIze_Folio() {
		return this.getAttributeValue("ize_folio");
	}
	
	@AttributeSchemaNameAttribute("Ize_Folio")
	@AttributeLogicalNameAttribute("ize_folio")
	public Product setIze_Folio(String value) {
		this.setAttributeValue("ize_folio", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Garaje")
	@AttributeLogicalNameAttribute("ize_garaje")
	public OptionSetValue getIze_Garaje() {
		return this.getAttributeValue("ize_garaje");
	}
	
	@AttributeSchemaNameAttribute("Ize_Garaje")
	@AttributeLogicalNameAttribute("ize_garaje")
	public Product setIze_Garaje(OptionSetValue value) {
		this.setAttributeValue("ize_garaje", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Garajes")
	@AttributeLogicalNameAttribute("ize_garajes")
	public boolean getIze_Garajes() {
		return this.getAttributeValue("ize_garajes");
	}
	
	@AttributeSchemaNameAttribute("Ize_Garajes")
	@AttributeLogicalNameAttribute("ize_garajes")
	public Product setIze_Garajes(boolean value) {
		this.setAttributeValue("ize_garajes", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_GastosComunidadIncluidos")
	@AttributeLogicalNameAttribute("ize_gastoscomunidadincluidos")
	public OptionSetValue getIze_GastosComunidadIncluidos() {
		return this.getAttributeValue("ize_gastoscomunidadincluidos");
	}
	
	@AttributeSchemaNameAttribute("Ize_GastosComunidadIncluidos")
	@AttributeLogicalNameAttribute("ize_gastoscomunidadincluidos")
	public Product setIze_GastosComunidadIncluidos(OptionSetValue value) {
		this.setAttributeValue("ize_gastoscomunidadincluidos", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Honorairosdescripcion")
	@AttributeLogicalNameAttribute("ize_honorairosdescripcion")
	public String getIze_Honorairosdescripcion() {
		return this.getAttributeValue("ize_honorairosdescripcion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Honorairosdescripcion")
	@AttributeLogicalNameAttribute("ize_honorairosdescripcion")
	public Product setIze_Honorairosdescripcion(String value) {
		this.setAttributeValue("ize_honorairosdescripcion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Honorarios")
	@AttributeLogicalNameAttribute("ize_honorarios")
	public OptionSetValue getIze_Honorarios() {
		return this.getAttributeValue("ize_honorarios");
	}
	
	@AttributeSchemaNameAttribute("Ize_Honorarios")
	@AttributeLogicalNameAttribute("ize_honorarios")
	public Product setIze_Honorarios(OptionSetValue value) {
		this.setAttributeValue("ize_honorarios", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Horariovisita")
	@AttributeLogicalNameAttribute("ize_horariovisita")
	public String getIze_Horariovisita() {
		return this.getAttributeValue("ize_horariovisita");
	}
	
	@AttributeSchemaNameAttribute("Ize_Horariovisita")
	@AttributeLogicalNameAttribute("ize_horariovisita")
	public Product setIze_Horariovisita(String value) {
		this.setAttributeValue("ize_horariovisita", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_IBI")
	@AttributeLogicalNameAttribute("ize_ibi")
	public String getIze_IBI() {
		return this.getAttributeValue("ize_ibi");
	}
	
	@AttributeSchemaNameAttribute("Ize_IBI")
	@AttributeLogicalNameAttribute("ize_ibi")
	public Product setIze_IBI(String value) {
		this.setAttributeValue("ize_ibi", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_IBIincluidoenrenta")
	@AttributeLogicalNameAttribute("ize_ibiincluidoenrenta")
	public OptionSetValue getIze_IBIincluidoenrenta() {
		return this.getAttributeValue("ize_ibiincluidoenrenta");
	}
	
	@AttributeSchemaNameAttribute("Ize_IBIincluidoenrenta")
	@AttributeLogicalNameAttribute("ize_ibiincluidoenrenta")
	public Product setIze_IBIincluidoenrenta(OptionSetValue value) {
		this.setAttributeValue("ize_ibiincluidoenrenta", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ID")
	@AttributeLogicalNameAttribute("ize_id")
	public String getIze_ID() {
		return this.getAttributeValue("ize_id");
	}
	
	@AttributeSchemaNameAttribute("Ize_ID")
	@AttributeLogicalNameAttribute("ize_id")
	public Product setIze_ID(String value) {
		this.setAttributeValue("ize_id", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_id_bdui")
	@AttributeLogicalNameAttribute("ize_id_bdui")
	public int getIze_id_bdui() {
		return this.getAttributeValue("ize_id_bdui");
	}
	
	@AttributeSchemaNameAttribute("Ize_id_bdui")
	@AttributeLogicalNameAttribute("ize_id_bdui")
	public Product setIze_id_bdui(int value) {
		this.setAttributeValue("ize_id_bdui", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Idufir")
	@AttributeLogicalNameAttribute("ize_idufir")
	public String getIze_Idufir() {
		return this.getAttributeValue("ize_idufir");
	}
	
	@AttributeSchemaNameAttribute("Ize_Idufir")
	@AttributeLogicalNameAttribute("ize_idufir")
	public Product setIze_Idufir(String value) {
		this.setAttributeValue("ize_idufir", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_InmuebleColaborador")
	@AttributeLogicalNameAttribute("ize_inmueblecolaborador")
	public OptionSetValue getIze_InmuebleColaborador() {
		return this.getAttributeValue("ize_inmueblecolaborador");
	}
	
	@AttributeSchemaNameAttribute("Ize_InmuebleColaborador")
	@AttributeLogicalNameAttribute("ize_inmueblecolaborador")
	public Product setIze_InmuebleColaborador(OptionSetValue value) {
		this.setAttributeValue("ize_inmueblecolaborador", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Inscripcion")
	@AttributeLogicalNameAttribute("ize_inscripcion")
	public String getIze_Inscripcion() {
		return this.getAttributeValue("ize_inscripcion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Inscripcion")
	@AttributeLogicalNameAttribute("ize_inscripcion")
	public Product setIze_Inscripcion(String value) {
		this.setAttributeValue("ize_inscripcion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Jardin")
	@AttributeLogicalNameAttribute("ize_jardin")
	public OptionSetValue getIze_Jardin() {
		return this.getAttributeValue("ize_jardin");
	}
	
	@AttributeSchemaNameAttribute("Ize_Jardin")
	@AttributeLogicalNameAttribute("ize_jardin")
	public Product setIze_Jardin(OptionSetValue value) {
		this.setAttributeValue("ize_jardin", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_latitud")
	@AttributeLogicalNameAttribute("ize_latitud")
	public String getIze_latitud() {
		return this.getAttributeValue("ize_latitud");
	}
	
	@AttributeSchemaNameAttribute("Ize_latitud")
	@AttributeLogicalNameAttribute("ize_latitud")
	public Product setIze_latitud(String value) {
		this.setAttributeValue("ize_latitud", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Libro")
	@AttributeLogicalNameAttribute("ize_libro")
	public String getIze_Libro() {
		return this.getAttributeValue("ize_libro");
	}
	
	@AttributeSchemaNameAttribute("Ize_Libro")
	@AttributeLogicalNameAttribute("ize_libro")
	public Product setIze_Libro(String value) {
		this.setAttributeValue("ize_libro", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Localidadregistro")
	@AttributeLogicalNameAttribute("ize_localidadregistro")
	public String getIze_Localidadregistro() {
		return this.getAttributeValue("ize_localidadregistro");
	}
	
	@AttributeSchemaNameAttribute("Ize_Localidadregistro")
	@AttributeLogicalNameAttribute("ize_localidadregistro")
	public Product setIze_Localidadregistro(String value) {
		this.setAttributeValue("ize_localidadregistro", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Longitud")
	@AttributeLogicalNameAttribute("ize_longitud")
	public String getIze_Longitud() {
		return this.getAttributeValue("ize_longitud");
	}
	
	@AttributeSchemaNameAttribute("Ize_Longitud")
	@AttributeLogicalNameAttribute("ize_longitud")
	public Product setIze_Longitud(String value) {
		this.setAttributeValue("ize_longitud", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_m2altillo")
	@AttributeLogicalNameAttribute("ize_m2altillo")
	public BigDecimal getIze_m2altillo() {
		return this.getAttributeValue("ize_m2altillo");
	}
	
	@AttributeSchemaNameAttribute("Ize_m2altillo")
	@AttributeLogicalNameAttribute("ize_m2altillo")
	public Product setIze_m2altillo(BigDecimal value) {
		this.setAttributeValue("ize_m2altillo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_m2balcon")
	@AttributeLogicalNameAttribute("ize_m2balcon")
	public BigDecimal getIze_m2balcon() {
		return this.getAttributeValue("ize_m2balcon");
	}
	
	@AttributeSchemaNameAttribute("Ize_m2balcon")
	@AttributeLogicalNameAttribute("ize_m2balcon")
	public Product setIze_m2balcon(BigDecimal value) {
		this.setAttributeValue("ize_m2balcon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_M2ccomunes")
	@AttributeLogicalNameAttribute("ize_m2ccomunes")
	public BigDecimal getIze_M2ccomunes() {
		return this.getAttributeValue("ize_m2ccomunes");
	}
	
	@AttributeSchemaNameAttribute("Ize_M2ccomunes")
	@AttributeLogicalNameAttribute("ize_m2ccomunes")
	public Product setIze_M2ccomunes(BigDecimal value) {
		this.setAttributeValue("ize_m2ccomunes", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_m2construidosdecimal")
	@AttributeLogicalNameAttribute("ize_m2construidosdecimal")
	public BigDecimal getIze_m2construidosdecimal() {
		return this.getAttributeValue("ize_m2construidosdecimal");
	}
	
	@AttributeSchemaNameAttribute("Ize_m2construidosdecimal")
	@AttributeLogicalNameAttribute("ize_m2construidosdecimal")
	public Product setIze_m2construidosdecimal(BigDecimal value) {
		this.setAttributeValue("ize_m2construidosdecimal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_m2edificables")
	@AttributeLogicalNameAttribute("ize_m2edificables")
	public BigDecimal getIze_m2edificables() {
		return this.getAttributeValue("ize_m2edificables");
	}
	
	@AttributeSchemaNameAttribute("Ize_m2edificables")
	@AttributeLogicalNameAttribute("ize_m2edificables")
	public Product setIze_m2edificables(BigDecimal value) {
		this.setAttributeValue("ize_m2edificables", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_m2PlantaBaja")
	@AttributeLogicalNameAttribute("ize_m2plantabaja")
	public BigDecimal getIze_m2PlantaBaja() {
		return this.getAttributeValue("ize_m2plantabaja");
	}
	
	@AttributeSchemaNameAttribute("Ize_m2PlantaBaja")
	@AttributeLogicalNameAttribute("ize_m2plantabaja")
	public Product setIze_m2PlantaBaja(BigDecimal value) {
		this.setAttributeValue("ize_m2plantabaja", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_m2plantaprimera")
	@AttributeLogicalNameAttribute("ize_m2plantaprimera")
	public BigDecimal getIze_m2plantaprimera() {
		return this.getAttributeValue("ize_m2plantaprimera");
	}
	
	@AttributeSchemaNameAttribute("Ize_m2plantaprimera")
	@AttributeLogicalNameAttribute("ize_m2plantaprimera")
	public Product setIze_m2plantaprimera(BigDecimal value) {
		this.setAttributeValue("ize_m2plantaprimera", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_m2plantasegunda")
	@AttributeLogicalNameAttribute("ize_m2plantasegunda")
	public BigDecimal getIze_m2plantasegunda() {
		return this.getAttributeValue("ize_m2plantasegunda");
	}
	
	@AttributeSchemaNameAttribute("Ize_m2plantasegunda")
	@AttributeLogicalNameAttribute("ize_m2plantasegunda")
	public Product setIze_m2plantasegunda(BigDecimal value) {
		this.setAttributeValue("ize_m2plantasegunda", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_m2sotano")
	@AttributeLogicalNameAttribute("ize_m2sotano")
	public BigDecimal getIze_m2sotano() {
		return this.getAttributeValue("ize_m2sotano");
	}
	
	@AttributeSchemaNameAttribute("Ize_m2sotano")
	@AttributeLogicalNameAttribute("ize_m2sotano")
	public Product setIze_m2sotano(BigDecimal value) {
		this.setAttributeValue("ize_m2sotano", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_m2terreno")
	@AttributeLogicalNameAttribute("ize_m2terreno")
	public BigDecimal getIze_m2terreno() {
		return this.getAttributeValue("ize_m2terreno");
	}
	
	@AttributeSchemaNameAttribute("Ize_m2terreno")
	@AttributeLogicalNameAttribute("ize_m2terreno")
	public Product setIze_m2terreno(BigDecimal value) {
		this.setAttributeValue("ize_m2terreno", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_M2tilesdecimal")
	@AttributeLogicalNameAttribute("ize_m2tilesdecimal")
	public BigDecimal getIze_M2tilesdecimal() {
		return this.getAttributeValue("ize_m2tilesdecimal");
	}
	
	@AttributeSchemaNameAttribute("Ize_M2tilesdecimal")
	@AttributeLogicalNameAttribute("ize_m2tilesdecimal")
	public Product setIze_M2tilesdecimal(BigDecimal value) {
		this.setAttributeValue("ize_m2tilesdecimal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Mandato")
	@AttributeLogicalNameAttribute("ize_mandato")
	public OptionSetValue getIze_Mandato() {
		return this.getAttributeValue("ize_mandato");
	}
	
	@AttributeSchemaNameAttribute("Ize_Mandato")
	@AttributeLogicalNameAttribute("ize_mandato")
	public Product setIze_Mandato(OptionSetValue value) {
		this.setAttributeValue("ize_mandato", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Mediocaptacion")
	@AttributeLogicalNameAttribute("ize_mediocaptacion")
	public OptionSetValue getIze_Mediocaptacion() {
		return this.getAttributeValue("ize_mediocaptacion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Mediocaptacion")
	@AttributeLogicalNameAttribute("ize_mediocaptacion")
	public Product setIze_Mediocaptacion(OptionSetValue value) {
		this.setAttributeValue("ize_mediocaptacion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Mobiliarioincluido")
	@AttributeLogicalNameAttribute("ize_mobiliarioincluido")
	public boolean getIze_Mobiliarioincluido() {
		return this.getAttributeValue("ize_mobiliarioincluido");
	}
	
	@AttributeSchemaNameAttribute("Ize_Mobiliarioincluido")
	@AttributeLogicalNameAttribute("ize_mobiliarioincluido")
	public Product setIze_Mobiliarioincluido(boolean value) {
		this.setAttributeValue("ize_mobiliarioincluido", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_MTerrazaJardin")
	@AttributeLogicalNameAttribute("ize_mterrazajardin")
	public BigDecimal getIze_MTerrazaJardin() {
		return this.getAttributeValue("ize_mterrazajardin");
	}
	
	@AttributeSchemaNameAttribute("Ize_MTerrazaJardin")
	@AttributeLogicalNameAttribute("ize_mterrazajardin")
	public Product setIze_MTerrazaJardin(BigDecimal value) {
		this.setAttributeValue("ize_mterrazajardin", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_NumAscensores")
	@AttributeLogicalNameAttribute("ize_numascensores")
	public OptionSetValue getIze_NumAscensores() {
		return this.getAttributeValue("ize_numascensores");
	}
	
	@AttributeSchemaNameAttribute("Ize_NumAscensores")
	@AttributeLogicalNameAttribute("ize_numascensores")
	public Product setIze_NumAscensores(OptionSetValue value) {
		this.setAttributeValue("ize_numascensores", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_numAseos")
	@AttributeLogicalNameAttribute("ize_numaseos")
	public OptionSetValue getIze_numAseos() {
		return this.getAttributeValue("ize_numaseos");
	}
	
	@AttributeSchemaNameAttribute("Ize_numAseos")
	@AttributeLogicalNameAttribute("ize_numaseos")
	public Product setIze_numAseos(OptionSetValue value) {
		this.setAttributeValue("ize_numaseos", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_NumBanios")
	@AttributeLogicalNameAttribute("ize_numbanios")
	public OptionSetValue getIze_NumBanios() {
		return this.getAttributeValue("ize_numbanios");
	}
	
	@AttributeSchemaNameAttribute("Ize_NumBanios")
	@AttributeLogicalNameAttribute("ize_numbanios")
	public Product setIze_NumBanios(OptionSetValue value) {
		this.setAttributeValue("ize_numbanios", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Numeroregistropropiedad")
	@AttributeLogicalNameAttribute("ize_numeroregistropropiedad")
	public String getIze_Numeroregistropropiedad() {
		return this.getAttributeValue("ize_numeroregistropropiedad");
	}
	
	@AttributeSchemaNameAttribute("Ize_Numeroregistropropiedad")
	@AttributeLogicalNameAttribute("ize_numeroregistropropiedad")
	public Product setIze_Numeroregistropropiedad(String value) {
		this.setAttributeValue("ize_numeroregistropropiedad", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_numGarajesincluidos")
	@AttributeLogicalNameAttribute("ize_numgarajesincluidos")
	public int getIze_numGarajesincluidos() {
		return this.getAttributeValue("ize_numgarajesincluidos");
	}
	
	@AttributeSchemaNameAttribute("Ize_numGarajesincluidos")
	@AttributeLogicalNameAttribute("ize_numgarajesincluidos")
	public Product setIze_numGarajesincluidos(int value) {
		this.setAttributeValue("ize_numgarajesincluidos", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Numhabitaciones")
	@AttributeLogicalNameAttribute("ize_numhabitaciones")
	public OptionSetValue getIze_Numhabitaciones() {
		return this.getAttributeValue("ize_numhabitaciones");
	}
	
	@AttributeSchemaNameAttribute("Ize_Numhabitaciones")
	@AttributeLogicalNameAttribute("ize_numhabitaciones")
	public Product setIze_Numhabitaciones(OptionSetValue value) {
		this.setAttributeValue("ize_numhabitaciones", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_numTrasterosincluidos")
	@AttributeLogicalNameAttribute("ize_numtrasterosincluidos")
	public int getIze_numTrasterosincluidos() {
		return this.getAttributeValue("ize_numtrasterosincluidos");
	}
	
	@AttributeSchemaNameAttribute("Ize_numTrasterosincluidos")
	@AttributeLogicalNameAttribute("ize_numtrasterosincluidos")
	public Product setIze_numTrasterosincluidos(int value) {
		this.setAttributeValue("ize_numtrasterosincluidos", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Orientacion")
	@AttributeLogicalNameAttribute("ize_orientacion")
	public OptionSetValue getIze_Orientacion() {
		return this.getAttributeValue("ize_orientacion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Orientacion")
	@AttributeLogicalNameAttribute("ize_orientacion")
	public Product setIze_Orientacion(OptionSetValue value) {
		this.setAttributeValue("ize_orientacion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Parquing")
	@AttributeLogicalNameAttribute("ize_parquing")
	public String getIze_Parquing() {
		return this.getAttributeValue("ize_parquing");
	}
	
	@AttributeSchemaNameAttribute("Ize_Parquing")
	@AttributeLogicalNameAttribute("ize_parquing")
	public Product setIze_Parquing(String value) {
		this.setAttributeValue("ize_parquing", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Piscina")
	@AttributeLogicalNameAttribute("ize_piscina")
	public OptionSetValue getIze_Piscina() {
		return this.getAttributeValue("ize_piscina");
	}
	
	@AttributeSchemaNameAttribute("Ize_Piscina")
	@AttributeLogicalNameAttribute("ize_piscina")
	public Product setIze_Piscina(OptionSetValue value) {
		this.setAttributeValue("ize_piscina", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Piscinas")
	@AttributeLogicalNameAttribute("ize_piscinas")
	public boolean getIze_Piscinas() {
		return this.getAttributeValue("ize_piscinas");
	}
	
	@AttributeSchemaNameAttribute("Ize_Piscinas")
	@AttributeLogicalNameAttribute("ize_piscinas")
	public Product setIze_Piscinas(boolean value) {
		this.setAttributeValue("ize_piscinas", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PisoBanco")
	@AttributeLogicalNameAttribute("ize_pisobanco")
	public boolean getIze_PisoBanco() {
		return this.getAttributeValue("ize_pisobanco");
	}
	
	@AttributeSchemaNameAttribute("Ize_PisoBanco")
	@AttributeLogicalNameAttribute("ize_pisobanco")
	public Product setIze_PisoBanco(boolean value) {
		this.setAttributeValue("ize_pisobanco", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Planta")
	@AttributeLogicalNameAttribute("ize_planta")
	public OptionSetValue getIze_Planta() {
		return this.getAttributeValue("ize_planta");
	}
	
	@AttributeSchemaNameAttribute("Ize_Planta")
	@AttributeLogicalNameAttribute("ize_planta")
	public Product setIze_Planta(OptionSetValue value) {
		this.setAttributeValue("ize_planta", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Plantasdelinmueble")
	@AttributeLogicalNameAttribute("ize_plantasdelinmueble")
	public String getIze_Plantasdelinmueble() {
		return this.getAttributeValue("ize_plantasdelinmueble");
	}
	
	@AttributeSchemaNameAttribute("Ize_Plantasdelinmueble")
	@AttributeLogicalNameAttribute("ize_plantasdelinmueble")
	public Product setIze_Plantasdelinmueble(String value) {
		this.setAttributeValue("ize_plantasdelinmueble", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Poblacion")
	@AttributeLogicalNameAttribute("ize_poblacion")
	public EntityReference getIze_Poblacion() {
		return this.getAttributeValue("ize_poblacion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Poblacion")
	@AttributeLogicalNameAttribute("ize_poblacion")
	public Product setIze_Poblacion(EntityReference value) {
		this.setAttributeValue("ize_poblacion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PoblacionName")
	@AttributeLogicalNameAttribute("ize_poblacionname")
	public String getIze_PoblacionName() {
		return this.getAttributeValue("ize_poblacionname");
	}
	
	@AttributeSchemaNameAttribute("Ize_PoblacionName")
	@AttributeLogicalNameAttribute("ize_poblacionname")
	public Product setIze_PoblacionName(String value) {
		this.setAttributeValue("ize_poblacionname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Poblaciontexto")
	@AttributeLogicalNameAttribute("ize_poblaciontexto")
	public String getIze_Poblaciontexto() {
		return this.getAttributeValue("ize_poblaciontexto");
	}
	
	@AttributeSchemaNameAttribute("Ize_Poblaciontexto")
	@AttributeLogicalNameAttribute("ize_poblaciontexto")
	public Product setIze_Poblaciontexto(String value) {
		this.setAttributeValue("ize_poblaciontexto", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PortalParcela")
	@AttributeLogicalNameAttribute("ize_portalparcela")
	public String getIze_PortalParcela() {
		return this.getAttributeValue("ize_portalparcela");
	}
	
	@AttributeSchemaNameAttribute("Ize_PortalParcela")
	@AttributeLogicalNameAttribute("ize_portalparcela")
	public Product setIze_PortalParcela(String value) {
		this.setAttributeValue("ize_portalparcela", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Posibilidadsalidahumos")
	@AttributeLogicalNameAttribute("ize_posibilidadsalidahumos")
	public boolean getIze_Posibilidadsalidahumos() {
		return this.getAttributeValue("ize_posibilidadsalidahumos");
	}
	
	@AttributeSchemaNameAttribute("Ize_Posibilidadsalidahumos")
	@AttributeLogicalNameAttribute("ize_posibilidadsalidahumos")
	public Product setIze_Posibilidadsalidahumos(boolean value) {
		this.setAttributeValue("ize_posibilidadsalidahumos", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Posibilidadterraza")
	@AttributeLogicalNameAttribute("ize_posibilidadterraza")
	public boolean getIze_Posibilidadterraza() {
		return this.getAttributeValue("ize_posibilidadterraza");
	}
	
	@AttributeSchemaNameAttribute("Ize_Posibilidadterraza")
	@AttributeLogicalNameAttribute("ize_posibilidadterraza")
	public Product setIze_Posibilidadterraza(boolean value) {
		this.setAttributeValue("ize_posibilidadterraza", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Posicionexacta")
	@AttributeLogicalNameAttribute("ize_posicionexacta")
	public boolean getIze_Posicionexacta() {
		return this.getAttributeValue("ize_posicionexacta");
	}
	
	@AttributeSchemaNameAttribute("Ize_Posicionexacta")
	@AttributeLogicalNameAttribute("ize_posicionexacta")
	public Product setIze_Posicionexacta(boolean value) {
		this.setAttributeValue("ize_posicionexacta", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Precioalquilerenopcion")
	@AttributeLogicalNameAttribute("ize_precioalquilerenopcion")
	public Money getIze_Precioalquilerenopcion() {
		return this.getAttributeValue("ize_precioalquilerenopcion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Precioalquilerenopcion")
	@AttributeLogicalNameAttribute("ize_precioalquilerenopcion")
	public Product setIze_Precioalquilerenopcion(Money value) {
		this.setAttributeValue("ize_precioalquilerenopcion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_precioalquilerenopcion_Base")
	@AttributeLogicalNameAttribute("ize_precioalquilerenopcion_base")
	public Money getIze_precioalquilerenopcion_Base() {
		return this.getAttributeValue("ize_precioalquilerenopcion_base");
	}
	
	@AttributeSchemaNameAttribute("Ize_precioalquilerenopcion_Base")
	@AttributeLogicalNameAttribute("ize_precioalquilerenopcion_base")
	public Product setIze_precioalquilerenopcion_Base(Money value) {
		this.setAttributeValue("ize_precioalquilerenopcion_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PrecioAlquilermconst")
	@AttributeLogicalNameAttribute("ize_precioalquilermconst")
	public Money getIze_PrecioAlquilermconst() {
		return this.getAttributeValue("ize_precioalquilermconst");
	}
	
	@AttributeSchemaNameAttribute("Ize_PrecioAlquilermconst")
	@AttributeLogicalNameAttribute("ize_precioalquilermconst")
	public Product setIze_PrecioAlquilermconst(Money value) {
		this.setAttributeValue("ize_precioalquilermconst", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_precioalquilermconst_Base")
	@AttributeLogicalNameAttribute("ize_precioalquilermconst_base")
	public Money getIze_precioalquilermconst_Base() {
		return this.getAttributeValue("ize_precioalquilermconst_base");
	}
	
	@AttributeSchemaNameAttribute("Ize_precioalquilermconst_Base")
	@AttributeLogicalNameAttribute("ize_precioalquilermconst_base")
	public Product setIze_precioalquilermconst_Base(Money value) {
		this.setAttributeValue("ize_precioalquilermconst_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PrecioReserva")
	@AttributeLogicalNameAttribute("ize_precioreserva")
	public Money getIze_PrecioReserva() {
		return this.getAttributeValue("ize_precioreserva");
	}
	
	@AttributeSchemaNameAttribute("Ize_PrecioReserva")
	@AttributeLogicalNameAttribute("ize_precioreserva")
	public Product setIze_PrecioReserva(Money value) {
		this.setAttributeValue("ize_precioreserva", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_precioreserva_Base")
	@AttributeLogicalNameAttribute("ize_precioreserva_base")
	public Money getIze_precioreserva_Base() {
		return this.getAttributeValue("ize_precioreserva_base");
	}
	
	@AttributeSchemaNameAttribute("Ize_precioreserva_Base")
	@AttributeLogicalNameAttribute("ize_precioreserva_base")
	public Product setIze_precioreserva_Base(Money value) {
		this.setAttributeValue("ize_precioreserva_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PreciosAlquiler")
	@AttributeLogicalNameAttribute("ize_preciosalquiler")
	public Money getIze_PreciosAlquiler() {
		return this.getAttributeValue("ize_preciosalquiler");
	}
	
	@AttributeSchemaNameAttribute("Ize_PreciosAlquiler")
	@AttributeLogicalNameAttribute("ize_preciosalquiler")
	public Product setIze_PreciosAlquiler(Money value) {
		this.setAttributeValue("ize_preciosalquiler", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_preciosalquiler_Base")
	@AttributeLogicalNameAttribute("ize_preciosalquiler_base")
	public Money getIze_preciosalquiler_Base() {
		return this.getAttributeValue("ize_preciosalquiler_base");
	}
	
	@AttributeSchemaNameAttribute("Ize_preciosalquiler_Base")
	@AttributeLogicalNameAttribute("ize_preciosalquiler_base")
	public Product setIze_preciosalquiler_Base(Money value) {
		this.setAttributeValue("ize_preciosalquiler_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PrecioTraspaso")
	@AttributeLogicalNameAttribute("ize_preciotraspaso")
	public Money getIze_PrecioTraspaso() {
		return this.getAttributeValue("ize_preciotraspaso");
	}
	
	@AttributeSchemaNameAttribute("Ize_PrecioTraspaso")
	@AttributeLogicalNameAttribute("ize_preciotraspaso")
	public Product setIze_PrecioTraspaso(Money value) {
		this.setAttributeValue("ize_preciotraspaso", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_preciotraspaso_Base")
	@AttributeLogicalNameAttribute("ize_preciotraspaso_base")
	public Money getIze_preciotraspaso_Base() {
		return this.getAttributeValue("ize_preciotraspaso_base");
	}
	
	@AttributeSchemaNameAttribute("Ize_preciotraspaso_Base")
	@AttributeLogicalNameAttribute("ize_preciotraspaso_base")
	public Product setIze_preciotraspaso_Base(Money value) {
		this.setAttributeValue("ize_preciotraspaso_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PrecioVentam2constr")
	@AttributeLogicalNameAttribute("ize_precioventam2constr")
	public Money getIze_PrecioVentam2constr() {
		return this.getAttributeValue("ize_precioventam2constr");
	}
	
	@AttributeSchemaNameAttribute("Ize_PrecioVentam2constr")
	@AttributeLogicalNameAttribute("ize_precioventam2constr")
	public Product setIze_PrecioVentam2constr(Money value) {
		this.setAttributeValue("ize_precioventam2constr", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_precioventam2constr_Base")
	@AttributeLogicalNameAttribute("ize_precioventam2constr_base")
	public Money getIze_precioventam2constr_Base() {
		return this.getAttributeValue("ize_precioventam2constr_base");
	}
	
	@AttributeSchemaNameAttribute("Ize_precioventam2constr_Base")
	@AttributeLogicalNameAttribute("ize_precioventam2constr_base")
	public Product setIze_precioventam2constr_Base(Money value) {
		this.setAttributeValue("ize_precioventam2constr_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PrimaOpcion")
	@AttributeLogicalNameAttribute("ize_primaopcion")
	public Money getIze_PrimaOpcion() {
		return this.getAttributeValue("ize_primaopcion");
	}
	
	@AttributeSchemaNameAttribute("Ize_PrimaOpcion")
	@AttributeLogicalNameAttribute("ize_primaopcion")
	public Product setIze_PrimaOpcion(Money value) {
		this.setAttributeValue("ize_primaopcion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_primaopcion_Base")
	@AttributeLogicalNameAttribute("ize_primaopcion_base")
	public Money getIze_primaopcion_Base() {
		return this.getAttributeValue("ize_primaopcion_base");
	}
	
	@AttributeSchemaNameAttribute("Ize_primaopcion_Base")
	@AttributeLogicalNameAttribute("ize_primaopcion_base")
	public Product setIze_primaopcion_Base(Money value) {
		this.setAttributeValue("ize_primaopcion_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PromocinFaseId")
	@AttributeLogicalNameAttribute("ize_promocinfaseid")
	public EntityReference getIze_PromocinFaseId() {
		return this.getAttributeValue("ize_promocinfaseid");
	}
	
	@AttributeSchemaNameAttribute("Ize_PromocinFaseId")
	@AttributeLogicalNameAttribute("ize_promocinfaseid")
	public Product setIze_PromocinFaseId(EntityReference value) {
		this.setAttributeValue("ize_promocinfaseid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PromocinFaseIdName")
	@AttributeLogicalNameAttribute("ize_promocinfaseidname")
	public String getIze_PromocinFaseIdName() {
		return this.getAttributeValue("ize_promocinfaseidname");
	}
	
	@AttributeSchemaNameAttribute("Ize_PromocinFaseIdName")
	@AttributeLogicalNameAttribute("ize_promocinfaseidname")
	public Product setIze_PromocinFaseIdName(String value) {
		this.setAttributeValue("ize_promocinfaseidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PropiedadCuentaId")
	@AttributeLogicalNameAttribute("ize_propiedadcuentaid")
	public EntityReference getIze_PropiedadCuentaId() {
		return this.getAttributeValue("ize_propiedadcuentaid");
	}
	
	@AttributeSchemaNameAttribute("Ize_PropiedadCuentaId")
	@AttributeLogicalNameAttribute("ize_propiedadcuentaid")
	public Product setIze_PropiedadCuentaId(EntityReference value) {
		this.setAttributeValue("ize_propiedadcuentaid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PropiedadCuentaIdName")
	@AttributeLogicalNameAttribute("ize_propiedadcuentaidname")
	public String getIze_PropiedadCuentaIdName() {
		return this.getAttributeValue("ize_propiedadcuentaidname");
	}
	
	@AttributeSchemaNameAttribute("Ize_PropiedadCuentaIdName")
	@AttributeLogicalNameAttribute("ize_propiedadcuentaidname")
	public Product setIze_PropiedadCuentaIdName(String value) {
		this.setAttributeValue("ize_propiedadcuentaidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_PropiedadCuentaIdYomiName")
	@AttributeLogicalNameAttribute("ize_propiedadcuentaidyominame")
	public String getIze_PropiedadCuentaIdYomiName() {
		return this.getAttributeValue("ize_propiedadcuentaidyominame");
	}
	
	@AttributeSchemaNameAttribute("Ize_PropiedadCuentaIdYomiName")
	@AttributeLogicalNameAttribute("ize_propiedadcuentaidyominame")
	public Product setIze_PropiedadCuentaIdYomiName(String value) {
		this.setAttributeValue("ize_propiedadcuentaidyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ProvinciaTexto")
	@AttributeLogicalNameAttribute("ize_provinciatexto")
	public String getIze_ProvinciaTexto() {
		return this.getAttributeValue("ize_provinciatexto");
	}
	
	@AttributeSchemaNameAttribute("Ize_ProvinciaTexto")
	@AttributeLogicalNameAttribute("ize_provinciatexto")
	public Product setIze_ProvinciaTexto(String value) {
		this.setAttributeValue("ize_provinciatexto", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_publicarenweb")
	@AttributeLogicalNameAttribute("ize_publicarenweb")
	public boolean getIze_publicarenweb() {
		return this.getAttributeValue("ize_publicarenweb");
	}
	
	@AttributeSchemaNameAttribute("Ize_publicarenweb")
	@AttributeLogicalNameAttribute("ize_publicarenweb")
	public Product setIze_publicarenweb(boolean value) {
		this.setAttributeValue("ize_publicarenweb", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_RefCatastral")
	@AttributeLogicalNameAttribute("ize_refcatastral")
	public String getIze_RefCatastral() {
		return this.getAttributeValue("ize_refcatastral");
	}
	
	@AttributeSchemaNameAttribute("Ize_RefCatastral")
	@AttributeLogicalNameAttribute("ize_refcatastral")
	public Product setIze_RefCatastral(String value) {
		this.setAttributeValue("ize_refcatastral", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Refllaves")
	@AttributeLogicalNameAttribute("ize_refllaves")
	public String getIze_Refllaves() {
		return this.getAttributeValue("ize_refllaves");
	}
	
	@AttributeSchemaNameAttribute("Ize_Refllaves")
	@AttributeLogicalNameAttribute("ize_refllaves")
	public Product setIze_Refllaves(String value) {
		this.setAttributeValue("ize_refllaves", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Reformado")
	@AttributeLogicalNameAttribute("ize_reformado")
	public boolean getIze_Reformado() {
		return this.getAttributeValue("ize_reformado");
	}
	
	@AttributeSchemaNameAttribute("Ize_Reformado")
	@AttributeLogicalNameAttribute("ize_reformado")
	public Product setIze_Reformado(boolean value) {
		this.setAttributeValue("ize_reformado", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Rentabilidad")
	@AttributeLogicalNameAttribute("ize_rentabilidad")
	public BigDecimal getIze_Rentabilidad() {
		return this.getAttributeValue("ize_rentabilidad");
	}
	
	@AttributeSchemaNameAttribute("Ize_Rentabilidad")
	@AttributeLogicalNameAttribute("ize_rentabilidad")
	public Product setIze_Rentabilidad(BigDecimal value) {
		this.setAttributeValue("ize_rentabilidad", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Reservadoweb")
	@AttributeLogicalNameAttribute("ize_reservadoweb")
	public boolean getIze_Reservadoweb() {
		return this.getAttributeValue("ize_reservadoweb");
	}
	
	@AttributeSchemaNameAttribute("Ize_Reservadoweb")
	@AttributeLogicalNameAttribute("ize_reservadoweb")
	public Product setIze_Reservadoweb(boolean value) {
		this.setAttributeValue("ize_reservadoweb", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Restaurante")
	@AttributeLogicalNameAttribute("ize_restaurante")
	public boolean getIze_Restaurante() {
		return this.getAttributeValue("ize_restaurante");
	}
	
	@AttributeSchemaNameAttribute("Ize_Restaurante")
	@AttributeLogicalNameAttribute("ize_restaurante")
	public Product setIze_Restaurante(boolean value) {
		this.setAttributeValue("ize_restaurante", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Salidadehumos")
	@AttributeLogicalNameAttribute("ize_salidadehumos")
	public boolean getIze_Salidadehumos() {
		return this.getAttributeValue("ize_salidadehumos");
	}
	
	@AttributeSchemaNameAttribute("Ize_Salidadehumos")
	@AttributeLogicalNameAttribute("ize_salidadehumos")
	public Product setIze_Salidadehumos(boolean value) {
		this.setAttributeValue("ize_salidadehumos", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_send_bdui")
	@AttributeLogicalNameAttribute("ize_send_bdui")
	public boolean getIze_send_bdui() {
		return this.getAttributeValue("ize_send_bdui");
	}
	
	@AttributeSchemaNameAttribute("Ize_send_bdui")
	@AttributeLogicalNameAttribute("ize_send_bdui")
	public Product setIze_send_bdui(boolean value) {
		this.setAttributeValue("ize_send_bdui", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_ServicioConserjeria")
	@AttributeLogicalNameAttribute("ize_servicioconserjeria")
	public OptionSetValue getIze_ServicioConserjeria() {
		return this.getAttributeValue("ize_servicioconserjeria");
	}
	
	@AttributeSchemaNameAttribute("Ize_ServicioConserjeria")
	@AttributeLogicalNameAttribute("ize_servicioconserjeria")
	public Product setIze_ServicioConserjeria(OptionSetValue value) {
		this.setAttributeValue("ize_servicioconserjeria", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Sinbarrerasarquitectonicas")
	@AttributeLogicalNameAttribute("ize_sinbarrerasarquitectonicas")
	public boolean getIze_Sinbarrerasarquitectonicas() {
		return this.getAttributeValue("ize_sinbarrerasarquitectonicas");
	}
	
	@AttributeSchemaNameAttribute("Ize_Sinbarrerasarquitectonicas")
	@AttributeLogicalNameAttribute("ize_sinbarrerasarquitectonicas")
	public Product setIze_Sinbarrerasarquitectonicas(boolean value) {
		this.setAttributeValue("ize_sinbarrerasarquitectonicas", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Situacion")
	@AttributeLogicalNameAttribute("ize_situacion")
	public String getIze_Situacion() {
		return this.getAttributeValue("ize_situacion");
	}
	
	@AttributeSchemaNameAttribute("Ize_Situacion")
	@AttributeLogicalNameAttribute("ize_situacion")
	public Product setIze_Situacion(String value) {
		this.setAttributeValue("ize_situacion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_situacioncomercial")
	@AttributeLogicalNameAttribute("ize_situacioncomercial")
	public OptionSetValue getIze_situacioncomercial() {
		return this.getAttributeValue("ize_situacioncomercial");
	}
	
	@AttributeSchemaNameAttribute("Ize_situacioncomercial")
	@AttributeLogicalNameAttribute("ize_situacioncomercial")
	public Product setIze_situacioncomercial(OptionSetValue value) {
		this.setAttributeValue("ize_situacioncomercial", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_subtipologia")
	@AttributeLogicalNameAttribute("ize_subtipologia")
	public OptionSetValue getIze_subtipologia() {
		return this.getAttributeValue("ize_subtipologia");
	}
	
	@AttributeSchemaNameAttribute("Ize_subtipologia")
	@AttributeLogicalNameAttribute("ize_subtipologia")
	public Product setIze_subtipologia(OptionSetValue value) {
		this.setAttributeValue("ize_subtipologia", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Terraza")
	@AttributeLogicalNameAttribute("ize_terraza")
	public OptionSetValue getIze_Terraza() {
		return this.getAttributeValue("ize_terraza");
	}
	
	@AttributeSchemaNameAttribute("Ize_Terraza")
	@AttributeLogicalNameAttribute("ize_terraza")
	public Product setIze_Terraza(OptionSetValue value) {
		this.setAttributeValue("ize_terraza", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Terrazas")
	@AttributeLogicalNameAttribute("ize_terrazas")
	public boolean getIze_Terrazas() {
		return this.getAttributeValue("ize_terrazas");
	}
	
	@AttributeSchemaNameAttribute("Ize_Terrazas")
	@AttributeLogicalNameAttribute("ize_terrazas")
	public Product setIze_Terrazas(boolean value) {
		this.setAttributeValue("ize_terrazas", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_TipoEdificio")
	@AttributeLogicalNameAttribute("ize_tipoedificio")
	public OptionSetValue getIze_TipoEdificio() {
		return this.getAttributeValue("ize_tipoedificio");
	}
	
	@AttributeSchemaNameAttribute("Ize_TipoEdificio")
	@AttributeLogicalNameAttribute("ize_tipoedificio")
	public Product setIze_TipoEdificio(OptionSetValue value) {
		this.setAttributeValue("ize_tipoedificio", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_tipologia")
	@AttributeLogicalNameAttribute("ize_tipologia")
	public OptionSetValue getIze_tipologia() {
		return this.getAttributeValue("ize_tipologia");
	}
	
	@AttributeSchemaNameAttribute("Ize_tipologia")
	@AttributeLogicalNameAttribute("ize_tipologia")
	public Product setIze_tipologia(OptionSetValue value) {
		this.setAttributeValue("ize_tipologia", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Tomo")
	@AttributeLogicalNameAttribute("ize_tomo")
	public String getIze_Tomo() {
		return this.getAttributeValue("ize_tomo");
	}
	
	@AttributeSchemaNameAttribute("Ize_Tomo")
	@AttributeLogicalNameAttribute("ize_tomo")
	public Product setIze_Tomo(String value) {
		this.setAttributeValue("ize_tomo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Traspaso")
	@AttributeLogicalNameAttribute("ize_traspaso")
	public OptionSetValue getIze_Traspaso() {
		return this.getAttributeValue("ize_traspaso");
	}
	
	@AttributeSchemaNameAttribute("Ize_Traspaso")
	@AttributeLogicalNameAttribute("ize_traspaso")
	public Product setIze_Traspaso(OptionSetValue value) {
		this.setAttributeValue("ize_traspaso", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Trastero")
	@AttributeLogicalNameAttribute("ize_trastero")
	public OptionSetValue getIze_Trastero() {
		return this.getAttributeValue("ize_trastero");
	}
	
	@AttributeSchemaNameAttribute("Ize_Trastero")
	@AttributeLogicalNameAttribute("ize_trastero")
	public Product setIze_Trastero(OptionSetValue value) {
		this.setAttributeValue("ize_trastero", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Trasteros")
	@AttributeLogicalNameAttribute("ize_trasteros")
	public boolean getIze_Trasteros() {
		return this.getAttributeValue("ize_trasteros");
	}
	
	@AttributeSchemaNameAttribute("Ize_Trasteros")
	@AttributeLogicalNameAttribute("ize_trasteros")
	public Product setIze_Trasteros(boolean value) {
		this.setAttributeValue("ize_trasteros", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Ubicacionllaves")
	@AttributeLogicalNameAttribute("ize_ubicacionllaves")
	public String getIze_Ubicacionllaves() {
		return this.getAttributeValue("ize_ubicacionllaves");
	}
	
	@AttributeSchemaNameAttribute("Ize_Ubicacionllaves")
	@AttributeLogicalNameAttribute("ize_ubicacionllaves")
	public Product setIze_Ubicacionllaves(String value) {
		this.setAttributeValue("ize_ubicacionllaves", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Varlorcatastral")
	@AttributeLogicalNameAttribute("ize_varlorcatastral")
	public Money getIze_Varlorcatastral() {
		return this.getAttributeValue("ize_varlorcatastral");
	}
	
	@AttributeSchemaNameAttribute("Ize_Varlorcatastral")
	@AttributeLogicalNameAttribute("ize_varlorcatastral")
	public Product setIze_Varlorcatastral(Money value) {
		this.setAttributeValue("ize_varlorcatastral", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_varlorcatastral_Base")
	@AttributeLogicalNameAttribute("ize_varlorcatastral_base")
	public Money getIze_varlorcatastral_Base() {
		return this.getAttributeValue("ize_varlorcatastral_base");
	}
	
	@AttributeSchemaNameAttribute("Ize_varlorcatastral_Base")
	@AttributeLogicalNameAttribute("ize_varlorcatastral_base")
	public Product setIze_varlorcatastral_Base(Money value) {
		this.setAttributeValue("ize_varlorcatastral_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Venta")
	@AttributeLogicalNameAttribute("ize_venta")
	public OptionSetValue getIze_Venta() {
		return this.getAttributeValue("ize_venta");
	}
	
	@AttributeSchemaNameAttribute("Ize_Venta")
	@AttributeLogicalNameAttribute("ize_venta")
	public Product setIze_Venta(OptionSetValue value) {
		this.setAttributeValue("ize_venta", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Vistas")
	@AttributeLogicalNameAttribute("ize_vistas")
	public OptionSetValue getIze_Vistas() {
		return this.getAttributeValue("ize_vistas");
	}
	
	@AttributeSchemaNameAttribute("Ize_Vistas")
	@AttributeLogicalNameAttribute("ize_vistas")
	public Product setIze_Vistas(OptionSetValue value) {
		this.setAttributeValue("ize_vistas", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Vpo")
	@AttributeLogicalNameAttribute("ize_vpo")
	public boolean getIze_Vpo() {
		return this.getAttributeValue("ize_vpo");
	}
	
	@AttributeSchemaNameAttribute("Ize_Vpo")
	@AttributeLogicalNameAttribute("ize_vpo")
	public Product setIze_Vpo(boolean value) {
		this.setAttributeValue("ize_vpo", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Ize_Zonatexto")
	@AttributeLogicalNameAttribute("ize_zonatexto")
	public String getIze_Zonatexto() {
		return this.getAttributeValue("ize_zonatexto");
	}
	
	@AttributeSchemaNameAttribute("Ize_Zonatexto")
	@AttributeLogicalNameAttribute("ize_zonatexto")
	public Product setIze_Zonatexto(String value) {
		this.setAttributeValue("ize_zonatexto", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedBy")
	@AttributeLogicalNameAttribute("modifiedby")
	public EntityReference getModifiedBy() {
		return this.getAttributeValue("modifiedby");
	}
	
	@AttributeSchemaNameAttribute("ModifiedBy")
	@AttributeLogicalNameAttribute("modifiedby")
	public Product setModifiedBy(EntityReference value) {
		this.setAttributeValue("modifiedby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedByName")
	@AttributeLogicalNameAttribute("modifiedbyname")
	public String getModifiedByName() {
		return this.getAttributeValue("modifiedbyname");
	}
	
	@AttributeSchemaNameAttribute("ModifiedByName")
	@AttributeLogicalNameAttribute("modifiedbyname")
	public Product setModifiedByName(String value) {
		this.setAttributeValue("modifiedbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedByYomiName")
	@AttributeLogicalNameAttribute("modifiedbyyominame")
	public String getModifiedByYomiName() {
		return this.getAttributeValue("modifiedbyyominame");
	}
	
	@AttributeSchemaNameAttribute("ModifiedByYomiName")
	@AttributeLogicalNameAttribute("modifiedbyyominame")
	public Product setModifiedByYomiName(String value) {
		this.setAttributeValue("modifiedbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOn")
	@AttributeLogicalNameAttribute("modifiedon")
	public Date getModifiedOn() {
		return this.getAttributeValue("modifiedon");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOn")
	@AttributeLogicalNameAttribute("modifiedon")
	public Product setModifiedOn(Date value) {
		this.setAttributeValue("modifiedon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfBy")
	@AttributeLogicalNameAttribute("modifiedonbehalfby")
	public EntityReference getModifiedOnBehalfBy() {
		return this.getAttributeValue("modifiedonbehalfby");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfBy")
	@AttributeLogicalNameAttribute("modifiedonbehalfby")
	public Product setModifiedOnBehalfBy(EntityReference value) {
		this.setAttributeValue("modifiedonbehalfby", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfByName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyname")
	public String getModifiedOnBehalfByName() {
		return this.getAttributeValue("modifiedonbehalfbyname");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfByName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyname")
	public Product setModifiedOnBehalfByName(String value) {
		this.setAttributeValue("modifiedonbehalfbyname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ModifiedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyyominame")
	public String getModifiedOnBehalfByYomiName() {
		return this.getAttributeValue("modifiedonbehalfbyyominame");
	}
	
	@AttributeSchemaNameAttribute("ModifiedOnBehalfByYomiName")
	@AttributeLogicalNameAttribute("modifiedonbehalfbyyominame")
	public Product setModifiedOnBehalfByYomiName(String value) {
		this.setAttributeValue("modifiedonbehalfbyyominame", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Name")
	@AttributeLogicalNameAttribute("name")
	public String getName() {
		return this.getAttributeValue("name");
	}
	
	@AttributeSchemaNameAttribute("Name")
	@AttributeLogicalNameAttribute("name")
	public Product setName(String value) {
		this.setAttributeValue("name", value);
		return this;
	}

	@AttributeSchemaNameAttribute("New_Test_actualizacion")
	@AttributeLogicalNameAttribute("new_test_actualizacion")
	public String getNew_Test_actualizacion() {
		return this.getAttributeValue("new_test_actualizacion");
	}
	
	@AttributeSchemaNameAttribute("New_Test_actualizacion")
	@AttributeLogicalNameAttribute("new_test_actualizacion")
	public Product setNew_Test_actualizacion(String value) {
		this.setAttributeValue("new_test_actualizacion", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OrganizationId")
	@AttributeLogicalNameAttribute("organizationid")
	public EntityReference getOrganizationId() {
		return this.getAttributeValue("organizationid");
	}
	
	@AttributeSchemaNameAttribute("OrganizationId")
	@AttributeLogicalNameAttribute("organizationid")
	public Product setOrganizationId(EntityReference value) {
		this.setAttributeValue("organizationid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OrganizationIdName")
	@AttributeLogicalNameAttribute("organizationidname")
	public String getOrganizationIdName() {
		return this.getAttributeValue("organizationidname");
	}
	
	@AttributeSchemaNameAttribute("OrganizationIdName")
	@AttributeLogicalNameAttribute("organizationidname")
	public Product setOrganizationIdName(String value) {
		this.setAttributeValue("organizationidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("OverriddenCreatedOn")
	@AttributeLogicalNameAttribute("overriddencreatedon")
	public Date getOverriddenCreatedOn() {
		return this.getAttributeValue("overriddencreatedon");
	}
	
	@AttributeSchemaNameAttribute("OverriddenCreatedOn")
	@AttributeLogicalNameAttribute("overriddencreatedon")
	public Product setOverriddenCreatedOn(Date value) {
		this.setAttributeValue("overriddencreatedon", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ParentProductId")
	@AttributeLogicalNameAttribute("parentproductid")
	public EntityReference getParentProductId() {
		return this.getAttributeValue("parentproductid");
	}
	
	@AttributeSchemaNameAttribute("ParentProductId")
	@AttributeLogicalNameAttribute("parentproductid")
	public Product setParentProductId(EntityReference value) {
		this.setAttributeValue("parentproductid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ParentProductIdName")
	@AttributeLogicalNameAttribute("parentproductidname")
	public String getParentProductIdName() {
		return this.getAttributeValue("parentproductidname");
	}
	
	@AttributeSchemaNameAttribute("ParentProductIdName")
	@AttributeLogicalNameAttribute("parentproductidname")
	public Product setParentProductIdName(String value) {
		this.setAttributeValue("parentproductidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Price")
	@AttributeLogicalNameAttribute("price")
	public Money getPrice() {
		return this.getAttributeValue("price");
	}
	
	@AttributeSchemaNameAttribute("Price")
	@AttributeLogicalNameAttribute("price")
	public Product setPrice(Money value) {
		this.setAttributeValue("price", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Price_Base")
	@AttributeLogicalNameAttribute("price_base")
	public Money getPrice_Base() {
		return this.getAttributeValue("price_base");
	}
	
	@AttributeSchemaNameAttribute("Price_Base")
	@AttributeLogicalNameAttribute("price_base")
	public Product setPrice_Base(Money value) {
		this.setAttributeValue("price_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("PriceLevelId")
	@AttributeLogicalNameAttribute("pricelevelid")
	public EntityReference getPriceLevelId() {
		return this.getAttributeValue("pricelevelid");
	}
	
	@AttributeSchemaNameAttribute("PriceLevelId")
	@AttributeLogicalNameAttribute("pricelevelid")
	public Product setPriceLevelId(EntityReference value) {
		this.setAttributeValue("pricelevelid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("PriceLevelIdName")
	@AttributeLogicalNameAttribute("pricelevelidname")
	public String getPriceLevelIdName() {
		return this.getAttributeValue("pricelevelidname");
	}
	
	@AttributeSchemaNameAttribute("PriceLevelIdName")
	@AttributeLogicalNameAttribute("pricelevelidname")
	public Product setPriceLevelIdName(String value) {
		this.setAttributeValue("pricelevelidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ProcessId")
	@AttributeLogicalNameAttribute("processid")
	public UUID getProcessId() {
		return this.getAttributeValue("processid");
	}
	
	@AttributeSchemaNameAttribute("ProcessId")
	@AttributeLogicalNameAttribute("processid")
	public Product setProcessId(UUID value) {
		this.setAttributeValue("processid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ProductNumber")
	@AttributeLogicalNameAttribute("productnumber")
	public String getProductNumber() {
		return this.getAttributeValue("productnumber");
	}
	
	@AttributeSchemaNameAttribute("ProductNumber")
	@AttributeLogicalNameAttribute("productnumber")
	public Product setProductNumber(String value) {
		this.setAttributeValue("productnumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ProductStructure")
	@AttributeLogicalNameAttribute("productstructure")
	public OptionSetValue getProductStructure() {
		return this.getAttributeValue("productstructure");
	}
	
	@AttributeSchemaNameAttribute("ProductStructure")
	@AttributeLogicalNameAttribute("productstructure")
	public Product setProductStructure(OptionSetValue value) {
		this.setAttributeValue("productstructure", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ProductTypeCode")
	@AttributeLogicalNameAttribute("producttypecode")
	public OptionSetValue getProductTypeCode() {
		return this.getAttributeValue("producttypecode");
	}
	
	@AttributeSchemaNameAttribute("ProductTypeCode")
	@AttributeLogicalNameAttribute("producttypecode")
	public Product setProductTypeCode(OptionSetValue value) {
		this.setAttributeValue("producttypecode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ProductUrl")
	@AttributeLogicalNameAttribute("producturl")
	public String getProductUrl() {
		return this.getAttributeValue("producturl");
	}
	
	@AttributeSchemaNameAttribute("ProductUrl")
	@AttributeLogicalNameAttribute("producturl")
	public Product setProductUrl(String value) {
		this.setAttributeValue("producturl", value);
		return this;
	}

	@AttributeSchemaNameAttribute("QuantityDecimal")
	@AttributeLogicalNameAttribute("quantitydecimal")
	public int getQuantityDecimal() {
		return this.getAttributeValue("quantitydecimal");
	}
	
	@AttributeSchemaNameAttribute("QuantityDecimal")
	@AttributeLogicalNameAttribute("quantitydecimal")
	public Product setQuantityDecimal(int value) {
		this.setAttributeValue("quantitydecimal", value);
		return this;
	}

	@AttributeSchemaNameAttribute("QuantityOnHand")
	@AttributeLogicalNameAttribute("quantityonhand")
	public BigDecimal getQuantityOnHand() {
		return this.getAttributeValue("quantityonhand");
	}
	
	@AttributeSchemaNameAttribute("QuantityOnHand")
	@AttributeLogicalNameAttribute("quantityonhand")
	public Product setQuantityOnHand(BigDecimal value) {
		this.setAttributeValue("quantityonhand", value);
		return this;
	}

	@AttributeSchemaNameAttribute("Size")
	@AttributeLogicalNameAttribute("size")
	public String getSize() {
		return this.getAttributeValue("size");
	}
	
	@AttributeSchemaNameAttribute("Size")
	@AttributeLogicalNameAttribute("size")
	public Product setSize(String value) {
		this.setAttributeValue("size", value);
		return this;
	}

	@AttributeSchemaNameAttribute("StageId")
	@AttributeLogicalNameAttribute("stageid")
	public UUID getStageId() {
		return this.getAttributeValue("stageid");
	}
	
	@AttributeSchemaNameAttribute("StageId")
	@AttributeLogicalNameAttribute("stageid")
	public Product setStageId(UUID value) {
		this.setAttributeValue("stageid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("StandardCost")
	@AttributeLogicalNameAttribute("standardcost")
	public Money getStandardCost() {
		return this.getAttributeValue("standardcost");
	}
	
	@AttributeSchemaNameAttribute("StandardCost")
	@AttributeLogicalNameAttribute("standardcost")
	public Product setStandardCost(Money value) {
		this.setAttributeValue("standardcost", value);
		return this;
	}

	@AttributeSchemaNameAttribute("StandardCost_Base")
	@AttributeLogicalNameAttribute("standardcost_base")
	public Money getStandardCost_Base() {
		return this.getAttributeValue("standardcost_base");
	}
	
	@AttributeSchemaNameAttribute("StandardCost_Base")
	@AttributeLogicalNameAttribute("standardcost_base")
	public Product setStandardCost_Base(Money value) {
		this.setAttributeValue("standardcost_base", value);
		return this;
	}

	@AttributeSchemaNameAttribute("StateCode")
	@AttributeLogicalNameAttribute("statecode")
	public OptionSetValue getStateCode() {
		return this.getAttributeValue("statecode");
	}
	
	@AttributeSchemaNameAttribute("StateCode")
	@AttributeLogicalNameAttribute("statecode")
	public Product setStateCode(OptionSetValue value) {
		this.setAttributeValue("statecode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("StatusCode")
	@AttributeLogicalNameAttribute("statuscode")
	public OptionSetValue getStatusCode() {
		return this.getAttributeValue("statuscode");
	}
	
	@AttributeSchemaNameAttribute("StatusCode")
	@AttributeLogicalNameAttribute("statuscode")
	public Product setStatusCode(OptionSetValue value) {
		this.setAttributeValue("statuscode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("StockVolume")
	@AttributeLogicalNameAttribute("stockvolume")
	public BigDecimal getStockVolume() {
		return this.getAttributeValue("stockvolume");
	}
	
	@AttributeSchemaNameAttribute("StockVolume")
	@AttributeLogicalNameAttribute("stockvolume")
	public Product setStockVolume(BigDecimal value) {
		this.setAttributeValue("stockvolume", value);
		return this;
	}

	@AttributeSchemaNameAttribute("StockWeight")
	@AttributeLogicalNameAttribute("stockweight")
	public BigDecimal getStockWeight() {
		return this.getAttributeValue("stockweight");
	}
	
	@AttributeSchemaNameAttribute("StockWeight")
	@AttributeLogicalNameAttribute("stockweight")
	public Product setStockWeight(BigDecimal value) {
		this.setAttributeValue("stockweight", value);
		return this;
	}

	@AttributeSchemaNameAttribute("SubjectId")
	@AttributeLogicalNameAttribute("subjectid")
	public EntityReference getSubjectId() {
		return this.getAttributeValue("subjectid");
	}
	
	@AttributeSchemaNameAttribute("SubjectId")
	@AttributeLogicalNameAttribute("subjectid")
	public Product setSubjectId(EntityReference value) {
		this.setAttributeValue("subjectid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("SubjectIdName")
	@AttributeLogicalNameAttribute("subjectidname")
	public String getSubjectIdName() {
		return this.getAttributeValue("subjectidname");
	}
	
	@AttributeSchemaNameAttribute("SubjectIdName")
	@AttributeLogicalNameAttribute("subjectidname")
	public Product setSubjectIdName(String value) {
		this.setAttributeValue("subjectidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("SupplierName")
	@AttributeLogicalNameAttribute("suppliername")
	public String getSupplierName() {
		return this.getAttributeValue("suppliername");
	}
	
	@AttributeSchemaNameAttribute("SupplierName")
	@AttributeLogicalNameAttribute("suppliername")
	public Product setSupplierName(String value) {
		this.setAttributeValue("suppliername", value);
		return this;
	}

	@AttributeSchemaNameAttribute("TimeZoneRuleVersionNumber")
	@AttributeLogicalNameAttribute("timezoneruleversionnumber")
	public int getTimeZoneRuleVersionNumber() {
		return this.getAttributeValue("timezoneruleversionnumber");
	}
	
	@AttributeSchemaNameAttribute("TimeZoneRuleVersionNumber")
	@AttributeLogicalNameAttribute("timezoneruleversionnumber")
	public Product setTimeZoneRuleVersionNumber(int value) {
		this.setAttributeValue("timezoneruleversionnumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("TransactionCurrencyId")
	@AttributeLogicalNameAttribute("transactioncurrencyid")
	public EntityReference getTransactionCurrencyId() {
		return this.getAttributeValue("transactioncurrencyid");
	}
	
	@AttributeSchemaNameAttribute("TransactionCurrencyId")
	@AttributeLogicalNameAttribute("transactioncurrencyid")
	public Product setTransactionCurrencyId(EntityReference value) {
		this.setAttributeValue("transactioncurrencyid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("TransactionCurrencyIdName")
	@AttributeLogicalNameAttribute("transactioncurrencyidname")
	public String getTransactionCurrencyIdName() {
		return this.getAttributeValue("transactioncurrencyidname");
	}
	
	@AttributeSchemaNameAttribute("TransactionCurrencyIdName")
	@AttributeLogicalNameAttribute("transactioncurrencyidname")
	public Product setTransactionCurrencyIdName(String value) {
		this.setAttributeValue("transactioncurrencyidname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("TraversedPath")
	@AttributeLogicalNameAttribute("traversedpath")
	public String getTraversedPath() {
		return this.getAttributeValue("traversedpath");
	}
	
	@AttributeSchemaNameAttribute("TraversedPath")
	@AttributeLogicalNameAttribute("traversedpath")
	public Product setTraversedPath(String value) {
		this.setAttributeValue("traversedpath", value);
		return this;
	}

	@AttributeSchemaNameAttribute("UTCConversionTimeZoneCode")
	@AttributeLogicalNameAttribute("utcconversiontimezonecode")
	public int getUTCConversionTimeZoneCode() {
		return this.getAttributeValue("utcconversiontimezonecode");
	}
	
	@AttributeSchemaNameAttribute("UTCConversionTimeZoneCode")
	@AttributeLogicalNameAttribute("utcconversiontimezonecode")
	public Product setUTCConversionTimeZoneCode(int value) {
		this.setAttributeValue("utcconversiontimezonecode", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ValidFromDate")
	@AttributeLogicalNameAttribute("validfromdate")
	public Date getValidFromDate() {
		return this.getAttributeValue("validfromdate");
	}
	
	@AttributeSchemaNameAttribute("ValidFromDate")
	@AttributeLogicalNameAttribute("validfromdate")
	public Product setValidFromDate(Date value) {
		this.setAttributeValue("validfromdate", value);
		return this;
	}

	@AttributeSchemaNameAttribute("ValidToDate")
	@AttributeLogicalNameAttribute("validtodate")
	public Date getValidToDate() {
		return this.getAttributeValue("validtodate");
	}
	
	@AttributeSchemaNameAttribute("ValidToDate")
	@AttributeLogicalNameAttribute("validtodate")
	public Product setValidToDate(Date value) {
		this.setAttributeValue("validtodate", value);
		return this;
	}

	@AttributeSchemaNameAttribute("VendorID")
	@AttributeLogicalNameAttribute("vendorid")
	public String getVendorID() {
		return this.getAttributeValue("vendorid");
	}
	
	@AttributeSchemaNameAttribute("VendorID")
	@AttributeLogicalNameAttribute("vendorid")
	public Product setVendorID(String value) {
		this.setAttributeValue("vendorid", value);
		return this;
	}

	@AttributeSchemaNameAttribute("VendorName")
	@AttributeLogicalNameAttribute("vendorname")
	public String getVendorName() {
		return this.getAttributeValue("vendorname");
	}
	
	@AttributeSchemaNameAttribute("VendorName")
	@AttributeLogicalNameAttribute("vendorname")
	public Product setVendorName(String value) {
		this.setAttributeValue("vendorname", value);
		return this;
	}

	@AttributeSchemaNameAttribute("VendorPartNumber")
	@AttributeLogicalNameAttribute("vendorpartnumber")
	public String getVendorPartNumber() {
		return this.getAttributeValue("vendorpartnumber");
	}
	
	@AttributeSchemaNameAttribute("VendorPartNumber")
	@AttributeLogicalNameAttribute("vendorpartnumber")
	public Product setVendorPartNumber(String value) {
		this.setAttributeValue("vendorpartnumber", value);
		return this;
	}

	@AttributeSchemaNameAttribute("VersionNumber")
	@AttributeLogicalNameAttribute("versionnumber")
	public BigInteger getVersionNumber() {
		return this.getAttributeValue("versionnumber");
	}
	
	@AttributeSchemaNameAttribute("VersionNumber")
	@AttributeLogicalNameAttribute("versionnumber")
	public Product setVersionNumber(BigInteger value) {
		this.setAttributeValue("versionnumber", value);
		return this;
	}

}

