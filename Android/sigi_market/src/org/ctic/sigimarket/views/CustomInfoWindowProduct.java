package org.ctic.sigimarket.views;

import org.ctic.sigimarket.ProductActivity;
import org.ctic.sigimarket.constants.Constants;
import org.ctic.sigimarket.views.listener.OnInfoWindowElemTouchListener;
import org.ctic.sigimarket.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidmapsextensions.Marker;
import com.google.android.gms.maps.model.LatLng;
import com.microsoft.xrm.sdk.client.Product;
import com.microsoft.xrm.sdk.client.ize.ize_opciones;
import com.microsoft.xrm.sdk.client.ize.ize_tipologia;

public class CustomInfoWindowProduct extends LinearLayout {

	private ImageButton infoButton_task;
	private ImageButton navigationButton_task;
	private OnInfoWindowElemTouchListener infoButtonListener_task;
	private OnInfoWindowElemTouchListener navigationButtonListener_task;
	
	private SharedPreferences sharedPreferences;	
	private final String usePluginsPrefs = "sigiUsePluginsPrefs";

	private View viewInfoWindow;

	private Product product;

	private String mode_map;

	private LatLng user_location;

	public CustomInfoWindowProduct(Context context) {
		super(context);
		
		init();

	}

	private void init() {
		
		sharedPreferences = getContext().getSharedPreferences(usePluginsPrefs, getContext().MODE_PRIVATE);

		viewInfoWindow = ((Activity) getContext()).getLayoutInflater().inflate(
				R.layout.custom_window_product_marker, null);

		infoButton_task = (ImageButton) viewInfoWindow
				.findViewById(R.id.imageButton_disclosure);
		navigationButton_task = (ImageButton) viewInfoWindow
				.findViewById(R.id.imageButton_location);

		this.infoButtonListener_task = new OnInfoWindowElemTouchListener(
				infoButton_task, getResources().getDrawable(
						R.drawable.ic_disclosure), getResources().getDrawable(
						R.drawable.ic_disclosure)) {

			@Override
			protected void onClickConfirmed(View v, Marker marker) {

				

					Intent intent = new Intent(getContext(), ProductActivity.class);					
					getContext().startActivity(intent);
				

			}
		};

		this.infoButton_task.setOnTouchListener(infoButtonListener_task);

		this.navigationButtonListener_task = new OnInfoWindowElemTouchListener(
				navigationButton_task, getResources().getDrawable(
						R.drawable.ic_location_directions), getResources()
						.getDrawable(R.drawable.ic_location_directions)) {

			@Override
			protected void onClickConfirmed(View v, Marker marker) {

				String mode_navigation = "h";

				
				if(mode_map!=null){
				
				if (mode_map.equals("driving"))
					mode_navigation = "h";
				else
					mode_navigation = "w";
				}

				Intent intent = new Intent(Intent.ACTION_VIEW,

				Uri.parse("http://ditu.google.cn/maps?f=d&source=s_d"
						+ "&saddr=" + user_location.latitude + ","
						+ user_location.longitude + "&daddr="
						+ marker.getPosition().latitude + ","
						+ marker.getPosition().longitude + "&hl=zh&t=m&dirflg="
						+ mode_navigation));
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						& Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
				intent.setClassName("com.google.android.apps.maps",
						"com.google.android.maps.MapsActivity");
				getContext().startActivity(intent);

			}
		};
		this.navigationButton_task
				.setOnTouchListener(navigationButtonListener_task);

	}

	public void createInfoWindow(Marker marker_select, Product task_select,
			String mode_map, LatLng user_location) {

		this.product = task_select;
		this.mode_map = mode_map;
		this.user_location = user_location;
		TextView textView_title = (TextView) viewInfoWindow
				.findViewById(R.id.textView_title);
		TextView textView_state = (TextView) viewInfoWindow
				.findViewById(R.id.textView_state);
		TextView textView_size = (TextView) viewInfoWindow
				.findViewById(R.id.textView_size);
		
		TextView textView_price = (TextView) viewInfoWindow
				.findViewById(R.id.textView_price);

		
		textView_title.setText(getContext().getString(R.string.text_product_flat_address)+" "+task_select.getIze_direccion_calle1());
		
		String state = "";

		String price = "";

		/*if (product.getIze_Venta() != null) {

			if (ize_opciones.toObject(product.getIze_Venta().getValue()).equals(ize_opciones.SI)) {

				state = getContext().getString(R.string.text_product_state_buy);
				price = Constants.FORMAT_MONEY.format(product.getPrice().Value) + " "+getContext().getString(R.string.text_product_money_euro);
			}
		}

		if (product.getIze_Alquiler() != null) {

			if (ize_opciones.toObject(product.getIze_Alquiler().getValue()).equals(ize_opciones.SI)) {

				if (state.equals("")) {

					state = getContext().getString(R.string.text_product_state_rent);
					price = Constants.FORMAT_MONEY.format(product.getIze_PreciosAlquiler().Value) + " "+getContext().getString(R.string.text_product_money_euro_m2);
				} 
			}
		}*/
		
		if (sharedPreferences.contains(Constants.FILTER_RADIOBUTTON_RENT)) {
	    	
	    	if (product.getIze_Alquiler() != null) {
			
			if (ize_opciones.toObject(product.getIze_Alquiler().getValue()).equals(ize_opciones.SI)) {

				if (state.equals("")) {

					state = getContext().getString(R.string.text_product_state_rent);
					price = Constants.FORMAT_MONEY.format(product.getIze_PreciosAlquiler().Value) + getContext().getString(R.string.text_product_money_euro)+ " - "
							+ Constants.FORMAT_MONEY.format(product.getIze_PrecioAlquilermconst().Value) +" "+ getContext().getString(R.string.text_product_money_euro_m2);
				}				
			}
	    	}
			
		}else{
			if (product.getIze_Venta() != null) {
			if (ize_opciones.toObject(product.getIze_Venta().getValue()).equals(ize_opciones.SI)) {

				state = getContext().getString(R.string.text_product_state_buy);
				price = Constants.FORMAT_MONEY.format(product.getPrice().Value) + getContext().getString(R.string.text_product_money_euro)+ " - "
						+ Constants.FORMAT_MONEY.format(product.getIze_PrecioVentam2constr().Value) +" "+ getContext().getString(R.string.text_product_money_euro_m2);
			}
			}
			
		}
		
		textView_state.setText("("+state+" "+ize_tipologia.toObject(product.getIze_tipologia().getValue()).name()+")");
		textView_size.setText(getContext().getString(R.string.text_product_size)+" "+task_select.getIze_m2construidosdecimal().intValue()+" m�");
		textView_price.setText(getContext().getString(R.string.text_product_price)+" "+price);

		

		infoButton_task.setVisibility(Button.VISIBLE);
		infoButton_task.setOnTouchListener(infoButtonListener_task);
		infoButtonListener_task.setMarker(marker_select);
		navigationButtonListener_task.setMarker(marker_select);

	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {

	}

	public View getViewInfoWindow() {
		return viewInfoWindow;
	}

	public void setViewInfoWindow(View viewInfoWindow) {
		this.viewInfoWindow = viewInfoWindow;
	}

}
