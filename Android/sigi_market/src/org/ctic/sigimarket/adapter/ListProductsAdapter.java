package org.ctic.sigimarket.adapter;

import java.util.List;

import org.ctic.sigimarket.constants.Constants;
import org.ctic.sigimarket.R;

import com.microsoft.xrm.sdk.client.Product;
import com.microsoft.xrm.sdk.client.ize.ize_opciones;
import com.microsoft.xrm.sdk.client.ize.ize_tipologia;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * ListProductsAdapter
 * 
 * Adaptador personalizado de las lista de products (pisos).
 * 
 * @author Celia (Fundación CTIC)
 */

public class ListProductsAdapter extends ArrayAdapter<Product> {
	
	private SharedPreferences sharedPreferences;
	
	private final String usePluginsPrefs = "sigiUsePluginsPrefs";
	
	public ListProductsAdapter(Activity context, int id, List<Product> products) {
		super(context, id, products);	
		
		sharedPreferences = context.getSharedPreferences(usePluginsPrefs, context.MODE_PRIVATE);
	}

	//TODO PATRON VIEWHOLDER IMPLEMENTADO
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View item = convertView;
	    ViewHolder holder;
	    
	    if(item == null)
	    {
	    	LayoutInflater infalInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	item = infalInflater.inflate(R.layout.custom_item_list, null);
	 
	        holder = new ViewHolder();
	        holder.textView_item_title = (TextView)item.findViewById(R.id.textView_item_title);
	        holder.textView_item_sub_title = (TextView)item.findViewById(R.id.textView_item_sub_title);
	        holder.textView_item_top_right = (TextView)item.findViewById(R.id.textView_item_top_right);
	        holder.textView_item_bottom_right = (TextView)item.findViewById(R.id.textView_item_bottom_right);
	        holder.linearLayout_item_color = (LinearLayout) item.findViewById(R.id.linearLayout_item_color);
	        
	        item.setTag(holder);
	    }
	    else
	    {
	        holder = (ViewHolder)item.getTag();
	    }
	 
	    Product product = getItem(position);
	    
	    holder.textView_item_title.setText(getContext().getString(R.string.text_product_flat_address)+" "+ product.getIze_direccion_calle1());
	    
	    String state = "";
	    String price = "";
	    
	    if (sharedPreferences.contains(Constants.FILTER_RADIOBUTTON_RENT)) {
	    	
	    	if (product.getIze_Alquiler() != null) {
			
			if (ize_opciones.toObject(product.getIze_Alquiler().getValue()).equals(ize_opciones.SI)) {

				if (state.equals("")) {

					state = getContext().getString(R.string.text_product_state_rent);
					price = Constants.FORMAT_MONEY.format(product.getIze_PreciosAlquiler().Value) + getContext().getString(R.string.text_product_money_euro)+ " - "
							+ Constants.FORMAT_MONEY.format(product.getIze_PrecioAlquilermconst().Value) +" "+ getContext().getString(R.string.text_product_money_euro_m2);
				}				
			}
	    	}
			
		}else{
			if (product.getIze_Venta() != null) {
			if (ize_opciones.toObject(product.getIze_Venta().getValue()).equals(ize_opciones.SI)) {

				state = getContext().getString(R.string.text_product_state_buy);
				price = Constants.FORMAT_MONEY.format(product.getPrice().Value) + getContext().getString(R.string.text_product_money_euro)+ " - "
						+ Constants.FORMAT_MONEY.format(product.getIze_PrecioVentam2constr().Value) +" "+ getContext().getString(R.string.text_product_money_euro_m2);
			}
			}
			
		}

		/*if (product.getIze_Venta() != null) {

			if (ize_opciones.toObject(product.getIze_Venta().getValue()).equals(ize_opciones.SI)) {

				state = getContext().getString(R.string.text_product_state_buy);
				price = Constants.FORMAT_MONEY.format(product.getPrice().Value) + getContext().getString(R.string.text_product_money_euro);
			}
		}

		if (product.getIze_Alquiler() != null) {

			if (ize_opciones.toObject(product.getIze_Alquiler().getValue()).equals(ize_opciones.SI)) {

				if (state.equals("")) {

					state = getContext().getString(R.string.text_product_state_rent);
					price = Constants.FORMAT_MONEY.format(product.getIze_PreciosAlquiler().Value) + getContext().getString(R.string.text_product_money_euro);
				} 
			}
		}*/
	    
	    holder.textView_item_sub_title.setText(state + " "+ize_tipologia.toObject(product.getIze_tipologia().getValue()).name());
	    holder.textView_item_top_right.setText(price);
        holder.textView_item_bottom_right.setText(product.getIze_m2construidosdecimal().intValue() + getContext().getString(R.string.text_product_m2));
        holder.linearLayout_item_color.setBackgroundColor(Color.LTGRAY);
	 
	    return(item);
		
	}
	
	static class ViewHolder {	    
		LinearLayout linearLayout_item_color;
		TextView textView_item_title;
		TextView textView_item_sub_title;	    
	    TextView textView_item_top_right;
	    TextView textView_item_bottom_right;
	}

}
