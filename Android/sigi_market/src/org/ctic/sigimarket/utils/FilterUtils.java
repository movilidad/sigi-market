package org.ctic.sigimarket.utils;



import org.ctic.sigimarket.constants.Constants;

import com.microsoft.xrm.sdk.client.Product;
import com.microsoft.xrm.sdk.client.ize.ize_numeros;
import com.microsoft.xrm.sdk.client.ize.ize_opciones;
import com.microsoft.xrm.sdk.client.ize.ize_tipologia;

import android.content.SharedPreferences;
import android.util.Log;


public class FilterUtils {
	
	private static final String TAG = FilterUtils.class.getSimpleName();

	public static boolean filterAll(Product p, SharedPreferences sPf) {
		
		
		Log.e(TAG, "ESTAMOS EN FILTROS!!");
		
		//Log.e(TAG, "RESULTADO TOTAL "+(filterRooms(p,sPf)&&filterSizeM2(p,sPf)&&filterState(p,sPf)&&filterType(p,sPf)&&filterWC(p,sPf)));
		/*Log.e(TAG, "RESULTADO TOTAL "+(filterRooms(p,sPf)&&filterSizeM2(p,sPf)&&filterState(p,sPf)&&filterWC(p,sPf)));
		Log.e(TAG, "RESULTADO ROOMS "+filterRooms(p,sPf));
		Log.e(TAG, "RESULTADO SIZE "+filterSizeM2(p,sPf));
		Log.e(TAG, "RESULTADO STATE "+filterState(p,sPf));
		//Log.e(TAG, "RESULTADO TYPE "+filterType(p,sPf));
		Log.e(TAG, "RESULTADO WC "+filterWC(p,sPf));*/
		
		
		if((filterRooms(p,sPf)&&filterSizeM2(p,sPf)&&filterState(p,sPf)&&filterType(p,sPf)&&filterWC(p,sPf))){
		//if((filterRooms(p,sPf)&&filterSizeM2(p,sPf)&&filterState(p,sPf)&&filterWC(p,sPf))){
			return true;
		}else{
			return false;
		}
	}

	public static boolean filterRooms(Product product, SharedPreferences sharedPreferences) {
		
		Log.e(TAG, "ESTAMOS EN FILTRO ROOOMS ");
		
		if(product.getIze_Numhabitaciones()!=null){
			
			Log.e(TAG, "ESTAMOS EN FILTRO ROOOMS DENTRO");

		int num_habitaciones = ize_numeros.toObject(product.getIze_Numhabitaciones().getValue());

		boolean is_1_rooms = sharedPreferences.contains(Constants.FILTER_CHECKBOX_ROOMS_1);
		boolean is_2_rooms = sharedPreferences.contains(Constants.FILTER_CHECKBOX_ROOMS_2);
		boolean is_3_rooms = sharedPreferences.contains(Constants.FILTER_CHECKBOX_ROOMS_3);

		if (is_1_rooms && num_habitaciones == 1) {
			return true;
		}
		if (is_2_rooms && num_habitaciones == 2) {
			return true;
		}
		if (is_3_rooms && num_habitaciones >= 3) {
			return true;
		}
		if (!is_1_rooms && !is_2_rooms && !is_3_rooms) {
			return true;
		}
		}

		return false;

	}

	public static boolean filterWC(Product product, SharedPreferences sharedPreferences) {
		
		Log.e(TAG, "ESTAMOS EN FILTRO WCS ");
		
		if(product.getIze_NumBanios()!=null){
			
			Log.e(TAG, "ESTAMOS EN FILTRO WCS DENTRO");

		int num_banios = ize_numeros.toObject(product.getIze_NumBanios().getValue());

		boolean is_1_wc = sharedPreferences.contains(Constants.FILTER_CHECKBOX_WC_1);
		boolean is_2_wc = sharedPreferences.contains(Constants.FILTER_CHECKBOX_WC_2);
		boolean is_3_wc = sharedPreferences.contains(Constants.FILTER_CHECKBOX_WC_3);

		if (is_1_wc && num_banios == 1) {
			return true;
		}
		if (is_2_wc && num_banios == 2) {
			return true;
		}
		if (is_3_wc && num_banios >= 3) {
			return true;
		}
		if (!is_1_wc && !is_2_wc && !is_3_wc) {
			return true;
		}
		}
		return false;

	}

	public static boolean filterState(Product product, SharedPreferences sharedPreferences) {
		
		/*Log.e(TAG, "ESTAMOS EN FILTRO VENTA "+product.getIze_Venta().getValue());
		Log.e(TAG, "ESTAMOS EN FILTRO ALQUILER "+product.getIze_Alquiler().getValue());*/

		ize_opciones buy = ize_opciones.toObject(product.getIze_Venta().getValue());
		ize_opciones rent = ize_opciones.toObject(product.getIze_Alquiler().getValue());

		boolean is_buy = sharedPreferences.contains(Constants.FILTER_RADIOBUTTON_BUY);
		boolean is_rent = sharedPreferences.contains(Constants.FILTER_RADIOBUTTON_RENT);

		// DOS OPCIONES SI
		
		Log.e(TAG, "VENTA "+is_buy+"/"+buy.name());
		Log.e(TAG, "VENTA "+is_rent+"/"+rent.name());
		
		if ((is_buy && buy.equals(ize_opciones.SI)) && filterPriceBuy(product,sharedPreferences)) {
			return true;
		}
		// DOS OPCIONES SI
		if ((is_rent && rent.equals(ize_opciones.SI)) && filterPriceRent(product,sharedPreferences)) {
			return true;
		}

		if (!is_buy && !is_rent) {

			return true;
		}

		return false;
		
		//return true;

	}

	public static boolean filterType(Product product, SharedPreferences sharedPreferences) {

		boolean is_type = sharedPreferences.contains(Constants.FILTER_SPINNER_TYPE);
		int type_value = sharedPreferences.getInt(Constants.FILTER_SPINNER_TYPE, 0);
		if (is_type) {
			if (Constants.FILTER_TYPE[type_value].equals(Constants.FILTER_TYPE[0])) {
				return true;
			} else {
				if ((Constants.FILTER_TYPE[type_value].equals(ize_tipologia.toObject(product.getIze_tipologia().getValue()).name()))) {
					return true;
				} else
					return false;

			}

		} else {

			return true;
		}

	}

	public static boolean filterSizeM2(Product product, SharedPreferences sharedPreferences) {
		
		Log.e(TAG, "ESTAMOS EN FILTRO M2 "+product.getIze_m2construidosdecimal().intValue());
		

		int size_m2 = product.getIze_m2construidosdecimal().intValue();

		String size_init = Constants.FILTER_SIZE_M2[sharedPreferences.getInt(Constants.FILTER_SPINNER_SIZE_INIT, 0)];
		String size_finish = Constants.FILTER_SIZE_M2[sharedPreferences.getInt(Constants.FILTER_SPINNER_SIZE_FINISH,
				0)];

		if (sharedPreferences.contains(Constants.FILTER_SPINNER_SIZE_INIT)
				&& sharedPreferences.contains(Constants.FILTER_SPINNER_SIZE_FINISH)) {

			if (size_init.equals(Constants.FILTER_SIZE_M2[0])) {
				// SI AMBAS SON INDIFERENTES
				if (size_finish.equals(Constants.FILTER_SIZE_M2[0])) {

					return true;
				} else {
					// MIN INDIFERENTE PERO CON MAXIMO MARCADO
					if (size_m2 <= Integer.valueOf(size_finish.split(" ")[0])) {

						return true;
					} else {
						// NO CUMPLE LA CONDICION
						return false;
					}
				}
			} // SI EL MIN ESTA MARCADO
			else {
				// Y EL MAXIMO ES INDIFERENTE
				if (size_m2 >= Integer.valueOf(size_init.split(" ")[0])
						&& size_finish.equals(Constants.FILTER_SIZE_M2[0])) {

					return true;
				}
				// TANTO MIN COMO MAXIMO ESTAN MARCADOS
				else {
					if (size_m2 >= Integer.valueOf(size_init.split(" ")[0])
							&& size_m2 <= Integer.valueOf(size_finish.split(" ")[0])) {

						return true;
					} else {
						return false;
					}
				}
			}

		} else {
			return true;
		}

	}

	public static boolean filterPriceBuy(Product product, SharedPreferences sharedPreferences) {

		float price_buy = Float.valueOf(Constants.FORMAT_MONEY.format(product.getPrice().Value));

		String price_buy_init = Constants.FILTER_PRICE_BUY[sharedPreferences
				.getInt(Constants.FILTER_SPINNER_PRICE_BUY_INIT, 0)];
		String price_buy_finish = Constants.FILTER_PRICE_BUY[sharedPreferences
				.getInt(Constants.FILTER_SPINNER_PRICE_BUY_FINISH, 0)];

		if (sharedPreferences.contains(Constants.FILTER_SPINNER_PRICE_BUY_INIT)
				&& sharedPreferences.contains(Constants.FILTER_SPINNER_PRICE_BUY_FINISH)) {

			if (price_buy_init.equals(Constants.FILTER_PRICE_BUY[0])) {
				// SI AMBAS SON INDIFERENTES
				if (price_buy_finish.equals(Constants.FILTER_PRICE_BUY[0])) {

					return true;
				} else {
					// MIN INDIFERENTE PERO CON MAXIMO MARCADO
					if (price_buy <= Float.valueOf(price_buy_finish)) {

						return true;
					} else {
						// NO CUMPLE LA CONDICION
						return false;
					}
				}
			} // SI EL MIN ESTA MARCADO
			else {
				// Y EL MAXIMO ES INDIFERENTE
				if (price_buy >= Float.valueOf(price_buy_init)
						&& price_buy_finish.equals(Constants.FILTER_PRICE_BUY[0])) {

					return true;
				}
				// TANTO MIN COMO MAXIMO ESTAN MARCADOS
				else {
					if (price_buy >= Float.valueOf(price_buy_init.split(" ")[0])
							&& price_buy <= Float.valueOf(price_buy_finish.split(" ")[0])) {

						return true;
					} else {
						return false;
					}
				}
			}

		} else {
			return true;
		}

	}

	public static boolean filterPriceRent(Product product, SharedPreferences sharedPreferences) {

		float price_rent = Float.valueOf(Constants.FORMAT_MONEY.format(product.getIze_PreciosAlquiler().Value));

		String price_rent_init = Constants.FILTER_PRICE_RENT[sharedPreferences
				.getInt(Constants.FILTER_SPINNER_PRICE_RENT_INIT, 0)];
		String price_rent_finish = Constants.FILTER_PRICE_RENT[sharedPreferences
				.getInt(Constants.FILTER_SPINNER_PRICE_RENT_FINISH, 0)];

		if (sharedPreferences.contains(Constants.FILTER_SPINNER_PRICE_RENT_INIT)
				&& sharedPreferences.contains(Constants.FILTER_SPINNER_PRICE_RENT_FINISH)) {

			if (price_rent_init.equals(Constants.FILTER_PRICE_RENT[0])) {
				// SI AMBAS SON INDIFERENTES
				if (price_rent_finish.equals(Constants.FILTER_PRICE_RENT[0])) {

					return true;
				} else {
					// MIN INDIFERENTE PERO CON MAXIMO MARCADO
					if (price_rent <= Float.valueOf(price_rent_finish)) {

						return true;
					} else {
						// NO CUMPLE LA CONDICION
						return false;
					}
				}
			} // SI EL MIN ESTA MARCADO
			else {
				// Y EL MAXIMO ES INDIFERENTE
				if (price_rent >= Float.valueOf(price_rent_init)
						&& price_rent_finish.equals(Constants.FILTER_PRICE_RENT[0])) {

					return true;
				}
				// TANTO MIN COMO MAXIMO ESTAN MARCADOS
				else {
					if (price_rent >= Float.valueOf(price_rent_init.split(" ")[0])
							&& price_rent <= Float.valueOf(price_rent_finish.split(" ")[0])) {

						return true;
					} else {
						return false;
					}
				}
			}

		} else {
			return true;
		}

	}

}
