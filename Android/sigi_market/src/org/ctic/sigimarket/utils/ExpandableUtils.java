package org.ctic.sigimarket.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.widget.ExpandableListView;

public class ExpandableUtils {

	private static final String TAG = ExpandableUtils.class.getSimpleName();

	public static void setIndicatorBounds(int width, Resources resources, ExpandableListView expandableList) {

		Configuration config = resources.getConfiguration();
		int size = config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			if (size == Configuration.SCREENLAYOUT_SIZE_LARGE || size == Configuration.SCREENLAYOUT_SIZE_XLARGE) {

				expandableList.setIndicatorBounds(width - getDipsFromPixels(390, resources),
						width - getDipsFromPixels(260, resources));
			} else

				expandableList.setIndicatorBounds(width - getDipsFromPixels(60, resources),
						width - getDipsFromPixels(10, resources));
		} else {
			// SI LA VERSION ES SUPERIOR A 18 (SDK>18) SE UTILIZA
			// setIndicatorBoundsRelative
			if (size == Configuration.SCREENLAYOUT_SIZE_LARGE || size == Configuration.SCREENLAYOUT_SIZE_XLARGE) {

				expandableList.setIndicatorBoundsRelative(width - getDipsFromPixels(250, resources),
						width - getDipsFromPixels(380, resources));
			} else

				expandableList.setIndicatorBoundsRelative(width - getDipsFromPixels(70, resources),
						width - getDipsFromPixels(25, resources));
		}
	}

	public static int getDipsFromPixels(float pixels, Resources resources) {
		// Get the screen's density scale
		final float scale = resources.getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}

	public static int getPixelsFromDips(Context context, float dips) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dips * scale + 0.5f);
	}

}
