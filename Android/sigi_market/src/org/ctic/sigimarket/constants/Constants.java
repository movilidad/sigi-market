package org.ctic.sigimarket.constants;

import java.text.DecimalFormat;

import com.microsoft.xrm.sdk.client.ize.ize_tipologia;

/**
 * Constants
 * 
 * Variables constantes.
 * 
 * @author Celia (Fundaci�n CTIC).
 */

public class Constants {
	
	static public final int ACTIVITY_SUCCESS_RESULT =1;
	
	// IDENTIFICADOR DE LAS DIFERENTES OPCIONES DE LA VISTA
	//VISTA RA
	static public final int OPT_VIEW_1 =1;
	//VISTA MAPA
	static public final int OPT_VIEW_2 =2;
	//VISTA LISTA
	static public final int OPT_VIEW_3 =3;
	
	//NOMBRE DE LAS PREFERENCES GUARDADAS EN SharedPreferences EN EL FILTRO
	static public final String FILTER_SPINNER_TYPE ="spinner_type";
	static public final String FILTER_SPINNER_PRICE_BUY_INIT ="spinner_price_buy_init";
	static public final String FILTER_SPINNER_PRICE_BUY_FINISH ="spinner_price_buy_finish";
	static public final String FILTER_SPINNER_PRICE_RENT_INIT ="spinner_price_rent_init";
	static public final String FILTER_SPINNER_PRICE_RENT_FINISH ="spinner_price_rent_finish";
	static public final String FILTER_SPINNER_SIZE_INIT ="spinner_size_init";
	static public final String FILTER_SPINNER_SIZE_FINISH ="spinner_size_finish";
	static public final String FILTER_RADIOBUTTON_BUY ="radioButton_buy";
	static public final String FILTER_RADIOBUTTON_RENT ="radioButton_rent";
	static public final String FILTER_CHECKBOX_ROOMS_1 ="checkBox_rooms_1";
	static public final String FILTER_CHECKBOX_ROOMS_2 ="checkBox_rooms_2";
	static public final String FILTER_CHECKBOX_ROOMS_3 ="checkBox_rooms_3";
	static public final String FILTER_CHECKBOX_WC_1 ="checkBox_wc_1";
	static public final String FILTER_CHECKBOX_WC_2 ="checkBox_wc_2";
	static public final String FILTER_CHECKBOX_WC_3 ="checkBox_wc_3";
	
	//VALORES PARA LOS SPINNERS DEL FILTRO
	static public final String[] FILTER_TYPE = new String[] { "Indiferente",ize_tipologia.Vivienda.name(), ize_tipologia.Obranueva.name(), ize_tipologia.Local.name(), ize_tipologia.Garaje.name(), ize_tipologia.Nave.name(), ize_tipologia.Oficina.name(), ize_tipologia.Terreno.name() };
	static public final String[] FILTER_PRICE_RENT = new String[] { "Indiferente", "100", "150", "200", "250", "300", "350", "400", "500", "600", "700", "800", "900", "1000", "1200", "1400", "1600" };
	static public final String[] FILTER_PRICE_BUY = new String[] { "Indiferente", "60.000", "80.000", "100.000", "120.000", "140.000",
			"160.000", "180.000", "200.000", "250.000", "300.000", "350.000", "400.000", "500.000" };
	static public final String[] FILTER_SIZE_M2 = new String[] { "Indiferente", "40 m�", "60 m�", "80 m�", "100 m�", "120 m�", "140 m�",
			"160 m�","180 m�","200 m�","250 m�","300 m�" };

	//FORMATO DINERO
	static public final DecimalFormat FORMAT_MONEY = new DecimalFormat("#,###,###");

	//FORMATO FECHA
	static public final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	static public final String TIME_ZONE = "GMT";

	//***************************** DATOS CRM *****************************//
	
	//LOGIN
	static public final String CRM_URL = "https://sigi1.crm4.dynamics.com/";
	static public final String CRM_USERNAME = "sigi1@sigi1.onmicrosoft.com";
	static public final String CRM_PASSWORD = "Abral12345%";
	
	//TIPOS DE PETICIONES
	/*static public final int CRM_TYPE_REQUEST_WHO_I_AM=0;
	static public final int CRM_TYPE_REQUEST_SYSTEM_USER=1;
	static public final int CRM_TYPE_REQUEST_RETRIEVE_MULTIPLE_PRODUCT=2;*/

	static public final int AUTHENTICATION_REQUEST_VALIDITY_MINUTES = 5;
	
	static public final String CRM_ORGANIZATION_SERVICE_URL_SUFFIX = "/XRMServices/2011/Organization.svc";
	//INDICAMOS QUE ENVIAMOS CONTENIDO DE TIPO SOAP/XML
	static public final String CRM_CONTENT_TYPE = "application/soap+xml; charset=UTF-8";
	
	//URL LOGIN
	static public final String CRM_LOGIN_URL = "https://login.microsoftonline.com/RST2.srf";
	
	//CONTENIDO LLAMADA AUTENTIFIACION CRM
	static public final String CRM_AUTH_SOAP = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\" "
			+ "xmlns:a=\"http://www.w3.org/2005/08/addressing\" "
			+ "xmlns:u=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
			+ " <s:Header>"
			+ "  <a:Action s:mustUnderstand=\"1\">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action>"
			+ "  <a:MessageID>urn:uuid:%s</a:MessageID>"
			+ "  <a:ReplyTo><a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address></a:ReplyTo>"
			+ "  <VsDebuggerCausalityData xmlns=\"http://schemas.microsoft.com/vstudio/diagnostics/servicemodelsink\">uIDPo2V68j15KH9PqGf9DWiAfGQAAAAA/Dr1z6qvqUGzr5Yv4aMcdIr9AKDFU7VHn7lpNp0zeXEACQAA</VsDebuggerCausalityData>"
			+ "  <a:To s:mustUnderstand=\"1\">%s</a:To>"
			+ "  <o:Security s:mustUnderstand=\"1\" xmlns:o=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
			+ "   <u:Timestamp u:Id=\"_0\">" + "    <u:Created>%s</u:Created>" + "    <u:Expires>%s</u:Expires>"
			+ "   </u:Timestamp>" + "   <o:UsernameToken u:Id=\"%s\">" + "    <o:Username>%s</o:Username>"
			+ "    <o:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">%s</o:Password>"
			+ "   </o:UsernameToken>" + "  </o:Security>" + " </s:Header>" + " <s:Body>"
			+ "  <t:RequestSecurityToken xmlns:t=\"http://schemas.xmlsoap.org/ws/2005/02/trust\">"
			+ "   <wsp:AppliesTo xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\">"
			+ "    <a:EndpointReference>" + "     <a:Address>urn:%s</a:Address>" + "    </a:EndpointReference>"
			+ "   </wsp:AppliesTo>"
			+ "   <t:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</t:RequestType>"
			+ "  </t:RequestSecurityToken>" + " </s:Body>" + "</s:Envelope>";

	static public final String CRM_SECURITY_HEADER_SOAP = "<s:Header>"
			+ "<a:Action s:mustUnderstand=\"1\">http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute</a:Action>"
			+ "<Security xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
			+ "<EncryptedData xmlns=\"http://www.w3.org/2001/04/xmlenc#\" Id=\"Assertion0\" Type=\"http://www.w3.org/2001/04/xmlenc#Element\">"
			+ "	<EncryptionMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#tripledes-cbc\"/>"
			+ "	<ds:KeyInfo xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\">" + "		<EncryptedKey>"
			+ "			<EncryptionMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#rsa-oaep-mgf1p\"/>"
			+ "			<ds:KeyInfo Id=\"keyinfo\">"
			+ "				<wsse:SecurityTokenReference xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
			+ "					<wsse:KeyIdentifier EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\" "
			+ "						ValueType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509SubjectKeyIdentifier\">%s</wsse:KeyIdentifier>"
			+ "				</wsse:SecurityTokenReference>" + "			</ds:KeyInfo>" + "			<CipherData>"
			+ "				<CipherValue>%s</CipherValue>" + "			</CipherData>" + "		</EncryptedKey>"
			+ "	</ds:KeyInfo>" + "	<CipherData>" + "		<CipherValue>%s</CipherValue>" + "	</CipherData>"
			+ "</EncryptedData>" + "</Security>" + "<a:MessageID>urn:uuid:%s</a:MessageID>" + "<a:ReplyTo>"
			+ "<a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>" + "</a:ReplyTo>"
			+ "<a:To s:mustUnderstand=\"1\">%sXRMServices/2011/Organization.svc</a:To>" + "</s:Header>";

	
	
	//CONTENIDO LLAMADA PARA OBTENER LOS PRODUCTS UNA VEZ AUTENTIFICADOS EN EL CRM
	static public final String CRM_REQUEST_RETRIEVE_MULTIPLE=
	"<s:Body>"+
	"<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">"+
	"<request i:type=\"a:RetrieveMultipleRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">"+
	"<a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">"+
	"<a:KeyValuePairOfstringanyType>"+	
	//HASTA AQUI ES GENERICO
	"<b:key>Query</b:key>"+
	"<b:value i:type=\"a:QueryExpression\">"+
	"<a:ColumnSet>"+
	"<a:AllColumns>false</a:AllColumns>"+
	"<a:Columns xmlns:c=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">"+
	//DATOS QUE QUEREMOS OBTENER �DEBERIA SER DINAMICO?
	"<c:string>productid</c:string>"+
	"<c:string>ize_longitud</c:string>"+		
	"<c:string>ize_latitud</c:string>"+
	"<c:string>ize_direccion_calle1</c:string>"+			
	"<c:string>ize_direccion_numero</c:string>"+
	"<c:string>ize_planta</c:string>"+
	"<c:string>ize_direccion_letra</c:string>"+
	"<c:string>ize_numhabitaciones</c:string>"+
	"<c:string>ize_numbanios</c:string>"+	
	//"<c:string>ize_m2edificables</c:string>"+
	"<c:string>ize_m2construidosdecimal</c:string>"+
	"<c:string>ize_preciosalquiler</c:string>"+	
	"<c:string>ize_precioventam2constr</c:string>"+
	"<c:string>price</c:string>"+
	//"<c:string>price_base</c:string>"+
	//"<c:string>ize_codcategora</c:string>"+
	"<c:string>ize_venta</c:string>"+
	"<c:string>ize_alquiler</c:string>"+
	"<c:string>ize_precioalquilermconst</c:string>"+
	//NUEVO
	"<c:string>ize_subtipologia</c:string>"+			
	"<c:string>ize_tipologia</c:string>"+
	
	
	"</a:Columns>"+
	"</a:ColumnSet>"+	
	"<a:Distinct>false</a:Distinct>"+
	
	
	"<a:EntityName>product</a:EntityName>"+
	"<a:LinkEntities />"+
	"<a:Orders />"+
	"</b:value>"+
	
	"</a:KeyValuePairOfstringanyType>"+
	"</a:Parameters>"+
	"<a:RequestId i:nil=\"true\" />"+
	"<a:RequestName>RetrieveMultiple</a:RequestName>"+
	"</request>"+
	"</Execute>"+
	"</s:Body>";
}
