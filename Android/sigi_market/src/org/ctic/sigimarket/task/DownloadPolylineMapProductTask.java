package org.ctic.sigimarket.task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ctic.sigimarket.json.DirectionsJSONParser;
import org.ctic.sigimarket.listener.OnGetPolylineMapProductListener;
import org.json.JSONObject;

import com.androidmapsextensions.PolylineOptions;
import com.google.android.gms.maps.model.LatLng;

import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

public class DownloadPolylineMapProductTask extends AsyncTask<String, Void, String> {

	public OnGetPolylineMapProductListener listener = null;
	
	public static final String TAG = DownloadPolylineMapProductTask.class.getSimpleName();

	// Downloading data in non-ui thread
	@Override
	protected String doInBackground(String... url) {

		// For storing data from web service
		String data = "";

		try {
			// Fetching the data from web service
			data = downloadUrl(url[0]);
		} catch (Exception e) {
			Log.e(TAG,"Exception "+ e.toString());
		}
		return data;
	}

	// Executes in UI thread, after the execution of
	// doInBackground()
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		JSONObject jObject;
		List<List<HashMap<String, String>>> routes = null;

		try {
			jObject = new JSONObject(result);
			// DirectionsJSONParser parser = new DirectionsJSONParser();

			// Starts parsing data
			routes = DirectionsJSONParser.parse(jObject);
		} catch (Exception e) {
			e.printStackTrace();
		}

		ArrayList<LatLng> points = null;
		PolylineOptions lineOptions = null;


		// Traversing through all the routes
		if (routes != null) {
			for (int i = 0; i < routes.size(); i++) {
				points = new ArrayList<LatLng>();
				lineOptions = new PolylineOptions();

				// Fetching i-th route
				List<HashMap<String, String>> path = routes.get(i);

				// Fetching all the points in i-th route
				for (int j = 0; j < path.size(); j++) {
					HashMap<String, String> point = path.get(j);

					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);

					points.add(position);
				}

				// Adding all the points in the route to LineOptions
				lineOptions.addAll(points);
				lineOptions.width(6);
				lineOptions.color(Color.RED);

			}
		}

		// Drawing polyline in the Google Map for the i-th route
		if (lineOptions != null) {
			listener.onGetPolylineMapProductComplete(lineOptions);
		}

	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			Log.e(TAG,"Exception while downloading url"+ e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

}
