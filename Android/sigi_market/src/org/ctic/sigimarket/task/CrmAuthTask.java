package org.ctic.sigimarket.task;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.ctic.sigimarket.constants.Constants;
import org.ctic.sigimarket.constants.dao.CrmAuthenticationHeader;
import org.ctic.sigimarket.listener.OnPostCrmAuthListener;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ae.javax.xml.bind.DatatypeConverter;
import android.os.AsyncTask;
import android.util.Log;

/**
 * CrmAuthTask
 * 
 * Permite ejecutar la operacion para obtener la autentificación, llamando
 * a los servicios de CRM en segundo plano y mostrar los resultados o una excepción en caso de
 * que el proceso no haya ido bien.
 * 
 * @author Celia (Fundación CTIC).
 */

public class CrmAuthTask extends AsyncTask<Void, Integer, CrmAuthenticationHeader> {

	public static final String TAG = CrmAuthTask.class.getSimpleName();
	
	private String url;
	private String username;
	private String password;

	public OnPostCrmAuthListener listener;

	public CrmAuthTask(OnPostCrmAuthListener listener, String username, String password, String url) {
		this.listener = listener;
		this.url = url;
		this.username = username;
		this.password = password;

	}

	@Override
	protected CrmAuthenticationHeader doInBackground(Void... params) {
		try {
			if (!url.endsWith("/"))
				url += "/";

			String urnAddress = GetUrnOnline(url);			

			// Prepare extra input parameter for CRM Authentication Request
			// > Random Message Id
			String paramMessageId = UUID.randomUUID().toString();
			// > Request Timestamp and +5 minutes validity
			TimeZone gmtTZ = TimeZone.getTimeZone(Constants.TIME_ZONE);
			SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
			formatter.setTimeZone(gmtTZ);
			Calendar calendar = Calendar.getInstance(gmtTZ);
			Date timestampRequest = calendar.getTime();
			calendar.add(Calendar.MINUTE, Constants.AUTHENTICATION_REQUEST_VALIDITY_MINUTES);
			Date timestampExpiryRequest = calendar.getTime();
			String paramTimestampRequest = formatter.format(timestampRequest);
			String paramTimestampExpiryRequest = formatter.format(timestampExpiryRequest);
			// > Random Token Id
			String paramTokenId = "uuid-" + UUID.randomUUID().toString() + "-1";
			
			// Prepare online CRM authentication SOAP request
			String onlineCRMAuthSOAPEnvelope = String.format(Constants.CRM_AUTH_SOAP, paramMessageId,
					Constants.CRM_LOGIN_URL, paramTimestampRequest, paramTimestampExpiryRequest, paramTokenId,
					username, password, urnAddress);			

			URL LoginURL = new URL(Constants.CRM_LOGIN_URL);
			HttpURLConnection rc = (HttpURLConnection) LoginURL.openConnection();

			rc.setRequestMethod("POST");
			rc.setDoOutput(true);
			rc.setDoInput(true);
			rc.setRequestProperty("Content-Type", Constants.CRM_CONTENT_TYPE);
			String reqStr = onlineCRMAuthSOAPEnvelope.toString();
			int len = reqStr.length();
			rc.setRequestProperty("Content-Length", Integer.toString(len));
			rc.connect();
			OutputStreamWriter out = new OutputStreamWriter(rc.getOutputStream());
			out.write(reqStr, 0, len);
			out.flush();

			InputStreamReader read = new InputStreamReader(rc.getInputStream());
			StringBuilder sb = new StringBuilder();
			int ch = read.read();
			while (ch != -1) {
				sb.append((char) ch);
				ch = read.read();
			}
			String response = sb.toString();
			read.close();
			rc.disconnect();

			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			Document x = builder.parse(new ByteArrayInputStream(response.getBytes()));

			NodeList cipherElements = x.getElementsByTagName("CipherValue");
			final String token1 = cipherElements.item(0).getTextContent();
			String token2 = cipherElements.item(1).getTextContent();

			NodeList keyIdentiferElements = x.getElementsByTagName("wsse:KeyIdentifier");
			String keyIdentifer = keyIdentiferElements.item(0).getTextContent();

			NodeList tokenExpiresElements = x.getElementsByTagName("wsu:Expires");
			String tokenExpires = tokenExpiresElements.item(0).getTextContent();

			Calendar c = DatatypeConverter.parseDateTime(tokenExpires);
			CrmAuthenticationHeader authHeader = new CrmAuthenticationHeader();
			authHeader.expires = c.getTime();
			
			authHeader.header = String.format(Constants.CRM_SECURITY_HEADER_SOAP, keyIdentifer, token1, token2,
					java.util.UUID.randomUUID(),url);			
			

			return authHeader;
		} catch (MalformedURLException e) {
			Log.e(TAG, "MalformedURLException "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, "IOException "+e.getMessage());
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			Log.e(TAG, "ParserConfigurationException "+e.getMessage());
			e.printStackTrace();
		} catch (SAXException e) {
			Log.e(TAG, "SAXException "+e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Gets the correct URN Address based on the Online region.
	 * 
	 * @return String URN Address.
	 * @param url
	 *            The Url of the CRM Online organization
	 *            (https://org.crm.dynamics.com).
	 */
	public String GetUrnOnline(String url) {
		if (url.toUpperCase().contains("CRM2.DYNAMICS.COM"))
			return "crmsam:dynamics.com";
		if (url.toUpperCase().contains("CRM4.DYNAMICS.COM"))
			return "crmemea:dynamics.com";
		if (url.toUpperCase().contains("CRM5.DYNAMICS.COM"))
			return "crmapac:dynamics.com";

		return "crmna:dynamics.com";
	}

		

	protected void onPostExecute(CrmAuthenticationHeader result) {

		if (result != null) {
			listener.onPostCrmAuthTaskComplete(result);
		} else {
			listener.onPostCrmAuthTaskError(0, TAG+" ERROR Autentificación CRM");
		}

	}

}
