package org.ctic.sigimarket.task;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.ctic.sigimarket.constants.Constants;
import org.ctic.sigimarket.constants.dao.CrmAuthenticationHeader;
import org.ctic.sigimarket.listener.OnPostCrmExecuteSoapListener;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import android.os.AsyncTask;
import android.util.Log;

/**
 * CrmExecuteSoapTask
 * 
 * Permite ejecutar la operacion para obtener los datos de los productos,
 * llamando a los servicios de CRM en segundo plano y mostrar los resultados o
 * una excepci�n en caso de que el proceso no haya ido bien.
 * 
 * @author Celia (Fundaci�n CTIC).
 */

public class CrmExecuteSoapTask extends AsyncTask<Void, Integer, Document> {

	public static final String TAG = CrmExecuteSoapTask.class.getSimpleName();

	private String url;
	private String requestBody;
	private CrmAuthenticationHeader authHeader;

	public OnPostCrmExecuteSoapListener listener;

	public CrmExecuteSoapTask(OnPostCrmExecuteSoapListener listener, CrmAuthenticationHeader authHeader, String requestBody,
			String url) {
		this.listener = listener;
		this.url = url;
		this.requestBody = requestBody;
		this.authHeader = authHeader;

	}

	@Override
	protected Document doInBackground(Void... params) {
		try {
			if (!url.endsWith("/"))
				url += "/";

			StringBuilder xml = new StringBuilder();
			xml.append(
					"<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:a=\"http://www.w3.org/2005/08/addressing\">");
			xml.append(authHeader.header);
			xml.append(requestBody);
			xml.append("</s:Envelope>");

			URL SoapURL = new URL(url + Constants.CRM_ORGANIZATION_SERVICE_URL_SUFFIX);
			HttpURLConnection rc = (HttpURLConnection) SoapURL.openConnection();

			rc.setRequestMethod("POST");
			rc.setDoOutput(true);
			rc.setDoInput(true);
			rc.setRequestProperty("Content-Type", Constants.CRM_CONTENT_TYPE);
			String reqStr = xml.toString();
			int len = reqStr.length();
			rc.setRequestProperty("Content-Length", Integer.toString(len));
			rc.connect();
			OutputStreamWriter out = new OutputStreamWriter(rc.getOutputStream());
			out.write(reqStr, 0, len);
			out.flush();

			InputStreamReader read = new InputStreamReader(rc.getInputStream());
			StringBuilder sb = new StringBuilder();
			int ch = read.read();
			while (ch != -1) {
				sb.append((char) ch);
				ch = read.read();
			}
			String response = sb.toString();
			read.close();
			rc.disconnect();
			
			Log.e(TAG, "RESPUESTA CRM "+response);

			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			Document doc = builder.parse(new ByteArrayInputStream(response.getBytes()));

			return doc;
		} catch (MalformedURLException e) {
			Log.e(TAG, "MalformedURLException " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, "IOException " + e.getMessage());
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			Log.e(TAG, "ParserConfigurationException " + e.getMessage());
			e.printStackTrace();
		} catch (SAXException e) {
			Log.e(TAG, "SAXException " + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	protected void onPostExecute(Document result) {

		if (result != null) {

			listener.onPostCrmExecuteSoapRetrieveMultipleProductTaskComplete(result);

		} else {
			listener.onPostCrmExecuteSoapTaskError(0, TAG + " Error al obtener los datos de los productos CRM");
		}

	}

}
