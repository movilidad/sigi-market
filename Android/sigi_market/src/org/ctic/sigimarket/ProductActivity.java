package org.ctic.sigimarket;

import org.ctic.sigimarket.application.SigiApplication;
import org.ctic.sigimarket.constants.Constants;
import org.ctic.sigimarket.utils.ExpandableUtils;
import org.ctic.sigimarket.views.MapWrapperLayout;
import org.ctic.sigimarket.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.SupportMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.microsoft.xrm.sdk.client.Product;
import com.microsoft.xrm.sdk.client.ize.ize_numeros;
import com.microsoft.xrm.sdk.client.ize.ize_opciones;
import com.microsoft.xrm.sdk.client.ize.ize_tipologia;

/**
 * ProductActivity Permite visualizar los datos m�s relevantes de un producto
 * (piso).
 * 
 * @author Celia (Fundaci�n CTIC)
 *
 */

public class ProductActivity extends ActionBarActivity {

	public static final String TAG = ProductActivity.class.getSimpleName();
	private SigiApplication sigiApp;

	private Product product;
	
	private SharedPreferences sharedPreferences;
	//private Editor editor;
	private final String usePluginsPrefs = "sigiUsePluginsPrefs";

	// Objeto mapa.
	private View view;
	private GoogleMap map = null;
	private SupportMapFragment mapFragment;
	private MapWrapperLayout mapWrapperLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		sharedPreferences = getSharedPreferences(usePluginsPrefs, MODE_PRIVATE);
		//editor = sharedPreferences.edit();

		sigiApp = (SigiApplication) getApplicationContext();
		product = sigiApp.getProduct();

		RelativeLayout relativeLayout_title = (RelativeLayout) findViewById(R.id.relativeLayout_title);

		TextView textView_title_product = (TextView) findViewById(R.id.textView_title_product);
		textView_title_product.setText(getString(R.string.text_product_flat_address)+" "+ product.getIze_direccion_calle1());

		TextView textView_info_product = (TextView) findViewById(R.id.textView_info_product);
		textView_info_product.setText(product.getIze_m2construidosdecimal().intValue()+" "+getString(R.string.text_product_m2));

		TextView textView_state = (TextView) findViewById(R.id.textView_state);

		String state = "";
		String price = "";
		// String price_m2 ="";
		
		if (sharedPreferences.contains(Constants.FILTER_RADIOBUTTON_RENT)) {
			
			if (ize_opciones.toObject(product.getIze_Alquiler().getValue()).equals(ize_opciones.SI)) {

				if (state.equals("")) {

					state = getString(R.string.text_product_state_rent);
					price = Constants.FORMAT_MONEY.format(product.getIze_PreciosAlquiler().Value) + getString(R.string.text_product_money_euro)+ " - "
							+ Constants.FORMAT_MONEY.format(product.getIze_PrecioAlquilermconst().Value) +" "+ getString(R.string.text_product_money_euro_m2);
				}				
			}
			
		}else{
			
			if (ize_opciones.toObject(product.getIze_Venta().getValue()).equals(ize_opciones.SI)) {

				state = getString(R.string.text_product_state_buy);
				price = Constants.FORMAT_MONEY.format(product.getPrice().Value) + getString(R.string.text_product_money_euro)+ " - "
						+ Constants.FORMAT_MONEY.format(product.getIze_PrecioVentam2constr().Value) +" "+ getString(R.string.text_product_money_euro_m2);
			}
			
		}

		/*if (product.getIze_Venta() != null) {

			if (ize_opciones.toObject(product.getIze_Venta().getValue()).equals(ize_opciones.SI)) {

				state = getString(R.string.text_product_state_buy);
				price = Constants.FORMAT_MONEY.format(product.getPrice().Value) + getString(R.string.text_product_money_euro)+ " - "
						+ Constants.FORMAT_MONEY.format(product.getIze_PrecioVentam2constr().Value) +" "+ getString(R.string.text_product_money_euro_m2);
			}
		}

		if (product.getIze_Alquiler() != null) {

			if (ize_opciones.toObject(product.getIze_Alquiler().getValue()).equals(ize_opciones.SI)) {

				if (state.equals("")) {

					state = getString(R.string.text_product_state_rent);
					price = Constants.FORMAT_MONEY.format(product.getIze_PreciosAlquiler().Value) + getString(R.string.text_product_money_euro)+ " - "
							+ Constants.FORMAT_MONEY.format(product.getIze_PrecioAlquilermconst().Value) +" "+ getString(R.string.text_product_money_euro_m2);
				}
				
			}
		}*/

		textView_state.setText(state + " " + ize_tipologia.toObject(product.getIze_tipologia().getValue()).name());

		relativeLayout_title.setBackgroundColor(Color.GRAY);

		ImageView imageView_product = (ImageView) findViewById(R.id.imageView_product);
		imageView_product.setImageResource(R.drawable.photo_flat);

		TextView textView_price = (TextView) findViewById(R.id.textView_price);
		textView_price.setText(price);

		TextView textView_features = (TextView) findViewById(R.id.textView_features);
		textView_features.setText(product.getIze_m2construidosdecimal().intValue() +" "+ getString(R.string.text_product_m2)+"\n"
				+ ize_numeros.toObject(product.getIze_Numhabitaciones().getValue()) +" "+getString(R.string.text_product_rooms)+"\n"
				+ ize_numeros.toObject(product.getIze_NumBanios().getValue()) +" "+ getString(R.string.text_product_wcs));

		String address = "";

		address += getString(R.string.text_product_address)+" "+ product.getIze_direccion_calle1();

		TextView textView_address = (TextView) findViewById(R.id.textView_address);
		if (product.getIze_direccion_numero() != null)
			address += "\n"+getString(R.string.text_product_number)+" "+ product.getIze_direccion_numero();
		if (product.getIze_Planta() != null)
			address += " "+getString(R.string.text_product_floor)+" "+ ize_numeros.toObject(product.getIze_Planta().getValue());
		if (product.getIze_Direccion_letra() != null)
			address += " " + product.getIze_Direccion_letra();
		textView_address.setText(address);

		// Mapa con Coordenadas
		if (product.getIze_latitud() != null && product.getIze_Longitud() != null) {
			// Vista del mapa
			ViewGroup parent = (ViewGroup) findViewById(R.id.linearLayout_map_fragment);
			parent.setVisibility(View.VISIBLE);
			view = LayoutInflater.from(this).inflate(R.layout.activity_product_map_fragment, null);
			parent.addView(view);

			mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map_task);
			map = mapFragment.getExtendedMap();
			mapWrapperLayout = (MapWrapperLayout) findViewById(R.id.task_map_wrapper);

			// Tama�o del mapa
			int screenWidth = getResources().getDisplayMetrics().widthPixels;
			int screenHeight = getResources().getDisplayMetrics().heightPixels;
			android.view.ViewGroup.LayoutParams params = (android.view.ViewGroup.LayoutParams) mapWrapperLayout
					.getLayoutParams();

			// Ancho ocupando el m�ximo posible
			params.width = screenWidth;
			// Alto ocupando un tercio del total
			params.height = (int) (screenHeight / 3);

			mapWrapperLayout.setLayoutParams(params);

			mapWrapperLayout.init(map, ExpandableUtils.getPixelsFromDips(this, 39 + 20));

			// Permitir moverse en el mapa evitando el scroll en la tarea
			// http://www.londatiga.net/it/programming/android/how-to-make-android-map-scrollable-inside-a-scrollview-layout/
			mapFragment.setListener(new SupportMapFragment.OnTouchListener() {
				@Override
				public void onTouch() {
					((ViewGroup) view).requestDisallowInterceptTouchEvent(true);
				}
			});

			LatLng posicion = new LatLng(Double.parseDouble(product.getIze_latitud()),
					Double.parseDouble(product.getIze_Longitud()));

			map.moveCamera(CameraUpdateFactory.newLatLngZoom(posicion, 17));
			map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

			// Marcamos en el mapa la posicion de la tarea
			map.addMarker(new MarkerOptions().position(new LatLng(posicion.latitude, posicion.longitude))
					.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
		}

	}

	@Override
	protected void onStart() {

		super.onStart();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case android.R.id.home:

			backActivity();

			break;

		}

		return super.onOptionsItemSelected(item);
	}

	private void backActivity() {

		switch (sigiApp.getOpt_main()) {
		case Constants.OPT_VIEW_1:
			startActivity(new Intent(this, MixView.class));
			break;
		case Constants.OPT_VIEW_2:
			startActivity(new Intent(this, MapActivity.class));
			break;
		case Constants.OPT_VIEW_3:
			startActivity(new Intent(this, ListProductsActivity.class));
			break;

		}

	}

}
