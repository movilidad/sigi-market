package org.ctic.sigimarket;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.ctic.sigimarket.application.SigiApplication;
import org.ctic.sigimarket.constants.Constants;
import org.ctic.sigimarket.listener.OnGetPolylineMapProductListener;
import org.ctic.sigimarket.task.DownloadPolylineMapProductTask;
import org.ctic.sigimarket.utils.ExpandableUtils;
import org.ctic.sigimarket.views.CustomInfoWindowProduct;
import org.ctic.sigimarket.views.MapWrapperLayout;
import org.ctic.sigimarket.R;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.GoogleMap.InfoWindowAdapter;

import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.Polyline;
import com.androidmapsextensions.PolylineOptions;
import com.androidmapsextensions.SupportMapFragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.microsoft.xrm.sdk.client.Product;

/**
 * MapActivity Permite visualizar, en el mapa, la localización del usuario y los
 * products (pisos). Muestra información de los mismos y visualizar la ruta para
 * llegar hasta ellos.
 * 
 * @author Celia (Fundación CTIC)
 *
 */

public class MapActivity extends ActionBarActivity implements OnGetPolylineMapProductListener {

	public static final String TAG = MapActivity.class.getSimpleName();

	private SigiApplication sigiApp;

	// Mapa.
	private GoogleMap map = null;
	private SupportMapFragment mapFragment;
	private MapWrapperLayout mapWrapperLayout;
	private boolean is_load_map = false;

	// Localización
	private Timer timer_location;
	private LocationManager loc_manager;

	private boolean gps_enabled = false;
	private boolean network_enabled = false;

	private Location network_location = null;
	private Location gps_location = null;

	// Posicion del usuario
	private LatLng user_location;

	// Ruta
	private Polyline path_line;

	private List<Product> list_products;

	// Ventana con información sobre el producto
	private CustomInfoWindowProduct info_window_product;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		sigiApp = (SigiApplication) getApplicationContext();
		sigiApp.setOpt_main(Constants.OPT_VIEW_2);

		// Cargamos productos guardados en el contexto
		list_products = sigiApp.getProducts();

		// Se obtiene la posición del usuario para dibujarla en el mapa.
		getLocation();

		// Se inicializa el mapa.
		mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map);
		mapWrapperLayout = (MapWrapperLayout) findViewById(R.id.drawerLayout_main);

		map = mapFragment.getExtendedMap();

		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		// Activamos la capa o layer MyLocation
		map.setMyLocationEnabled(true);

		mapWrapperLayout.init(map, ExpandableUtils.getPixelsFromDips(this, 39 + 20));

		this.info_window_product = new CustomInfoWindowProduct(this);

		if (list_products != null) {

			if (list_products.size() > 0) {

				for (int i = 0; i < list_products.size(); i++) {

					// Por cada product(piso) añadimos una nueva marca en el
					// mapa
					Product product = list_products.get(i);

					Marker marker = map.addMarker(new MarkerOptions()
							.position(new LatLng(Double.valueOf(product.getIze_latitud()),
									Double.valueOf(product.getIze_Longitud())))
							.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

					marker.setTitle(marker.getId() + "_Edificio_" + i);

				}
			} else {
				Toast.makeText(this, getString(R.string.error_list_produts_null), Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(this, getString(R.string.error_list_produts_null), Toast.LENGTH_LONG).show();
		}

		map.setInfoWindowAdapter(new InfoWindowAdapter() {
			@Override
			public View getInfoWindow(final Marker marker) {

				createInfoWindow(marker);

				mapWrapperLayout.setMarkerWithInfoWindow(marker, info_window_product.getViewInfoWindow());

				if (user_location != null) {

					String url = getPolylineDirectionsUrl(marker.getPosition());
					DownloadPolylineMapProductTask downloadProduct = new DownloadPolylineMapProductTask();
					downloadProduct.listener = MapActivity.this;

					// Start downloading json data from Google Directions API
					downloadProduct.execute(url);
				}

				map.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 17));

				return info_window_product.getViewInfoWindow();

			}

			@Override
			public View getInfoContents(Marker marker) {
				return null;
			}
		});

	}

	@Override
	protected void onStart() {
		super.onStart();

	}

	/*************************
	 * INICIO LOCALIZACIÓN USUARIO
	 ************************/

	public boolean getLocation() {
		// I use LocationResult callback class to pass location value from
		// MyLocation to user code.
		if (loc_manager == null)
			loc_manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		// exceptions will be thrown if provider is not permitted.
		try {
			gps_enabled = loc_manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			network_enabled = loc_manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception e) {
			Log.e(TAG, getString(R.string.error_exception_provider) + e.getMessage());

		}

		// don't start listeners if no provider is enabled
		if (!gps_enabled && !network_enabled)
			return false;

		if (gps_enabled)
			loc_manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
		if (network_enabled)
			loc_manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
		timer_location = new Timer();
		timer_location.schedule(new GetLastLocation(), 20000);
		return true;
	}

	LocationListener locationListenerGps = new LocationListener() {
		public void onLocationChanged(Location location) {
			timer_location.cancel();

			user_location = new LatLng(location.getLatitude(), location.getLongitude());

			loc_manager.removeUpdates(this);
			loc_manager.removeUpdates(locationListenerNetwork);

			if (!is_load_map) {
				LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
				map.animateCamera(cameraUpdate);
				map.moveCamera(cameraUpdate);
				is_load_map = true;
			}
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	LocationListener locationListenerNetwork = new LocationListener() {
		public void onLocationChanged(Location location) {
			timer_location.cancel();

			user_location = new LatLng(location.getLatitude(), location.getLongitude());

			loc_manager.removeUpdates(this);
			loc_manager.removeUpdates(locationListenerGps);

			if (!is_load_map) {
				LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
				map.animateCamera(cameraUpdate);
				map.moveCamera(cameraUpdate);
				is_load_map = true;
			}
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	class GetLastLocation extends TimerTask {
		@Override
		public void run() {
			loc_manager.removeUpdates(locationListenerGps);
			loc_manager.removeUpdates(locationListenerNetwork);

			if (gps_enabled)
				gps_location = loc_manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (network_enabled)
				network_location = loc_manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

			// if there are both values use the latest one
			if (gps_location != null && network_location != null) {
				if (gps_location.getTime() > network_location.getTime()) {

					user_location = new LatLng(gps_location.getLatitude(), gps_location.getLongitude());

				}

				else {

					user_location = new LatLng(network_location.getLatitude(), network_location.getLongitude());

				}
				return;
			}

			if (gps_location != null) {

				Handler handler = new Handler(Looper.getMainLooper());
				handler.post(new Runnable() {

					@Override
					public void run() {

						user_location = new LatLng(gps_location.getLatitude(), gps_location.getLongitude());

						return;
					}

				});

				return;
			}
			if (network_location != null) {
				Handler handler = new Handler(Looper.getMainLooper());
				handler.post(new Runnable() {

					@Override
					public void run() {

						user_location = new LatLng(network_location.getLatitude(), network_location.getLongitude());

						return;
					}

				});
			}

		}
	}

	/************************ FIN LOCALIZACIÓN USUARIO ************************/

	// Metodo para obtener la ruta hasta el product
	private String getPolylineDirectionsUrl(LatLng dest) {

		// Origin of route
		String str_origin = "origin=" + user_location.latitude + "," + user_location.longitude;

		// Destination of route
		String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

		// Sensor enabled
		String sensor = "sensor=false";

		// Building the parameters to the web service
		String parameters = str_origin + "&" + str_dest + "&" + sensor + "&mode=walking";
		// https://developers.google.com/maps/documentation/directions/#TravelModes
		// &mode=walking
		// Output format
		String output = "json";

		// Building the url to the web service
		String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

		return url;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		switch (keyCode) {

		case KeyEvent.KEYCODE_BACK:

			startActivity(new Intent(this, MainActivity.class));
			break;
		}

		return false;
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		System.gc();
	}

	@Override
	public void onGetPolylineMapProductComplete(PolylineOptions lineOptions) {
		if (path_line != null) {
			path_line.remove();
		}

		path_line = map.addPolyline(lineOptions);

	}

	private View createInfoWindow(Marker marker_select) {
		String title = marker_select.getTitle();

		String id = title.split("_")[2];

		Product product_select = list_products.get(Integer.valueOf(id));
		// Guardamos en el contexto el producto seleccionado.
		sigiApp.setProduct(product_select);

		info_window_product.createInfoWindow(marker_select, product_select, "driving", user_location);

		return null;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.options_menu_icon, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = null;
		switch (item.getItemId()) {
		/*case R.id.menu_opt_0:			
			break;*/
		// Vista RA
		case R.id.menu_opt_1:
			intent = new Intent(this, MixView.class);
			startActivityForResult(intent, 20);
			break;
		// Vista Mapa
		case R.id.menu_opt_2:
			intent = new Intent(this, MapActivity.class);
			startActivityForResult(intent, 20);
			break;
		// Vista Lista
		case R.id.menu_opt_3:
			intent = new Intent(this, ListProductsActivity.class);
			startActivityForResult(intent, 20);
			break;
		// Vista Filtro
		case R.id.menu_opt_4:
			intent = new Intent(this, FilterActivity.class);
			startActivityForResult(intent, 20);
			break;
		}
		
		return true;
	}

}
