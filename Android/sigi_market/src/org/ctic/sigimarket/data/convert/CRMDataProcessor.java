/*
 * Copyright (C) 2012- Peer internet solutions & Finalist IT Group
 * 
 * This file is part of mixare.
 * 
 * This program is free software: you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 * 
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <http://www.gnu.org/licenses/>
 */
package org.ctic.sigimarket.data.convert;

import java.util.ArrayList;
import java.util.List;

import org.ctic.sigimarket.MixView;
import org.ctic.sigimarket.data.DataHandler;
import org.ctic.sigimarket.data.DataSource;
import org.ctic.sigimarket.marker.CRMMarker;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mixare.lib.marker.Marker;

import android.graphics.Color;
import android.util.Log;

/**
 * CRMDataProcessor
 * 
 * Se encarga de convertir los datos de CRM en bruto a marcador de datos .
 * @author Celia (Fundación CTIC)
 */
public class CRMDataProcessor extends DataHandler implements DataProcessor{

	public static final int MAX_JSON_OBJECTS = 1000;
	
	@Override
	public String[] getUrlMatch() {
		String[] str = {"CRM"};
		return str;
	}

	@Override
	public String[] getDataMatch() {
		String[] str = {"CRM"};
		return str;
	}
	
	@Override
	public boolean matchesRequiredType(String type) {
		if(type.equals(DataSource.TYPE.CRM.name())){
			return true;
		}
		return false;
	}

	@Override
	//public List<Marker> load(String rawData, int taskId, int colour) throws JSONException {
	public List<Marker> load(String rawData, int colour) throws JSONException {
		
		
		//new CrmAuthTask(this, Constants.CRM_USERNAME, Constants.CRM_PASSWORD, Constants.CRM_URL).execute();
		
		Log.e("CRMDataProcessor","estamos en load "+rawData);
		List<Marker> markers = new ArrayList<Marker>();
		JSONObject root = convertToJSON(rawData);
		JSONArray dataArray = root.getJSONArray("results");
		int top = Math.min(MAX_JSON_OBJECTS, dataArray.length());	
		

		for (int i = 0; i < top; i++) {
			JSONObject jo = dataArray.getJSONObject(i);
			
			Marker ma = null;
			if (jo.has("title") && jo.has("lat") && jo.has("lng")
					&& jo.has("elevation")) {

				String id = "";
				if(jo.has("id"))
						id = jo.getString("id");
				
				Log.v(MixView.TAG, "processing Mixare JSON object");
				String link=null;
		
				if(jo.has("has_detail_page") && jo.getInt("has_detail_page")!=0 && jo.has("webpage"))
					link=jo.getString("webpage");
				
				ma = new CRMMarker(
						id,
						jo.getString("title"), 
						jo.getDouble("lat"), 
						jo.getDouble("lng"), 
						jo.getString("info"),
						jo.getDouble("elevation"), 
						 "", 
							 Color.GRAY);
				
				markers.add(ma);
			}
		}
		return markers;
		
	}
	
	private JSONObject convertToJSON(String rawData){
		try {
			return new JSONObject(rawData);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}
	
	
}
