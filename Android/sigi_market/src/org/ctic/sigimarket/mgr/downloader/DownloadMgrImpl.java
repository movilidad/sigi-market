/*
 * Copyright (C) 2010- Peer internet solutions
 * 
 * This file is part of mixare.
 * 
 * This program is free software: you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 * 
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <http://www.gnu.org/licenses/>
 */
package org.ctic.sigimarket.mgr.downloader;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.ctic.sigimarket.MixContext;
import org.ctic.sigimarket.MixView;
import org.ctic.sigimarket.constants.Constants;
import org.ctic.sigimarket.constants.dao.CrmAuthenticationHeader;
import org.ctic.sigimarket.data.DataSource;
import org.ctic.sigimarket.data.DataSource.TYPE;
import org.ctic.sigimarket.data.convert.CRMDataProcessor;
import org.ctic.sigimarket.data.convert.DataConvertor;
import org.ctic.sigimarket.json.GeneratorJSONCRM;
import org.ctic.sigimarket.listener.OnPostCrmAuthListener;
import org.ctic.sigimarket.listener.OnPostCrmExecuteSoapListener;
import org.ctic.sigimarket.mgr.HttpTools;
import org.ctic.sigimarket.task.CrmAuthTask;
import org.ctic.sigimarket.task.CrmExecuteSoapTask;
import org.mixare.lib.marker.Marker;
import org.w3c.dom.Document;

import android.graphics.Color;
import android.util.Log;

/**
 * DownloadMgrImpl
 * Gestiona el proceso de descarga de las distintar marcas. Modifique el c�digo para incluir la funcionalidad
 * del conexi�n con el CRM.
 * 
 * Clase IMPORTANTE para la funcionalidad que quer�amos implementar.
 * 
 * Modificado: Celia (Fundaci�n CTIC)
 *
 */


class DownloadMgrImpl implements Runnable, DownloadManager,OnPostCrmAuthListener,OnPostCrmExecuteSoapListener {

	private boolean stop = false;
	private MixContext ctx;
	private DownloadManagerState state = DownloadManagerState.Confused;
	private LinkedBlockingQueue<ManagedDownloadRequest> todoList = new LinkedBlockingQueue<ManagedDownloadRequest>();
	private ConcurrentHashMap<String, DownloadResult> doneList = new ConcurrentHashMap<String, DownloadResult>();
	private Executor executor = Executors.newSingleThreadExecutor();
	
	//A�adimos el objeto CrmAuthenticationHeader
	private CrmAuthenticationHeader crm_Auth_Header;

	
	DownloadResult result;
	ManagedDownloadRequest mRequest;

	public DownloadMgrImpl(MixContext ctx) {
		if (ctx == null) {
			throw new IllegalArgumentException("Mix Context IS NULL");
		}
		this.ctx = ctx;
		state=DownloadManagerState.OffLine;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ctic.sigimarket.mgr.downloader.DownloadManager#run()
	 */
	public void run() {
		//ManagedDownloadRequest mRequest;
		//DownloadResult result;
		stop = false;
		while (!stop) {
			state=DownloadManagerState.OnLine;
			// Wait for proceed
			while (!stop) {
				try {
					mRequest = todoList.take();
					state=DownloadManagerState.Downloading;
					//TODO cuando pinta marcas raras es porque no esta pillando bien el tipo!!
					if(mRequest.getOriginalRequest().getSource().getName().equals(DataSource.TYPE.CRM.name())){
						//Si se trate de CRM nos autentificamos y obtenemos los productos (pisos). 
						new CrmAuthTask(this, Constants.CRM_USERNAME, Constants.CRM_PASSWORD, Constants.CRM_URL).execute();	
					}
					else{
					//El resto...	
					result = processRequest(mRequest);
					}
										
					
					
				} catch (InterruptedException e) {
					result = new DownloadResult();
					result.setError(e, null);
				}
				doneList.put(result.getIdOfDownloadRequest(), result);
				state=DownloadManagerState.OnLine;
			}
		}
		state=DownloadManagerState.OffLine;
	}

	private DownloadResult processRequest(ManagedDownloadRequest mRequest) {
		
		
		
		DownloadRequest request = mRequest.getOriginalRequest();
		final DownloadResult result = new DownloadResult();
		try {
			if (request == null) {
				throw new Exception("Request is null");
			}
			
			if (!request.getSource().isWellFormed()) {
				throw new Exception("Datasource in not WellFormed");
			}

			
			
			String pageContent = HttpTools.getPageContent(request,
					ctx.getContentResolver());
			
			

			if (pageContent != null) {
				// try loading Marker data
				
				List<Marker> markers = DataConvertor.getInstance().load(
						request.getSource().getUrl(), pageContent,
						request.getSource());
				result.setAccomplish(mRequest.getUniqueKey(), markers,
						request.getSource());
			}
		} catch (Exception ex) {
			result.setError(ex, request);
			Log.w(MixContext.TAG, "ERROR ON DOWNLOAD REQUEST", ex);
		}
		
		
		
		return result;
	}
	
	//Metodo especifico para tratar el contenido de CRM
	private DownloadResult processRequestCRM(ManagedDownloadRequest mRequest,String pageContent) {
		
		
		
		DownloadRequest request = mRequest.getOriginalRequest();
		final DownloadResult result = new DownloadResult();
		try {
			if (request == null) {
				throw new Exception("Request is null");
			}
			
			if (!request.getSource().isWellFormed()) {
				throw new Exception("Datasource in not WellFormed");
			}

			

			if (pageContent != null) {
				
				List<Marker> markers = new CRMDataProcessor().load(
						pageContent,Color.GRAY);
				result.setAccomplish(mRequest.getUniqueKey(), markers,
						request.getSource());
			}
		} catch (Exception ex) {
			result.setError(ex, request);
			Log.w(MixContext.TAG, "ERROR ON DOWNLOAD REQUEST", ex);
		}
		
		
		
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ctic.sigimarket.mgr.downloader.DownloadManager#purgeLists()
	 */
	public synchronized void resetActivity() {
		todoList.clear();
		doneList.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.ctic.sigimarket.mgr.downloader.DownloadManager#submitJob(org.ctic.sigimarket.mgr.downloader
	 * .DownloadRequest)
	 */
	public String submitJob(DownloadRequest job) {
		String jobId = null;
		if (job != null && job.getSource().isWellFormed()) {
			ManagedDownloadRequest mJob;
			if (!todoList.contains(job)) {
				mJob = new ManagedDownloadRequest(job);
				todoList.add(mJob);
				Log.i(MixView.TAG, "Submitted " + job.toString());
				jobId = mJob.getUniqueKey();
			}
		}
		
		//Log.e("DownloadMgrImplement", "submitJob "+ jobId);
		
		return jobId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.ctic.sigimarket.mgr.downloader.DownloadManager#getReqResult(java.lang.String)
	 */
	public DownloadResult getReqResult(String jobId) {
		
		//Log.e("DownloadMgrImplement", "getReqResult "+ jobId);
		
		DownloadResult result = doneList.get(jobId);
		doneList.remove(jobId);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ctic.sigimarket.mgr.downloader.DownloadManager#getNextResult()
	 */
	public synchronized DownloadResult getNextResult() {
		DownloadResult result = null;
		if (!doneList.isEmpty()) {
			String nextId = doneList.keySet().iterator().next();
			result = doneList.get(nextId);
			doneList.remove(nextId);
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ctic.sigimarket.mgr.downloader.DownloadManager#getResultSize()
	 */
	public int getResultSize(){
		return doneList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ctic.sigimarket.mgr.downloader.DownloadManager#isDone()
	 */
	public Boolean isDone() {
		return todoList.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ctic.sigimarket.mgr.downloader.DownloadManager#goOnline()
	 */
	public void switchOn() {
		if (DownloadManagerState.OffLine.equals(getState())){
		    executor.execute(this);
		}else{
			Log.i(MixView.TAG, "DownloadManager already started");
		}
	}

	public void switchOff() {
		stop=true;
	}

	@Override
	public DownloadManagerState getState() {
		return state;
	}

	

	@Override
	public void onPostCrmAuthTaskComplete(CrmAuthenticationHeader result) {
		
		//OBTENEMOS EL HEADER DE LA LLAMADA
			crm_Auth_Header = result;			
			
			//Una vez que la autentificaci�n ha ido bien, obtenemos los datos de los productos (pisos).
			new CrmExecuteSoapTask(this, crm_Auth_Header, Constants.CRM_REQUEST_RETRIEVE_MULTIPLE, Constants.CRM_URL).execute();
			
			
			 
		
		
		
		
	}

	@Override
	public void onPostCrmAuthTaskError(int id_status, String error) {
		
		
	}
	
	

	@Override
	public void onPostCrmExecuteSoapTaskError(int id_status, String error) {
		
		
	}

	

	@Override
	public void onPostCrmExecuteSoapRetrieveMultipleProductTaskComplete(Document result) {	
		
		
		try {
			//Una vez que obtenemos los productos (pisos) del CRM llamamos al processRequestCRM
			this.result = processRequestCRM(mRequest,GeneratorJSONCRM.generarJsonCRM(result,ctx));
			doneList.put(this.result.getIdOfDownloadRequest(), this.result);
			state=DownloadManagerState.OnLine;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}

	
}
