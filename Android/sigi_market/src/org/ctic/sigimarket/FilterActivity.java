package org.ctic.sigimarket;

import org.ctic.sigimarket.application.SigiApplication;
import org.ctic.sigimarket.constants.Constants;
import org.ctic.sigimarket.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.ActionBarActivity;

/**
 * FilterActivity Permite visualizar la pantalla de los filtros que se pueden aplicar sobre los productos, obtenidos
 * del CRM, y almacena estos en SharedPreferences, implementando toda la l�gica.
 * 
 * @author Celia (Fundaci�n CTIC)
 *
 */

public class FilterActivity extends ActionBarActivity implements OnItemSelectedListener {

	public static final String TAG = FilterActivity.class.getSimpleName();

	private SigiApplication sigiApp;

	// El API SharedPreferences nos permitira guardar los filtros aplicados por
	// el usuario.
	// Cada preferencia se almacenar� en forma de clave-valor.
	private SharedPreferences sharedPreferences;
	private Editor editor;
	private final String usePluginsPrefs = "sigiUsePluginsPrefs";

	// Filtro estado
	private RadioButton radioButton_buy;
	private RadioButton radioButton_rent;

	// Filtro tipo
	private Spinner spinner_type;

	// Filtro precio compra
	private TextView textView_title_price_buy;
	private LinearLayout linearLayout_price_price_buy;
	private Spinner spinner_price_buy_init;
	private Spinner spinner_price_buy_finish;

	// Filtro precio alquiler
	private TextView textView_title_price_rent;
	private LinearLayout linearLayout_price_price_rent;
	private Spinner spinner_price_rent_init;
	private Spinner spinner_price_rent_finish;

	// Filtro habitaciones
	private CheckBox checkBox_rooms_1;
	private CheckBox checkBox_rooms_2;
	private CheckBox checkBox_rooms_3;

	// Filtro ba�os
	private CheckBox checkBox_wc_1;
	private CheckBox checkBox_wc_2;
	private CheckBox checkBox_wc_3;

	// Filtro tama�o m2
	private Spinner spinner_size_init;
	private Spinner spinner_size_finish;

	// Adaptador spinners
	private ArrayAdapter<String> adaptador;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_filter);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		sigiApp = (SigiApplication) getApplicationContext();

		sharedPreferences = getSharedPreferences(usePluginsPrefs, MODE_PRIVATE);
		editor = sharedPreferences.edit();

		textView_title_price_buy = (TextView) findViewById(R.id.textView_title_price_buy);
		linearLayout_price_price_buy = (LinearLayout) findViewById(R.id.linearLayout_price_price_buy);

		textView_title_price_rent = (TextView) findViewById(R.id.textView_title_price_rent);
		linearLayout_price_price_rent = (LinearLayout) findViewById(R.id.linearLayout_price_price_rent);

		// Filtro estado
		radioButton_buy = (RadioButton) findViewById(R.id.radioButton_buy);
		radioButton_buy.setChecked(true);
		textView_title_price_buy.setVisibility(View.VISIBLE);
		linearLayout_price_price_buy.setVisibility(View.VISIBLE);

		if (sharedPreferences.contains(Constants.FILTER_RADIOBUTTON_BUY)) {
			radioButton_buy.setChecked(true);

			textView_title_price_buy.setVisibility(View.VISIBLE);
			linearLayout_price_price_buy.setVisibility(View.VISIBLE);
			
			textView_title_price_rent.setVisibility(View.GONE);
			linearLayout_price_price_rent.setVisibility(View.GONE);

		}

		radioButton_rent = (RadioButton) findViewById(R.id.radioButton_rent);
		if (sharedPreferences.contains(Constants.FILTER_RADIOBUTTON_RENT)) {
			radioButton_rent.setChecked(true);
			
			

			textView_title_price_rent.setVisibility(View.VISIBLE);
			linearLayout_price_price_rent.setVisibility(View.VISIBLE);
			
			textView_title_price_buy.setVisibility(View.GONE);
			linearLayout_price_price_buy.setVisibility(View.GONE);

		}

		// Filtro tipo
		adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Constants.FILTER_TYPE);
		adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner_type = (Spinner) findViewById(R.id.spinner_type);
		spinner_type.setAdapter(adaptador);

		if (sharedPreferences.contains(Constants.FILTER_SPINNER_TYPE)) {
			spinner_type.setSelection(sharedPreferences.getInt(Constants.FILTER_SPINNER_TYPE, 0));
		}

		// Filtro precio venta init
		adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Constants.FILTER_PRICE_BUY);
		adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner_price_buy_init = (Spinner) findViewById(R.id.spinner_price_buy_init);
		spinner_price_buy_init.setAdapter(adaptador);
		spinner_price_buy_init.setOnItemSelectedListener(this);

		if (sharedPreferences.contains(Constants.FILTER_SPINNER_PRICE_BUY_INIT)) {
			spinner_price_buy_init.setSelection(sharedPreferences.getInt(Constants.FILTER_SPINNER_PRICE_BUY_INIT, 0));
		}

		// Filtro precio venta finish
		spinner_price_buy_finish = (Spinner) findViewById(R.id.spinner_price_buy_finish);
		spinner_price_buy_finish.setAdapter(adaptador);
		spinner_price_buy_finish.setOnItemSelectedListener(this);

		if (sharedPreferences.contains(Constants.FILTER_SPINNER_PRICE_BUY_FINISH)) {
			spinner_price_buy_finish
					.setSelection(sharedPreferences.getInt(Constants.FILTER_SPINNER_PRICE_BUY_FINISH, 0));
		}

		// Filtro precio alquiler init
		adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Constants.FILTER_PRICE_RENT);
		adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner_price_rent_init = (Spinner) findViewById(R.id.spinner_price_rent_init);
		spinner_price_rent_init.setAdapter(adaptador);
		spinner_price_rent_init.setOnItemSelectedListener(this);

		if (sharedPreferences.contains(Constants.FILTER_SPINNER_PRICE_RENT_INIT)) {
			spinner_price_rent_init.setSelection(sharedPreferences.getInt(Constants.FILTER_SPINNER_PRICE_RENT_INIT, 0));
		}

		// Filtro precio alquiler finish
		spinner_price_rent_finish = (Spinner) findViewById(R.id.spinner_price_rent_finish);
		spinner_price_rent_finish.setAdapter(adaptador);
		spinner_price_rent_finish.setOnItemSelectedListener(this);

		if (sharedPreferences.contains(Constants.FILTER_SPINNER_PRICE_RENT_FINISH)) {
			spinner_price_rent_finish
					.setSelection(sharedPreferences.getInt(Constants.FILTER_SPINNER_PRICE_RENT_FINISH, 0));
		}

		// Filtro habitaciones
		checkBox_rooms_1 = (CheckBox) findViewById(R.id.checkBox_rooms_1);
		if (sharedPreferences.contains(Constants.FILTER_CHECKBOX_ROOMS_1)) {
			checkBox_rooms_1.setChecked(true);
		}
		checkBox_rooms_2 = (CheckBox) findViewById(R.id.checkBox_rooms_2);
		if (sharedPreferences.contains(Constants.FILTER_CHECKBOX_ROOMS_2)) {
			checkBox_rooms_2.setChecked(true);
		}
		checkBox_rooms_3 = (CheckBox) findViewById(R.id.checkBox_rooms_3);
		if (sharedPreferences.contains(Constants.FILTER_CHECKBOX_ROOMS_3)) {
			checkBox_rooms_3.setChecked(true);
		}

		// Filtro ba�os
		checkBox_wc_1 = (CheckBox) findViewById(R.id.checkBox_wc_1);
		if (sharedPreferences.contains(Constants.FILTER_CHECKBOX_WC_1)) {
			checkBox_wc_1.setChecked(true);
		}
		checkBox_wc_2 = (CheckBox) findViewById(R.id.checkBox_wc_2);
		if (sharedPreferences.contains(Constants.FILTER_CHECKBOX_WC_2)) {
			checkBox_wc_2.setChecked(true);
		}
		checkBox_wc_3 = (CheckBox) findViewById(R.id.checkBox_wc_3);
		if (sharedPreferences.contains(Constants.FILTER_CHECKBOX_WC_3)) {
			checkBox_wc_3.setChecked(true);
		}

		// Filtro tama�o m2 init
		adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Constants.FILTER_SIZE_M2);
		adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner_size_init = (Spinner) findViewById(R.id.spinner_size_init);
		spinner_size_init.setAdapter(adaptador);
		spinner_size_init.setOnItemSelectedListener(this);
		if (sharedPreferences.contains(Constants.FILTER_SPINNER_SIZE_INIT)) {
			spinner_size_init.setSelection(sharedPreferences.getInt(Constants.FILTER_SPINNER_SIZE_INIT, 0));
		}

		// Filtro tama�o m2 finish
		spinner_size_finish = (Spinner) findViewById(R.id.spinner_size_finish);
		spinner_size_finish.setAdapter(adaptador);
		spinner_size_finish.setOnItemSelectedListener(this);
		if (sharedPreferences.contains(Constants.FILTER_SPINNER_SIZE_FINISH)) {
			spinner_size_finish.setSelection(sharedPreferences.getInt(Constants.FILTER_SPINNER_SIZE_FINISH, 0));
		}

		Button button_save = (Button) findViewById(R.id.button_save);
		button_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				editor.putInt(Constants.FILTER_SPINNER_TYPE, spinner_type.getSelectedItemPosition());

				editor.putInt(Constants.FILTER_SPINNER_SIZE_INIT, spinner_size_init.getSelectedItemPosition());
				editor.putInt(Constants.FILTER_SPINNER_SIZE_FINISH, spinner_size_finish.getSelectedItemPosition());

				if (radioButton_rent.isChecked()) {
					editor.putBoolean(Constants.FILTER_RADIOBUTTON_RENT, true);

					editor.putInt(Constants.FILTER_SPINNER_PRICE_RENT_INIT,
							spinner_price_rent_init.getSelectedItemPosition());
					editor.putInt(Constants.FILTER_SPINNER_PRICE_RENT_FINISH,
							spinner_price_rent_finish.getSelectedItemPosition());
				} else {
					editor.remove(Constants.FILTER_RADIOBUTTON_RENT);

					editor.remove(Constants.FILTER_SPINNER_PRICE_RENT_INIT);
					editor.remove(Constants.FILTER_SPINNER_PRICE_RENT_FINISH);
				}
				if (radioButton_buy.isChecked()) {
					editor.putBoolean(Constants.FILTER_RADIOBUTTON_BUY, true);

					editor.putInt(Constants.FILTER_SPINNER_PRICE_BUY_INIT,
							spinner_price_buy_init.getSelectedItemPosition());
					editor.putInt(Constants.FILTER_SPINNER_PRICE_BUY_FINISH,
							spinner_price_buy_finish.getSelectedItemPosition());
				} else {
					editor.remove(Constants.FILTER_RADIOBUTTON_BUY);

					editor.remove(Constants.FILTER_SPINNER_PRICE_BUY_INIT);
					editor.remove(Constants.FILTER_SPINNER_PRICE_BUY_FINISH);
				}

				if (checkBox_rooms_1.isChecked()) {
					editor.putBoolean(Constants.FILTER_CHECKBOX_ROOMS_1, true);
				} else {
					editor.remove(Constants.FILTER_CHECKBOX_ROOMS_1);
				}
				if (checkBox_rooms_2.isChecked()) {
					editor.putBoolean(Constants.FILTER_CHECKBOX_ROOMS_2, true);
				} else {
					editor.remove(Constants.FILTER_CHECKBOX_ROOMS_2);
				}
				if (checkBox_rooms_3.isChecked()) {
					editor.putBoolean(Constants.FILTER_CHECKBOX_ROOMS_3, true);
				} else {
					editor.remove(Constants.FILTER_CHECKBOX_ROOMS_3);
				}

				if (checkBox_wc_1.isChecked()) {
					editor.putBoolean(Constants.FILTER_CHECKBOX_WC_1, true);
				} else {
					editor.remove(Constants.FILTER_CHECKBOX_WC_1);
				}
				if (checkBox_wc_2.isChecked()) {
					editor.putBoolean(Constants.FILTER_CHECKBOX_WC_2, true);
				} else {
					editor.remove(Constants.FILTER_CHECKBOX_WC_2);
				}
				if (checkBox_wc_3.isChecked()) {
					editor.putBoolean(Constants.FILTER_CHECKBOX_WC_3, true);
				} else {
					editor.remove(Constants.FILTER_CHECKBOX_WC_3);
				}
				editor.commit();

				backActivity();

			}
		});

	}

	@Override
	protected void onStart() {

		super.onStart();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		switch (keyCode) {

		case KeyEvent.KEYCODE_BACK:

			backActivity();
			break;
		}

		return false;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		System.gc();
	}

	public void onRadioButtonClicked(View view) {
		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();

		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.radioButton_buy:
			if (checked) {
				textView_title_price_buy.setVisibility(View.VISIBLE);
				linearLayout_price_price_buy.setVisibility(View.VISIBLE);
				textView_title_price_rent.setVisibility(View.GONE);
				linearLayout_price_price_rent.setVisibility(View.GONE);
			} else {
				textView_title_price_buy.setVisibility(View.GONE);
				linearLayout_price_price_buy.setVisibility(View.GONE);
				textView_title_price_rent.setVisibility(View.VISIBLE);
				linearLayout_price_price_rent.setVisibility(View.VISIBLE);
			}
			break;
		case R.id.radioButton_rent:
			if (checked) {
				textView_title_price_buy.setVisibility(View.GONE);
				linearLayout_price_price_buy.setVisibility(View.GONE);
				textView_title_price_rent.setVisibility(View.VISIBLE);
				linearLayout_price_price_rent.setVisibility(View.VISIBLE);
			} else {
				textView_title_price_rent.setVisibility(View.GONE);
				linearLayout_price_price_rent.setVisibility(View.GONE);
				textView_title_price_buy.setVisibility(View.VISIBLE);
				linearLayout_price_price_buy.setVisibility(View.VISIBLE);
			}
			break;
		}
	}

	public void onChecRadioButtonClicked(View view) {
		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();

		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.radioButton_buy:
			if (checked) {
				textView_title_price_buy.setVisibility(View.VISIBLE);
				linearLayout_price_price_buy.setVisibility(View.VISIBLE);
			} else {
				textView_title_price_buy.setVisibility(View.GONE);
				linearLayout_price_price_buy.setVisibility(View.GONE);
			}
			break;
		case R.id.radioButton_rent:
			if (checked) {
				textView_title_price_rent.setVisibility(View.VISIBLE);
				linearLayout_price_price_rent.setVisibility(View.VISIBLE);
			} else {
				textView_title_price_rent.setVisibility(View.GONE);
				linearLayout_price_price_rent.setVisibility(View.GONE);
			}
			break;
		}
	}

	private void backActivity() {

		switch (sigiApp.getOpt_main()) {
		// Data sources
		case Constants.OPT_VIEW_1:

			startActivity(new Intent(this, MixView.class));
			break;
		case Constants.OPT_VIEW_2:
			startActivity(new Intent(this, MapActivity.class));
			break;
		case Constants.OPT_VIEW_3:
			startActivity(new Intent(this, ListProductsActivity.class));
			break;

		}

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

		Spinner spinner = (Spinner) parent;
		switch (spinner.getId()) {
		case R.id.spinner_price_buy_init:

			if (position > spinner_price_buy_finish.getSelectedItemPosition()
					&& spinner_price_buy_finish.getSelectedItemPosition() != 0) {
				Toast.makeText(this, getString(R.string.error_filter_price_buy_min_max),
						Toast.LENGTH_LONG).show();
				spinner_price_buy_init.setSelection(0);
			}
			break;
		case R.id.spinner_price_buy_finish:

			if (position < spinner_price_buy_init.getSelectedItemPosition()
					&& spinner_price_buy_finish.getSelectedItemPosition() != 0) {
				Toast.makeText(this,getString(R.string.error_filter_price_buy_max_min) , Toast.LENGTH_LONG)
						.show();
				spinner_price_buy_finish.setSelection(0);
			}
			break;
		case R.id.spinner_price_rent_init:

			if (position > spinner_price_rent_finish.getSelectedItemPosition()
					&& spinner_price_rent_finish.getSelectedItemPosition() != 0) {
				Toast.makeText(this, getString(R.string.error_filter_price_rent_min_max),
						Toast.LENGTH_LONG).show();
				spinner_price_rent_init.setSelection(0);
			}
			break;
		case R.id.spinner_price_rent_finish:

			if (position < spinner_price_rent_init.getSelectedItemPosition()
					&& spinner_price_rent_finish.getSelectedItemPosition() != 0) {
				Toast.makeText(this, getString(R.string.error_filter_price_rent_max_min),
						Toast.LENGTH_LONG).show();
				spinner_price_rent_finish.setSelection(0);
			}

			break;
		case R.id.spinner_size_init:

			if (position > spinner_size_finish.getSelectedItemPosition()
					&& spinner_size_finish.getSelectedItemPosition() != 0) {
				Toast.makeText(this, getString(R.string.error_filter_size_min_max), Toast.LENGTH_LONG)
						.show();
				spinner_size_init.setSelection(0);
			}
			break;
		case R.id.spinner_size_finish:

			if (position < spinner_size_init.getSelectedItemPosition()
					&& spinner_size_finish.getSelectedItemPosition() != 0) {
				Toast.makeText(this, getString(R.string.error_filter_size_max_min), Toast.LENGTH_LONG)
						.show();
				spinner_size_finish.setSelection(0);
			}
			break;

		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case android.R.id.home:

			backActivity();
			break;
		}

		return super.onOptionsItemSelected(item);
	}

}
