package org.ctic.sigimarket.application;

import java.util.ArrayList;

import org.ctic.sigimarket.utils.FilterUtils;

import com.microsoft.xrm.sdk.client.Product;
import android.app.Application;
import android.content.SharedPreferences;

/**
 * SigiApplication
 * 
 * Permite guardar en el contexto de la aplicación todo tipo de información.
 * Esto nos permite no tener que solicitar los datos cada vez que se necesite a los SW o 
 * conservarlos en caso de cambiar de una Activity a otra....
 * 
 * @author Celia (Fundación CTIC).
 */

public class SigiApplication extends Application {

	private ArrayList<Product> products;

	private Product product;

	private SharedPreferences sharedPreferences;

	private final String usePluginsPrefs = "sigiUsePluginsPrefs";

	// Almacena en el contexto la opción del menu lateral seleccionada.
	private int opt_main;

	public int getOpt_main() {
		return opt_main;
	}

	public void setOpt_main(int opt_main) {
		this.opt_main = opt_main;
	}

	public ArrayList<Product> getProducts() {

		// Antes de devolver los producto los filtramos.
		return filterProducts();
	}

	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}

	public Product getProduct() {

		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	// TODO MIRAR!!
	public boolean filterProduct(Product product) {

		sharedPreferences = getSharedPreferences(usePluginsPrefs, MODE_PRIVATE);

		if (FilterUtils.filterAll(product, sharedPreferences)) {
			return true;
		}

		return false;

	}

	private ArrayList<Product> filterProducts() {

		sharedPreferences = getSharedPreferences(usePluginsPrefs, MODE_PRIVATE);

		ArrayList<Product> products_filter = new ArrayList<Product>();

		if (products != null) {

			for (int i = 0; i < products.size(); i++) {
				Product product = products.get(i);
				if (FilterUtils.filterAll(product, sharedPreferences)) {
					products_filter.add(product);
				}

			}

		}

		return products_filter;

	}

}
