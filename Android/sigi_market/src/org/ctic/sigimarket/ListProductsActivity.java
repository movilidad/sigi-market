package org.ctic.sigimarket;

import java.lang.reflect.Method;
import java.util.List;

import org.ctic.sigimarket.adapter.ListProductsAdapter;
import org.ctic.sigimarket.application.SigiApplication;
import org.ctic.sigimarket.constants.Constants;

import com.microsoft.xrm.sdk.client.Product;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

/**
 * ListProductsActivity Permite visualizar, en forma de lista, los productos
 * obtenidos del CRM.
 * 
 * @author Celia (Fundación CTIC)
 *
 */

public class ListProductsActivity extends ActionBarActivity implements

OnItemClickListener {

	public static final String TAG = ListProductsActivity.class.getSimpleName();

	private SigiApplication sigiApp;

	private List<Product> list_products;

	private ListView listView_products;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_products);

		sigiApp = (SigiApplication) getApplicationContext();
		sigiApp.setOpt_main(Constants.OPT_VIEW_3);

		
		
		// Cargamos productos guardados en el contexto
		list_products = sigiApp.getProducts();

		if (list_products != null) {

			if (list_products.size() > 0) {

				listView_products = (ListView) findViewById(R.id.listView_products);

				ListProductsAdapter listProductsAdapter = new ListProductsAdapter(this, R.layout.custom_item_list,
						list_products);
				listView_products.setAdapter(listProductsAdapter);

				listView_products.setOnItemClickListener(this);
			} else {
				Toast.makeText(this, getString(R.string.error_list_produts_null), Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(this, getString(R.string.error_list_produts_null), Toast.LENGTH_LONG).show();
		}

	}

	@Override
	protected void onStart() {

		super.onStart();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

		view.setBackgroundColor(getResources().getColor(R.color.background_color_orange_light));

		Product product_select = list_products.get(position);
		// Guardamos en el contexto el producto seleccionado.
		sigiApp.setProduct(product_select);
		// Llamamos a la activity ProductActivity para mostrar la
		// información del producto(piso).
		startActivity(new Intent(this, ProductActivity.class));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.options_menu_icon, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = null;
		switch (item.getItemId()) {
		
		// Vista RA
		case R.id.menu_opt_1:
			intent = new Intent(this, MixView.class);
			startActivityForResult(intent, 20);
			break;
		// Vista Mapa
		case R.id.menu_opt_2:
			intent = new Intent(this, MapActivity.class);
			startActivityForResult(intent, 20);
			break;
		// Vista Lista
		case R.id.menu_opt_3:
			intent = new Intent(this, ListProductsActivity.class);
			startActivityForResult(intent, 20);
			break;
		// Vista Filtro
		case R.id.menu_opt_4:
			intent = new Intent(this, FilterActivity.class);
			startActivityForResult(intent, 20);
			break;
		}
		
		return true;
	}
	
	

}
