package org.ctic.sigimarket.listener;

import org.ctic.sigimarket.constants.dao.CrmAuthenticationHeader;

/**
 * OnPostCrmAuthListener
 * 
 * Al implementar esta interfaz, se realizara una notificación cuando la autentificación con CRM 
 * se haya realizado.
 * 
 * @author Celia (Fundación CTIC).
 */

public interface OnPostCrmAuthListener {
	
	public void onPostCrmAuthTaskComplete(CrmAuthenticationHeader result);
	public void onPostCrmAuthTaskError(int id_status,String error);

}
