package org.ctic.sigimarket.listener;

import org.w3c.dom.Document;

/**
 * OnPostCrmExecuteSoapListener
 * 
 * Al implementar esta interfaz, se realizara una notificación cuando la obtención de datos contra el CRM 
 * se haya realizado.
 * 
 * @author Celia (Fundación CTIC).
 */

public interface OnPostCrmExecuteSoapListener {
	
	public void onPostCrmExecuteSoapRetrieveMultipleProductTaskComplete(Document result);
	public void onPostCrmExecuteSoapTaskError(int id_status,String error);

}
