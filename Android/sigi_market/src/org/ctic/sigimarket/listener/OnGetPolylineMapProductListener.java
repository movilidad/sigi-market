package org.ctic.sigimarket.listener;

import com.androidmapsextensions.PolylineOptions;



public interface OnGetPolylineMapProductListener {
	
	public void onGetPolylineMapProductComplete(PolylineOptions lineOptions);
    
}
