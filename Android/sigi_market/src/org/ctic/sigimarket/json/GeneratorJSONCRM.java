package org.ctic.sigimarket.json;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.UUID;

import org.ctic.sigimarket.MixContext;
import org.ctic.sigimarket.application.SigiApplication;
import org.ctic.sigimarket.constants.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.microsoft.xrm.sdk.Money;
import com.microsoft.xrm.sdk.OptionSetValue;
import com.microsoft.xrm.sdk.client.Product;
import com.microsoft.xrm.sdk.client.ize.ize_numeros;
import com.microsoft.xrm.sdk.client.ize.ize_subtipoloologia;
import com.microsoft.xrm.sdk.client.ize.ize_tipologia;

import android.util.Log;

/**
 * GeneratorJSONCRM
 * 
 * Genera el JSON para poder pintar las marcas en la pantalla de RA.
 * 
 * @author Celia (Fundaci�n CTIC)
 */

public class GeneratorJSONCRM {

	private static final String TAG = GeneratorJSONCRM.class.getSimpleName();

	private static String M = "";

	private static JsonArray jsonAResultsCRM;

	private static SigiApplication sigiApp;

	public static String generarJsonCRM(Document result, MixContext ctx) throws Exception {

		sigiApp = (SigiApplication) ctx.getApplicationContext();

		JsonObject jsonObjCRM = new JsonObject();

		jsonObjCRM.addProperty("status", "OK");

		jsonAResultsCRM = new JsonArray();

		Log.e(TAG, M + " " + result.toString());
		
		
		

		String ize_latitud = "";
		String ize_longitud = "";
		String productid = "";
		String ize_planta = "";
		String ize_t_tipologia = "";
		String ize_subtipologia = "";
		//String ize_categoriasinmuebles = "";
		OptionSetValue numHabitaciones = null;
		OptionSetValue numBanio = null;
		Money precio_venta = null;
		Money precio_venta_m2 = null;
		Money precio_alquiler = null;
		Money precio_alquiler_m2 = null;

		BigDecimal m2 = null;
		OptionSetValue alquiler = null;
		OptionSetValue venta = null;

		ArrayList<Product> list_products = new ArrayList<>();

		JsonObject jsonObjResult = new JsonObject();
		Product product = new Product();

		int cont = 0;

		//NodeList nodes = result.getElementsByTagName("b:KeyValuePairOfstringanyType");
		
		NodeList nodes = result.getElementsByTagName("b:Entity");
		
		System.out.println("NODOS ENTITY " +nodes.getLength());
		
		//int cont=0;
		
		for (int i = 0; i < nodes.getLength(); i++) {
			
			Log.e(TAG,"CONTENIDO ENTITY "+i+" "+nodes.item(i).getTextContent());
			
			NodeList nodesEntity = nodes.item(i).getChildNodes();
			
			//for (int j = 0; j < nodesEntity.getLength(); j++) {
			
			Log.e(TAG,"INFO NODO ATRIBUTES "+nodesEntity.item(0).getTextContent());
			
			/*if(!nodesEntity.item(0).getTextContent().contains("ize_latitud")||!nodesEntity.item(0).getTextContent().contains("ize_longitud")){
				cont++;
			}*/
			try{
			
			if (nodesEntity.item(0).getTextContent().contains("ize_latitud") && nodesEntity.item(0).getTextContent().contains("ize_longitud")
					&& nodesEntity.item(0).getTextContent().contains("ize_tipologia")&&nodesEntity.item(0).getTextContent().contains("ize_subtipologia")
					
												 /* && nodesEntity.item(0).getTextContent().contains("ize_numhabitaciones")
												  && nodesEntity.item(0).getTextContent().contains("ize_numbanios")*/ &&
												  ((nodesEntity.item(0).getTextContent().contains("price") &&
												  nodesEntity.item(0).getTextContent().contains("ize_precioventam2constr")) ||
												 (nodesEntity.item(0).getTextContent().contains("ize_preciosalquiler") &&
												  nodesEntity.item(0).getTextContent().contains("ize_precioalquilermconst"))) && nodesEntity.item(0).getTextContent().contains("ize_m2construidosdecimal")
												 ) {
													 
													 
													 NodeList nodesAttributes = nodesEntity.item(0).getChildNodes();
														
														Log.e(TAG,"CANTIDAD ATRIBUTES "+nodesAttributes.getLength());
														
														
														
														for (int j = 0; j < nodesAttributes.getLength(); j++) {
														
															Log.e(TAG,"CONTENT ATRIBUTES "+nodesAttributes.item(j).getTextContent());
														String name_atribute = nodesAttributes.item(j).getFirstChild().getTextContent();
														String value_atribute = nodesAttributes.item(j).getLastChild().getTextContent();
															//Log.e(TAG,"NAME ATRIBUTES "+nodesAttributes.item(j).getFirstChild().getTextContent());
															//Log.e(TAG,"VALUE ATRIBUTES "+nodesAttributes.item(j).getLastChild().getTextContent());
															
															
															if (name_atribute.equals("productid")) {
																productid = value_atribute;
																jsonObjResult.addProperty("id", productid);
																product.setId(UUID.fromString(productid));
															}

															if (name_atribute.equals("ize_subtipologia")) {
																product.setIze_subtipologia(
																		new OptionSetValue(Integer.valueOf(value_atribute)));
																ize_subtipologia = ize_subtipoloologia.toObject(product.getIze_subtipologia().getValue()).name();
																Log.e(TAG, "ize_subtipologia " + ize_subtipologia);

															}

															if (name_atribute.equals("ize_tipologia")) {
																product.setIze_tipologia(
																		new OptionSetValue(Integer.valueOf(value_atribute)));
																ize_t_tipologia = ize_tipologia.toObject(product.getIze_tipologia().getValue()).name();
																Log.e(TAG, "ize_tipologia " + ize_t_tipologia);

															}

															if (name_atribute.equals("ize_direccion_calle1")) {
																product.setIze_direccion_calle1(value_atribute);
															}
															if (name_atribute.equals("ize_direccion_numero")) {
																product.setIze_direccion_numero(value_atribute);
															}

															if (name_atribute.equals("ize_planta")) {
																ize_planta = value_atribute;
																product.setIze_Planta(new OptionSetValue(Integer.valueOf(ize_planta)));
															}

															if (name_atribute.equals("ize_direccion_letra")) {
																product.setIze_Direccion_letra(value_atribute);

															}

															if (name_atribute.equals("ize_numhabitaciones")) {
																numHabitaciones = new OptionSetValue(Integer.valueOf(value_atribute));
																product.setIze_Numhabitaciones(numHabitaciones);

															}

															if (name_atribute.equals("ize_numbanios")) {
																numBanio = new OptionSetValue(Integer.valueOf(value_atribute));
																product.setIze_NumBanios(numBanio);

															}

															if (name_atribute.equals("ize_m2construidosdecimal")) {
																m2 = new BigDecimal(value_atribute);
																product.setIze_m2construidosdecimal(m2);

															}

															if (name_atribute.equals("ize_precioventam2constr")) {
																precio_venta_m2 = new Money(
																		new BigDecimal(value_atribute.split(",")[0]));
																product.setIze_PrecioVentam2constr(precio_venta_m2);

															}

															if (name_atribute.equals("price")) {

																precio_venta = new Money(new BigDecimal(value_atribute));
																product.setPrice(precio_venta);

															}

															if (name_atribute.equals("ize_precioalquilermconst")) {
																precio_alquiler_m2 = new Money(
																		new BigDecimal(value_atribute.split(",")[0]));
																product.setIze_PrecioAlquilermconst(precio_alquiler_m2);

															}

															if (name_atribute.equals("ize_preciosalquiler")) {

																precio_alquiler = new Money(new BigDecimal(value_atribute));
																product.setIze_PreciosAlquiler(precio_alquiler);

															}

															if (name_atribute.equals("ize_venta")) {

																venta = new OptionSetValue(Integer.valueOf(value_atribute));
																product.setIze_Venta(venta);

															}

															if (name_atribute.equals("ize_alquiler")) {

																alquiler = new OptionSetValue(Integer.valueOf(value_atribute));
																product.setIze_Alquiler(alquiler);

															}

															if (name_atribute.equals("ize_latitud")) {

																ize_latitud = value_atribute;
																jsonObjResult.addProperty("lat", ize_latitud);
																product.setIze_latitud(ize_latitud);
																Log.e(TAG, "ize_latitud " + ize_latitud);
															}

															if (name_atribute.equals("ize_longitud")) {

																ize_longitud = value_atribute;
																jsonObjResult.addProperty("lng", ize_longitud);
																product.setIze_Longitud(ize_longitud);
																Log.e(TAG, "ize_longitud " + ize_longitud);

															}

															/*if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_codcategora")) {

																ize_categoriasinmuebles = nodes.item(i).getLastChild().getTextContent()
																		.split("ize_categoriasinmuebles")[1];
																product.setIze_CodCategoraName(ize_categoriasinmuebles);

															}*/
														}
														
														Log.e(TAG,"------------------------------------------------------------------------------------------------");
														Log.e(TAG,"------------------------------------------------------------------------------------------------");
														
														if (sigiApp.filterProduct(product)) {

															jsonObjResult.addProperty("elevation", "10");
															jsonObjResult.addProperty("title", "Edificio " + cont);
															jsonObjResult.addProperty("info", 
																	product.getIze_direccion_calle1() + "\n" + product.getIze_m2construidosdecimal().intValue()
																			+ " m�\n" + ize_numeros.toObject(product.getIze_Numhabitaciones().getValue())
																			+ " habitaci�n\n" + ize_numeros.toObject(product.getIze_NumBanios().getValue())
																			+ " wc\n" + Constants.FORMAT_MONEY.format(product.getPrice().Value) + " �");
															jsonObjResult.addProperty("has_detail_page", "0");
															jsonObjResult.addProperty("webpage", "");
															cont++;

															jsonAResultsCRM.add(jsonObjResult);
														}

														// A�adimos a la lista de productos
														list_products.add(product);

														ize_latitud = "";
														ize_longitud = "";
														ize_t_tipologia = "";
														ize_subtipologia = "";
														productid = "";
														//ize_categoriasinmuebles = "";
														numHabitaciones = null;
														numBanio = null;
														precio_venta = null;
														precio_venta_m2 = null;
														precio_alquiler = null;
														precio_alquiler_m2 = null;
														m2 = null;
														jsonObjResult = new JsonObject();
														product = new Product();
													 
												 }
		}catch (Exception ex) {
			Log.e(TAG, "FALLO " + ex.getMessage());
			ex.printStackTrace();
		}
			
			
		}
		
		
		/*try{
		for (int i = 0; i < nodes.getLength(); i++) {

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("productid")) {
				productid = nodes.item(i).getLastChild().getTextContent();
				jsonObjResult.addProperty("id", productid);
				product.setId(UUID.fromString(productid));
			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_subtipologia")) {
				product.setIze_subtipologia(
						new OptionSetValue(Integer.valueOf(nodes.item(i).getLastChild().getTextContent())));
				ize_subtipologia = ize_subtipoloologia.toObject(product.getIze_subtipologia().getValue()).name();
				Log.e(TAG, "ize_subtipologia " + ize_subtipologia);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_tipologia")) {
				product.setIze_tipologia(
						new OptionSetValue(Integer.valueOf(nodes.item(i).getLastChild().getTextContent())));
				ize_t_tipologia = ize_tipologia.toObject(product.getIze_tipologia().getValue()).name();
				Log.e(TAG, "ize_tipologia " + ize_t_tipologia);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_direccion_calle1")) {
				product.setIze_direccion_calle1(nodes.item(i).getLastChild().getTextContent());
			}
			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_direccion_numero")) {
				product.setIze_direccion_numero(nodes.item(i).getLastChild().getTextContent());
			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_planta")) {
				ize_planta = nodes.item(i).getLastChild().getTextContent();
				product.setIze_Planta(new OptionSetValue(Integer.valueOf(ize_planta)));
			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_direccion_letra")) {
				product.setIze_Direccion_letra(nodes.item(i).getLastChild().getTextContent());

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_numhabitaciones")) {
				numHabitaciones = new OptionSetValue(Integer.valueOf(nodes.item(i).getLastChild().getTextContent()));
				product.setIze_Numhabitaciones(numHabitaciones);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_numbanios")) {
				numBanio = new OptionSetValue(Integer.valueOf(nodes.item(i).getLastChild().getTextContent()));
				product.setIze_NumBanios(numBanio);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_m2construidosdecimal")) {
				m2 = new BigDecimal(nodes.item(i).getLastChild().getTextContent());
				product.setIze_m2construidosdecimal(m2);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_precioventam2constr")) {
				precio_venta_m2 = new Money(
						new BigDecimal(nodes.item(i).getLastChild().getTextContent().split(",")[0]));
				product.setIze_PrecioVentam2constr(precio_venta_m2);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("price")) {

				precio_venta = new Money(new BigDecimal(nodes.item(i).getLastChild().getTextContent()));
				product.setPrice(precio_venta);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_precioalquilermconst")) {
				precio_alquiler_m2 = new Money(
						new BigDecimal(nodes.item(i).getLastChild().getTextContent().split(",")[0]));
				product.setIze_PrecioAlquilermconst(precio_alquiler_m2);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_preciosalquiler")) {

				precio_alquiler = new Money(new BigDecimal(nodes.item(i).getLastChild().getTextContent()));
				product.setIze_PreciosAlquiler(precio_alquiler);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_venta")) {

				venta = new OptionSetValue(Integer.valueOf(nodes.item(i).getLastChild().getTextContent()));
				product.setIze_Venta(venta);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_alquiler")) {

				alquiler = new OptionSetValue(Integer.valueOf(nodes.item(i).getLastChild().getTextContent()));
				product.setIze_Alquiler(alquiler);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_latitud")) {

				ize_latitud = nodes.item(i).getLastChild().getTextContent();
				jsonObjResult.addProperty("lat", ize_latitud);
				product.setIze_latitud(ize_latitud);
				Log.e(TAG, "ize_latitud " + ize_latitud);
			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_longitud")) {

				ize_longitud = nodes.item(i).getLastChild().getTextContent();
				jsonObjResult.addProperty("lng", ize_longitud);
				product.setIze_Longitud(ize_longitud);
				Log.e(TAG, "ize_longitud " + ize_longitud);

			}

			if ((nodes.item(i).getFirstChild().getTextContent()).equals("ize_codcategora")) {

				ize_categoriasinmuebles = nodes.item(i).getLastChild().getTextContent()
						.split("ize_categoriasinmuebles")[1];
				product.setIze_CodCategoraName(ize_categoriasinmuebles);

			}
			
			
			
			if ((ize_latitud != "") && (ize_longitud != "")
					&& ((ize_categoriasinmuebles != "")&&(ize_t_tipologia != "")&&(ize_subtipologia != ""))
					
												  && (numHabitaciones != null)
												  && (numBanio != null) &&
												  (((precio_venta != null) &&
												  (precio_venta_m2 != null)) ||
												 ((precio_alquiler != null) &&
												  (precio_alquiler_m2 !=
												  null))) && (m2 != null)
												 ) {

				Log.e(TAG, "CUMPLE LOS DATOS MIN!! " + product.getId());

				// Solo si cumple el filtro lo a�adimos como marca en la vista
				// RA.
				if (sigiApp.filterProduct(product)) {

					jsonObjResult.addProperty("elevation", "10");
					jsonObjResult.addProperty("title", "Edificio " + cont);
					jsonObjResult.addProperty("info",
							product.getIze_direccion_calle1() + "\n" + product.getIze_m2construidosdecimal().intValue()
									+ " m�\n" + ize_numeros.toObject(product.getIze_Numhabitaciones().getValue())
									+ " habitaci�n\n" + ize_numeros.toObject(product.getIze_NumBanios().getValue())
									+ " wc\n" + Constants.FORMAT_MONEY.format(product.getPrice().Value) + " �");
					jsonObjResult.addProperty("has_detail_page", "0");
					jsonObjResult.addProperty("webpage", "");
					cont++;

					jsonAResultsCRM.add(jsonObjResult);
				}

				// A�adimos a la lista de productos
				list_products.add(product);

				ize_latitud = "";
				ize_longitud = "";
				ize_t_tipologia = "";
				ize_subtipologia = "";
				productid = "";
				ize_categoriasinmuebles = "";
				numHabitaciones = null;
				numBanio = null;
				precio_venta = null;
				precio_venta_m2 = null;
				precio_alquiler = null;
				precio_alquiler_m2 = null;
				m2 = null;
				jsonObjResult = new JsonObject();
				product = new Product();

			}

		}
		}catch (Exception ex) {
			Log.e(TAG, "FALLO " + ex.getMessage());
			ex.printStackTrace();
		}*/
		

		// Guardamos en el contexto todos los productos (cumplan el filtro o no
		// de primeras, se les aplica cuando se intenta obtener).
		sigiApp.setProducts(list_products);

		jsonObjCRM.addProperty("num_results", jsonAResultsCRM.size());
		jsonObjCRM.add("results", jsonAResultsCRM);

		return jsonObjCRM.toString();
	}

}
