import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class program {

	public static void main(String[] args) throws Exception {

		CrmAuth auth = new CrmAuth();

		// CRM Online
		/*String url = "https://org.crm.dynamics.com/"; 
		String username ="username@org.onmicrosoft.com"; 
		String password = "password";*/
		
		/*
		 * sigi1.crm4.dynamics.com
Id. de usuario: sigi1@sigi1.onmicrosoft.com

Password: Abral12345%
		 */
		
		String url = "https://sigi1.crm4.dynamics.com/"; 
		String username ="sigi1@sigi1.onmicrosoft.com"; 
		String password = "Abral12345%";
		
		CrmAuthenticationHeader authHeader = auth.GetHeaderOnline(username,
		password, url);
		// End CRM Online

		// CRM OnPremise - IFD
		//String url = "https://org.domain.com/";
		//// Username format could be domain\\username or username in the form of an email
		//String username = "username";
		//String password = "password";
		//CrmAuthenticationHeader authHeader = auth.GetHeaderOnPremise(username, password, url);
		// End CRM OnPremise - IFD

		String id = CrmWhoAmI(authHeader, url);
		if (id == null)
			return;

		String name = CrmGetUserName(authHeader, id, url);
		System.out.println(name);
		
		
		System.out.println("RESPUESTA" +CrmRetrieve(authHeader, id, url));
		
	}

	public static String CrmWhoAmI(CrmAuthenticationHeader authHeader,
			String url) throws IOException, SAXException,
			ParserConfigurationException {
		StringBuilder xml = new StringBuilder();
		xml.append("<s:Body>");
		xml.append("<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\">");
		xml.append("<request i:type=\"c:WhoAmIRequest\" xmlns:b=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:c=\"http://schemas.microsoft.com/crm/2011/Contracts\">");
		xml.append("<b:Parameters xmlns:d=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\"/>");
		xml.append("<b:RequestId i:nil=\"true\"/>");
		xml.append("<b:RequestName>WhoAmI</b:RequestName>");
		xml.append("</request>");
		xml.append("</Execute>");
		xml.append("</s:Body>");

		Document xDoc = CrmExecuteSoap.ExecuteSoapRequest(authHeader,
				xml.toString(), url);
		if (xDoc == null)
			return null;

		NodeList nodes = xDoc
				.getElementsByTagName("b:KeyValuePairOfstringanyType");
		for (int i = 0; i < nodes.getLength(); i++) {
			if ((nodes.item(i).getFirstChild().getTextContent())
					.equals("UserId")) {
				return nodes.item(i).getLastChild().getTextContent();
			}
		}

		return null;
	}

	public static String CrmGetUserName(CrmAuthenticationHeader authHeader,
			String id, String url) throws IOException, SAXException,
			ParserConfigurationException {
		StringBuilder xml = new StringBuilder();
		xml.append("<s:Body>");
		xml.append("<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">");
		xml.append("<request i:type=\"a:RetrieveRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">");
		xml.append("<a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">");
		xml.append("<a:KeyValuePairOfstringanyType>");
		xml.append("<b:key>Target</b:key>");
		xml.append("<b:value i:type=\"a:EntityReference\">");
		xml.append("<a:Id>" + id + "</a:Id>");
		xml.append("<a:LogicalName>systemuser</a:LogicalName>");
		xml.append("<a:Name i:nil=\"true\" />");
		xml.append("</b:value>");
		xml.append("</a:KeyValuePairOfstringanyType>");
		xml.append("<a:KeyValuePairOfstringanyType>");
		xml.append("<b:key>ColumnSet</b:key>");
		xml.append("<b:value i:type=\"a:ColumnSet\">");
		xml.append("<a:AllColumns>false</a:AllColumns>");
		xml.append("<a:Columns xmlns:c=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
		xml.append("<c:string>firstname</c:string>");
		xml.append("<c:string>lastname</c:string>");
		xml.append("</a:Columns>");
		xml.append("</b:value>");
		xml.append("</a:KeyValuePairOfstringanyType>");
		xml.append("</a:Parameters>");
		xml.append("<a:RequestId i:nil=\"true\" />");
		xml.append("<a:RequestName>Retrieve</a:RequestName>");
		xml.append("</request>");
		xml.append("</Execute>");
		xml.append("</s:Body>");

		Document xDoc = CrmExecuteSoap.ExecuteSoapRequest(authHeader,
				xml.toString(), url);
		if (xDoc == null)
			return null;

		String firstname = "";
		String lastname = "";

		NodeList nodes = xDoc
				.getElementsByTagName("b:KeyValuePairOfstringanyType");
		for (int i = 0; i < nodes.getLength(); i++) {
			if ((nodes.item(i).getFirstChild().getTextContent())
					.equals("firstname")) {
				firstname = nodes.item(i).getLastChild().getTextContent();
			}
			if ((nodes.item(i).getFirstChild().getTextContent())
					.equals("lastname")) {
				lastname = nodes.item(i).getLastChild().getTextContent();
			}
		}

		return firstname + " " + lastname;
	}
	
	
	public static String CrmRetrieve(CrmAuthenticationHeader authHeader,
			String id, String url) throws IOException, SAXException,
			ParserConfigurationException {
		//String xml = "<s:Body><d:Retrieve><d:entityName>contact</d:entityName><d:id>7a3c068e-8662-4c0b-9ccb-a76921d22b6a</d:id><d:columnSet><a:AllColumns>false</a:AllColumns><a:Columns><f:string>contactid</f:string><f:string>fullname</f:string><f:string>address1_line1</f:string><f:string>address1_city</f:string><f:string>address1_stateorprovince</f:string><f:string>address1_postalcode</f:string><f:string>emailaddress1</f:string><f:string>jobtitle</f:string><f:string>telephone1</f:string></a:Columns></d:columnSet></d:Retrieve></s:Body>";
		StringBuilder xml = new StringBuilder();
		/*xml.append("<s:Body>");
		xml.append("<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">");
		xml.append("<request i:type=\"a:RetrieveRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">");
		xml.append("<a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">");
		xml.append("<a:KeyValuePairOfstringanyType>");
		xml.append("<b:key>Target</b:key>");
		xml.append("<b:value i:type=\"a:EntityReference\">");
		xml.append("<a:Id>" + id + "</a:Id>");
		xml.append("<a:LogicalName>contact</a:LogicalName>");
		xml.append("<a:Name i:nil=\"true\" />");
		xml.append("</b:value>");
		xml.append("</a:KeyValuePairOfstringanyType>");
		xml.append("<a:KeyValuePairOfstringanyType>");
		xml.append("<b:key>ColumnSet</b:key>");
		xml.append("<b:value i:type=\"a:ColumnSet\">");
		xml.append("<a:AllColumns>false</a:AllColumns>");
		xml.append("<a:Columns xmlns:c=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
		xml.append("<c:string>contactid</c:string>");
		xml.append("<c:string>fullname</c:string>");		
		xml.append("<c:string>address1_line1</c:string>");
		xml.append("<c:string>address1_city</c:string>");
		xml.append("<c:string>address1_stateorprovince</c:string>");
		xml.append("<c:string>telephone1</c:string>");		
		xml.append("</a:Columns>");
		xml.append("</b:value>");
		xml.append("</a:KeyValuePairOfstringanyType>");
		xml.append("</a:Parameters>");
		xml.append("<a:RequestId i:nil=\"true\" />");
		xml.append("<a:RequestName>Retrieve</a:RequestName>");
		xml.append("</request>");
		xml.append("</Execute>");
		xml.append("</s:Body>");*/
		
		
		/*xml.append("  <s:Body>");
		xml.append("    <Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">");
		xml.append("      <request i:type=\"a:RetrieveAllEntitiesRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">");
		xml.append("        <a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">");
		xml.append("          <a:KeyValuePairOfstringanyType>");
		xml.append("            <b:key>EntityFilters</b:key>");
		xml.append("            <b:value i:type=\"c:EntityFilters\" xmlns:c=\"http://schemas.microsoft.com/xrm/2011/Metadata\">Entity</b:value>");
		xml.append("          </a:KeyValuePairOfstringanyType>");
		xml.append("          <a:KeyValuePairOfstringanyType>");
		xml.append("            <b:key>RetrieveAsIfPublished</b:key>");
		xml.append("            <b:value i:type=\"c:boolean\" xmlns:c=\"http://www.w3.org/2001/XMLSchema\">true</b:value>");
		xml.append("          </a:KeyValuePairOfstringanyType>");
		xml.append("        </a:Parameters>");
		xml.append("        <a:RequestId i:nil=\"true\" />");
		xml.append("        <a:RequestName>RetrieveAllEntities</a:RequestName>");
		xml.append("      </request>");
		xml.append("    </Execute>");
		xml.append("  </s:Body>");*/
		
		
		
		
		/*<s:Envelope xmlns:s="schemas.xmlsoap.org/.../&quot;><s:Body>',

		           '<Execute xmlns="schemas.microsoft.com/.../Services&quot; xmlns:i="www.w3.org/.../XMLSchema-instance&quot; >',

		               '<request i:type="a:RetrieveMultipleRequest" xmlns:a="schemas.microsoft.com/.../Contracts&quot;>',

		                   '<a:Parameters xmlns:b="schemas.datacontract.org/.../System.Collections.Generic&quot;>',

		                       '<a:KeyValuePairOfstringanyType>',

		                           '<b:key>Query</b:key>',

		                           '<b:value i:type="a:FetchExpression">',

		                               '<a:Query>', encodeFetch(fetchXml), '</a:Query>',

		                           '</b:value>',

		                       '</a:KeyValuePairOfstringanyType>',

		                   '</a:Parameters>',

		                   '<a:RequestId i:nil="true"/>',

		                   '<a:RequestName>RetrieveMultiple</a:RequestName>',

		               '</request>',

		           '</Execute>',

		       '</s:Body></s:Envelope>*/
		
		
		// CONSEGUIDO!! obtenemos los datos del piso!!
		/*xml.append("<s:Body>");
		xml.append("<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">");
		xml.append("<request i:type=\"a:RetrieveRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">");
		xml.append("<a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">");
		xml.append("<a:KeyValuePairOfstringanyType>");
		xml.append("<b:key>Target</b:key>");
		xml.append("<b:value i:type=\"a:EntityReference\">");
		//xml.append("<a:Id>" + id + "</a:Id>");
		xml.append("<a:Id>a876afa7-161f-e511-80d0-3863bb354d10</a:Id>");
		//xml.append("<a:Id>c605af8a-171f-e511-80d0-3863bb354d10</a:Id>");		
		xml.append("<a:LogicalName>product</a:LogicalName>");
		xml.append("<a:Name i:nil=\"true\" />");
		xml.append("</b:value>");
		xml.append("</a:KeyValuePairOfstringanyType>");
		xml.append("<a:KeyValuePairOfstringanyType>");
		xml.append("<b:key>ColumnSet</b:key>");
		xml.append("<b:value i:type=\"a:ColumnSet\">");
		xml.append("<a:AllColumns>false</a:AllColumns>");
		xml.append("<a:Columns xmlns:c=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
		xml.append("<c:string>productid</c:string>");
		xml.append("<c:string>ize_longitud</c:string>");		
		xml.append("<c:string>ize_latitud</c:string>");
		xml.append("</a:Columns>");
		xml.append("</b:value>");
		xml.append("</a:KeyValuePairOfstringanyType>");
		xml.append("</a:Parameters>");
		xml.append("<a:RequestId i:nil=\"true\" />");
		xml.append("<a:RequestName>Retrieve</a:RequestName>");
		xml.append("</request>");
		xml.append("</Execute>");
		xml.append("</s:Body>");*/
		
		// Consulta para conseguir todos los productos (FILTRAR SOLO LOS PISOS)
		/*xml.append("<s:Body>");
		xml.append("<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">");
		xml.append("<request i:type=\"a:RetrieveMultipleRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">");
		xml.append("<a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">");
		xml.append("<a:KeyValuePairOfstringanyType>");		
		
		xml.append("<b:key>Query</b:key>");
		xml.append("<b:value i:type=\"a:QueryExpression\">");
		xml.append("<a:ColumnSet>");
		xml.append("<a:AllColumns>false</a:AllColumns>");
		xml.append("<a:Columns xmlns:c=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
		
		xml.append("<c:string>productid</c:string>");
		xml.append("<c:string>ize_longitud</c:string>");		
		xml.append("<c:string>ize_latitud</c:string>");
		xml.append("<c:string>ize_direccion_calle1</c:string>");
		xml.append("<c:string>ize_direccion_numero</c:string>");
		xml.append("<c:string>ize_m2construidosdecimal</c:string>");
		xml.append("<c:string>ize_numhabitaciones</c:string>");
		xml.append("<c:string>ize_numbanios</c:string>");
		xml.append("<c:string>ize_preciosalquiler</c:string>");
		xml.append("<c:string>ize_precioventam2constr</c:string>");	
		
		
		xml.append("</a:Columns>");
		xml.append("</a:ColumnSet>");
		
		xml.append("<a:Distinct>false</a:Distinct>");
		xml.append("<a:EntityName>product</a:EntityName>");
		xml.append("<a:LinkEntities />");
		xml.append("<a:Orders />");
		xml.append("</b:value>");
		xml.append("</a:KeyValuePairOfstringanyType>");
		xml.append("</a:Parameters>");
		xml.append("<a:RequestId i:nil=\"true\" />");
		xml.append("<a:RequestName>RetrieveMultiple</a:RequestName>");
		xml.append("</request>");
		xml.append("</Execute>");
		xml.append("</s:Body>");*/

		Document xDoc = CrmRetrieveSoap.RetrieveSoapRequest(authHeader,
				//xml.toString(), url);
				Constants.CRM_REQUEST_RETRIEVE_MULTIPLE, url);
		if (xDoc == null)
			return null;
		
		String ize_latitud = "";
		String ize_longitud = "";
		
		//NodeList nodes = xDoc.getElementsByTagName("b:KeyValuePairOfstringanyType");
		NodeList nodes = xDoc.getElementsByTagName("b:Entity");
		
		System.out.println("NODOS ENTITY " +nodes.getLength());
		
		int cont=0;
		
		for (int i = 0; i < nodes.getLength(); i++) {
			
			System.out.println("CONTENIDO ENTITY "+i+" "+nodes.item(i).getTextContent());
			
			NodeList nodesEntity = nodes.item(i).getChildNodes();
			
			//for (int j = 0; j < nodesEntity.getLength(); j++) {
			
			System.out.println("INFO NODO ATRIBUTES "+nodesEntity.item(0).getTextContent());
			
			if(!nodesEntity.item(0).getTextContent().contains("ize_latitud")||!nodesEntity.item(0).getTextContent().contains("ize_longitud")){
				cont++;
			}
			
			NodeList nodesAttributes = nodesEntity.item(0).getChildNodes();
			
			System.out.println("CANTIDAD ATRIBUTES "+nodesAttributes.getLength());
			
			
			
			for (int j = 0; j < nodesAttributes.getLength(); j++) {
			
			System.out.println("CONTENT ATRIBUTES "+nodesAttributes.item(j).getTextContent());
			
			System.out.println("NAME ATRIBUTES "+nodesAttributes.item(j).getFirstChild().getTextContent());
			System.out.println("VALUE ATRIBUTES "+nodesAttributes.item(j).getLastChild().getTextContent());
			}
			
			System.out.println("------------------------------------------------------------------------------------------------");
			System.out.println("------------------------------------------------------------------------------------------------");
			
			//}
			
			//System.out.println("INFO NODO ATRIBUTES"+nodes.item(i).getAttributes().getLength());
			
			//if(getTextContent().
			
			
			//for(int j=0;j<)
			
			if ((nodes.item(i).getFirstChild().getTextContent())
					.equals("ize_latitud")) {
				ize_latitud = nodes.item(i).getLastChild().getTextContent();
				System.out.println("ize_latitud " +ize_latitud);
			}
			if ((nodes.item(i).getFirstChild().getTextContent())
					.equals("ize_longitud")) {
				ize_longitud = nodes.item(i).getLastChild().getTextContent();
				System.out.println("ize_longitud " +ize_longitud);
			}
		}
		
		
		System.out.println("CANTIDAD FINAL DE LOCALES SIN COORDENADAS "+cont);

		/*String firstname = "";
		String lastname = "";

		NodeList nodes = xDoc
				.getElementsByTagName("b:KeyValuePairOfstringanyType");
		for (int i = 0; i < nodes.getLength(); i++) {
			if ((nodes.item(i).getFirstChild().getTextContent())
					.equals("firstname")) {
				firstname = nodes.item(i).getLastChild().getTextContent();
			}
			if ((nodes.item(i).getFirstChild().getTextContent())
					.equals("lastname")) {
				lastname = nodes.item(i).getLastChild().getTextContent();
			}
		}*/

		//return xDoc.toString();
		return xDoc.toString();
	}
}
