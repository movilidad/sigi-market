
public class Constants {
	
	static public final String CRM_REQUEST_RETRIEVE_MULTIPLE=
			"<s:Body>"+
			"<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">"+
			"<request i:type=\"a:RetrieveMultipleRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">"+
			"<a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">"+
			"<a:KeyValuePairOfstringanyType>"+	
			//HASTA AQUI ES GENERICO
			"<b:key>Query</b:key>"+
			"<b:value i:type=\"a:QueryExpression\">"+
			"<a:ColumnSet>"+
			"<a:AllColumns>false</a:AllColumns>"+
			"<a:Columns xmlns:c=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">"+
			//DEBERIA SER DINAMICO
			"<c:string>productid</c:string>"+
			"<c:string>ize_longitud</c:string>"+		
			"<c:string>ize_latitud</c:string>"+
			"<c:string>ize_direccion_calle1</c:string>"+			
			"<c:string>ize_direccion_numero</c:string>"+
			"<c:string>ize_planta</c:string>"+
			"<c:string>ize_direccion_letra</c:string>"+
			"<c:string>ize_numhabitaciones</c:string>"+
			"<c:string>ize_numbanios</c:string>"+	
			"<c:string>ize_m2edificables</c:string>"+
			"<c:string>ize_m2construidosdecimal</c:string>"+
			"<c:string>ize_preciosalquiler</c:string>"+	
			"<c:string>ize_precioventam2constr</c:string>"+	
			//NUEVOS
			"<c:string>ize_precioalquilerenopcion</c:string>"+
			"<c:string>ize_precioalquilerenopcion_base</c:string>"+
			"<c:string>ize_precioalquilermconst_base</c:string>"+
			"<c:string>ize_precioreserva_base</c:string>"+
			"<c:string>ize_precioreserva</c:string>"+
			"<c:string>ize_preciosalquiler_base</c:string>"+
			"<c:string>ize_precioventam2constr_base</c:string>"+
			//"<c:string>ize_primaopcion</c:string>"+
			"<c:string>currentcost</c:string>"+
			"<c:string>ize_varlorcatastral</c:string>"+			
			"<c:string>price</c:string>"+
			"<c:string>price_base</c:string>"+
			"<c:string>standardcost</c:string>"+
			"<c:string>ize_codcategora</c:string>"+
			"<c:string>ize_venta</c:string>"+
			"<c:string>ize_alquiler</c:string>"+
			"<c:string>ize_subtipologia</c:string>"+			
			"<c:string>ize_tipologia</c:string>"+
			"</a:Columns>"+
			"</a:ColumnSet>"+
			
			
			"<b:Criteria>"+
	         /*"<b:Conditions>"+
	            "<b:Condition>"+
	               "<b:AttributeName>ize_longitud</b:AttributeName>"+
	               //"<b:Operator>NotNull</b:Operator>"+
	               "<b:Operator>Equal</b:Operator>"+
	               "<b:Values>"+
	                  "<b:Value i:type=\"xsd:string\"></b:Value>"+
	               "</b:Values>"+
	               
	            "</b:Condition>"+
	               
	               "<b:Condition>"+
	               "<b:AttributeName>ize_latitud</b:AttributeName>"+
	               //"<b:Operator>NotNull</b:Operator>"+
	               "<b:Operator>Equal</b:Operator>"+
	               "<b:Values>"+
	                  "<b:Value i:type=\"xsd:string\"></b:Value>"+
	               "</b:Values>"+
	               
	            "</b:Condition>"+

	         "</b:Conditions>"+
	               
	         "<b:FilterOperator>And</b:FilterOperator>"+
	         "<b:Filters />"+*/
	         
"<b:Conditions>"+
            "<b:ConditionExpression>"+
               "<b:AttributeName>ize_latitud</b:AttributeName>"+
               "<b:Operator>Null</b:Operator>"+
               /*"<b:Values xmlns:c=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">"+
                  "<c:anyType xmlns:d=\"http://www.w3.org/2001/XMLSchema\" i:type=\"d:string\"></c:anyType>"+
               "</b:Values>"+*/
            "</b:ConditionExpression>"+
         "</b:Conditions>"+
               "<b:FilterOperator>And</b:FilterOperator>"+
               "<b:Filters />"+
	      
	      "</b:Criteria>"+
			
			
			"<a:Distinct>false</a:Distinct>"+
			
			//DEBERIA SER DINAMICO
			"<a:EntityName>product</a:EntityName>"+
			"<a:LinkEntities />"+
			"<a:Orders />"+
			"</b:value>"+
			//A PARTIR DE AQUI GENERICO MENOS RequestName DEBE SER DINAMICO
			"</a:KeyValuePairOfstringanyType>"+
			"</a:Parameters>"+
			"<a:RequestId i:nil=\"true\" />"+
			"<a:RequestName>RetrieveMultiple</a:RequestName>"+
			"</request>"+
			"</Execute>"+
			"</s:Body>";

}
